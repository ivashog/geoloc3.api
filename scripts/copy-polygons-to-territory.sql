DO
$do$
BEGIN
    FOR i IN 1..5 LOOP
        INSERT INTO test.to_territory
            (name, koatuu, region, district, county, station, district_to_counties, uniq_code, type_id, v_version )
        SELECT
            name,
            koatuu,
            region_id::varchar,
            district_id::varchar,
            county_id::varchar,
            polling_station_id,
            districts_to_counties_arr::varchar[],
            (CASE
                WHEN polygon_type_id = 3
                    THEN concat(region_id, ':', district_id)
                WHEN polygon_type_id = 1
                    THEN '1'
            ELSE uniq_id END),
            polygon_type_id,
            1
        FROM public.polygon_objects
        WHERE polygon_type_id = i
        ORDER BY region_id, district_id, county_id, station_id;
   END LOOP;
END;
$do$ LANGUAGE plpgsql;

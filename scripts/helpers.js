function parseArgv() {
    const props = {};
    const [, , ...args] = process.argv;

    args.forEach(arg => {
        const [prop, value] = arg.split('=');
        props[prop] = Number.isNaN(+value) ? value : +value;
    });

    return props;
}

module.exports = parseArgv;

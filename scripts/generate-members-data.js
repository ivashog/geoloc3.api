const { config } = require('dotenv');
const { Pool } = require('pg');
const axios = require('axios');
const faker = require('faker');
const fsPromises = require('fs').promises;
const os = require('os');
const {
    enp2s0: [{ address: localIP = 'localhost' }],
} = os.networkInterfaces();

const envName = process.env.NODE_ENV || 'development';
config({ path: `${process.cwd()}/${envName}.env` });

const API_HOST = `http://${localIP}:${process.env.APP_PORT}`;
const LOGIN_URL = '/api/v1/users/login';
const POST_MEMBERS_URL = '/api/v1/users/members';

const parseArgv = require('./helpers');
const args = parseArgv();

faker.locale = args.locale || 'uk';

const pool = new Pool({
    database: process.env.TYPEORM_DATABASE,
    user: process.env.TYPEORM_USERNAME,
    password: process.env.TYPEORM_PASSWORD,
    host: process.env.TYPEORM_HOST,
    port: process.env.TYPEORM_PORT,
});

pool.connect().catch(e => console.log(e));

async function run() {
    const amount = args.amount || 1000;

    const stations = await getRandomStations(amount);
    const users = generateFakeUsers(stations);

    await postUsersByAPI(users);
}

const getRandomStations = async (amount = 1000) => {
    const { rows: stations } = await pool.query(
        `
        SELECT
            uniq_code
        FROM geoloc3.to_territory
        WHERE type_id = 5
        ORDER BY RANDOM() LIMIT $1
    `,
        [amount],
    );

    return stations.map(station => station.uniq_code);
};

const generateFakeUsers = stations => {
    return stations.map(station => {
        const [region, county, polling_station] = station.split(':');
        return {
            name: `${faker.name.firstName()} ${faker.name.lastName()}`,
            phone: faker.phone.phoneNumber(),
            can_edit: true,
            level: 5,
            role: 3,
            region,
            county,
            station: polling_station,
            uniq_code: station,
        };
    });
};

const postUsersByAPI = async users => {
    const url = `${API_HOST}${POST_MEMBERS_URL}`;
    const token = await loginToAPI();
    const headers = { Authorization: `Bearer ${token}` };
    const result = {
        inserted: 0,
        errors: [],
    };

    users.forEach(async (user, idx) => {
        const { status, data } = await axios.post(url, user, { headers });

        if (status !== 201) {
            result.errors.push(data);
            console.log('FAIL >> user:', idx + 1, ' - ', user.name);
        } else {
            result.inserted++;
            console.log('OK >> inserted user:', idx + 1, ' - ', user.name);
        }

        await fsPromises.writeFile(
            `${process.cwd()}/scripts/logs/members-log.json`,
            JSON.stringify(result),
            {
                encoding: 'UTF-8',
            },
        );
    });
};

const loginToAPI = async () => {
    const url = `${API_HOST}${LOGIN_URL}`;
    const loginData = {
        login: process.env.ADMIN_LOGIN,
        password: process.env.ADMIN_PASSWORD,
    };
    const { data } = await axios.post(url, loginData, {
        headers: { 'Content-Type': 'application/json' },
    });

    return data.token;
};

run();

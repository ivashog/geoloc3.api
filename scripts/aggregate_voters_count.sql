WITH voters_by_regions AS (
	SELECT
		t.region,
		SUM(elsd.voters) AS voters
	FROM geoloc3.el_static_data elsd
	LEFT JOIN geoloc3.to_territory t
		ON elsd.to_id = t.id
	WHERE elsd.type_id = 5
	GROUP BY t.region
), voters_by_districts AS (
	SELECT
		t.district,
		SUM(elsd.voters) AS voters
	FROM geoloc3.el_static_data elsd
	LEFT JOIN geoloc3.to_territory t
		ON elsd.to_id = t.id
	WHERE elsd.type_id = 5
	GROUP BY t.district
), voters_by_counties AS (
	SELECT
		t.county,
		SUM(elsd.voters) AS voters
	FROM geoloc3.el_static_data elsd
	LEFT JOIN geoloc3.to_territory t
		ON elsd.to_id = t.id
	WHERE elsd.type_id = 5
	GROUP BY t.county
)
UPDATE geoloc3.el_static_data el
SET voters = (
	CASE
		WHEN el.type_id = 1
			THEN (SELECT SUM(voters) FROM voters_by_regions)
		WHEN el.type_id = 2
			THEN (SELECT SUM(voters) FROM voters_by_regions v
				  LEFT JOIN geoloc3.to_territory tr
				  	ON tr.type_id = 2 AND v.region = tr.region
				  WHERE el.to_id = tr.id)
		WHEN el.type_id = 3
			THEN (SELECT SUM(voters) FROM voters_by_districts v
				  LEFT JOIN geoloc3.to_territory tr
				  	ON tr.type_id = 3 AND v.district = tr.district
				  WHERE el.to_id = tr.id)
		WHEN el.type_id = 4
			THEN (SELECT SUM(voters) FROM voters_by_counties v
				  LEFT JOIN geoloc3.to_territory tr
				  	ON tr.type_id = 4 AND v.county = tr.county
				  WHERE el.to_id = tr.id)
	END
)
WHERE el.type_id IN (1,2,3,4);

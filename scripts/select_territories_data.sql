-- for ./migration_data/territories-data.csv
SELECT
	name, koatuu, region, district, county, station, district_to_counties, uniq_code, type_id, v_version
FROM geoloc3.to_territory
ORDER BY id ASC;

-- zegeoloc prod db (db-postgresql-11-fra1-the-geoloc-do-user-4746580-0.db.ondigitalocean.com) db name - cvk_tour2
-- for ./migration_data/stations-static-data.csv
SELECT
	t.dvk AS station,
	po.uniq_id,
	t.vot_count::int AS voters_count,
	ssd.station_type_id,
	ssd.address,
	ssd.boundaries,
	ssd.place AS place_descr
FROM public.turnout_monitoring t
LEFT JOIN public.polling_station_static_data ssd
	ON t.dvk = ssd.polling_station_id
LEFT JOIN public.polygon_objects po
	ON t.polygon_obj_id = po.id
ORDER BY t.dvk

WITH counties_by_region AS (
	SELECT
  		region,
  		COUNT(county) AS counties
	FROM geoloc3.to_territory
	WHERE type_id = 4
	GROUP BY region
), counties_by_districts AS (
	SELECT
  		district,
  		array_length(district_to_counties, 1) AS counties
	FROM geoloc3.to_territory
	WHERE type_id = 3
	GROUP BY district, district_to_counties
), districts_by_region AS (
	SELECT
  		region,
  		COUNT(district) AS districts
	FROM geoloc3.to_territory
	WHERE type_id = 3
	GROUP BY region
), districts_by_county AS (
	SELECT
  		county,
  		array_length(district_to_counties, 1) AS districts
	FROM geoloc3.to_territory
	WHERE type_id = 4
	GROUP BY county, district_to_counties
), stations_by_regions AS (
	SELECT
		region,
  		COUNT(station) AS stations
	FROM geoloc3.to_territory
	WHERE type_id = 5
	GROUP BY region
), stations_by_counties AS (
	SELECT
		region,
  		county,
  		COUNT(station) AS stations
	FROM geoloc3.to_territory
	WHERE type_id = 5
	GROUP BY region, county
), stations_by_districts AS (
	SELECT
		region,
  		district,
  		COUNT(station) AS stations
	FROM geoloc3.to_territory
	WHERE type_id = 5
	GROUP BY region, district
)
SELECT
	t.name,
	t.type_id,
	(CASE
	 	WHEN t.type_id = 1
	 		THEN (SELECT SUM(counties) FROM counties_by_region)
		WHEN t.type_id = 2
	 		THEN (SELECT counties FROM counties_by_region cbr WHERE cbr.region = t.region)
	 	WHEN t.type_id = 3
	 		THEN (SELECT counties FROM counties_by_districts cbd WHERE cbd.district = t.district)
	 	WHEN t.type_id = 4
	 		THEN 1
		ELSE null
	END) AS counties,
	(CASE
	 	WHEN t.type_id = 1
	 		THEN (SELECT SUM(districts) FROM districts_by_region)
		WHEN t.type_id = 2
	 		THEN (SELECT districts FROM districts_by_region dbr WHERE dbr.region = t.region)
	 	WHEN t.type_id = 3
	 		THEN 1
	 	WHEN t.type_id = 4
	 		THEN (SELECT districts FROM districts_by_county dbc WHERE dbc.county = t.county)
		ELSE null
	END) AS districts,
	(CASE
	 	WHEN t.type_id = 1
	 		THEN (SELECT SUM(stations) FROM stations_by_regions)
		WHEN t.type_id = 2
	 		THEN (SELECT stations FROM stations_by_regions sbr WHERE sbr.region = t.region)
	 	WHEN t.type_id = 3
	 		THEN (SELECT stations FROM stations_by_districts sbd WHERE sbd.district = t.district)
	 	WHEN t.type_id = 4
	 		THEN (SELECT stations FROM stations_by_counties sbc WHERE sbc.county = t.county)
	 	WHEN t.type_id = 5
	 		THEN 1
		ELSE null
	END) AS stations
FROM geoloc3.to_territory t
ORDER BY t.type_id, t.region, t.county, t.district;

const { config } = require('dotenv');
const { Pool } = require('pg');

const envName = process.env.NODE_ENV || 'development';
config({ path: `${process.cwd()}/${envName}.env` });

const pool = new Pool({
    database: process.env.TYPEORM_DATABASE,
    user: process.env.TYPEORM_USERNAME,
    password: process.env.TYPEORM_PASSWORD,
    host: process.env.TYPEORM_HOST,
    port: process.env.TYPEORM_PORT,
});

pool.connect().catch(e => console.log(e));

async function run() {
    const { rows: data } = await pool.query('SELECT now() AS time');
    console.log(' >> time:', data[0].time);
    console.log(' >> process.argv', process.argv);
}

run();

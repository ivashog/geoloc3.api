WITH votes_by_station AS (
	SELECT
		par.party_id,
		par.votes,
		ter.region,
		ter.district,
		ter.county,
		ter.station,
		ter.uniq_code,
		ter.id AS territory_id
	FROM geoloc3.to_territory ter
	LEFT JOIN geoloc3.el_result_party par
		ON ter.id = par.to_id
	WHERE ter.type_id = 5
), total_votes_by_station AS (
	SELECT
		uniq_code,
		county,
		region,
		COALESCE(SUM(votes), 0)::int AS total_voters
	FROM votes_by_station vs
	GROUP BY uniq_code, county, region
), total_votes_by_counties AS (
	SELECT
		county,
		region,
		COALESCE(SUM(total_voters), 0) AS total_voters
	FROM total_votes_by_station tvs
	GROUP BY county, region
), total_votes_by_regions AS (
	SELECT
		region,
		COALESCE(SUM(total_voters), 0) AS total_voters
	FROM total_votes_by_counties tvs
	GROUP BY region
), result_by_stations AS (
	SELECT
		5 AS level,
		vs.region,
		vs.county,
		vs.station,
		vs.party_id,
		vs.votes,
		tvs.total_voters,
		(CASE
		 	WHEN tvs.total_voters = 0 THEN 0
			ELSE vs.votes::decimal / tvs.total_voters::decimal * 100
		END)::decimal(5,2) AS percentage
	FROM votes_by_station vs
	LEFT JOIN total_votes_by_station tvs
		ON vs.uniq_code = tvs.uniq_code
), result_by_counties AS (
	SELECT
		4 AS level,
		vs.region,
		vs.county,
		null AS station,
		vs.party_id,
		SUM(vs.votes) AS votes,
		tvs.total_voters,
		(CASE
		 	WHEN tvs.total_voters = 0 THEN 0
			ELSE SUM(vs.votes)::decimal / tvs.total_voters::decimal * 100
		END)::decimal(5,2) AS percentage
	FROM result_by_stations vs
	LEFT JOIN total_votes_by_counties tvs
		ON vs.county = tvs.county
	GROUP BY vs.region, vs.county, vs.party_id, tvs.total_voters
), result_by_regions AS (
	SELECT
		2 AS level,
		vs.region,
		null AS county,
		null AS station,
		vs.party_id,
		SUM(vs.votes) AS votes,
		tvs.total_voters,
		(CASE
		 	WHEN tvs.total_voters = 0 THEN 0
			ELSE SUM(vs.votes)::decimal / tvs.total_voters::decimal * 100
		END)::decimal(5,2) AS percentage
	FROM result_by_counties vs
	LEFT JOIN total_votes_by_regions tvs
		ON vs.region = tvs.region
	GROUP BY vs.region, vs.party_id, tvs.total_voters
)
SELECT * FROM result_by_regions
UNION ALL
SELECT * FROM result_by_counties
UNION ALL
SELECT * FROM result_by_stations;




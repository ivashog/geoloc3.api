import { config } from 'dotenv';
import 'reflect-metadata';
import * as compression from 'compression';
import { NestFactory, Reflector } from '@nestjs/core';
import { Logger } from '@nestjs/common';

import { AppModule } from './app.module';
import { TransformBodyCasePipe } from './common/pipes/transform-body-case.pipe';
import { AuthGuard } from './common/guards/auth.guard';
import { AccessGuard } from './common/guards/access.guard';

const envName = process.env.NODE_ENV || 'development';
config({ path: `${process.cwd()}/${envName}.env` });
const PORT = +process.env.APP_PORT || 4000;

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.setGlobalPrefix('api/v1');
    app.enableCors();
    app.useGlobalPipes(new TransformBodyCasePipe());
    app.useGlobalGuards(new AuthGuard(), new AccessGuard(new Reflector()));
    app.use(compression());

    await app.listen(PORT);
    Logger.log(`Api running on http://localhost:${PORT}`, 'Bootstrap');
}
bootstrap();

import { Module } from '@nestjs/common';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppController } from './app.controller';
import { UserModule } from './user/user.module';
import { HttpExceptionFilter } from './common/exceptions/http-error.filter';
import { LoggingInterceptor } from './common/interceptors/logging.interceptor';
import { TerritoryModule } from './territory/territory.module';
import { ElectionModule } from './election/election.module';

@Module({
    imports: [TypeOrmModule.forRoot(), UserModule, TerritoryModule, ElectionModule],
    controllers: [AppController],
    providers: [
        {
            provide: APP_FILTER,
            useClass: HttpExceptionFilter,
        },
        {
            provide: APP_INTERCEPTOR,
            useClass: LoggingInterceptor,
        },
    ],
})
export class AppModule {}

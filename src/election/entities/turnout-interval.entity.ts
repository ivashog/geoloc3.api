import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { TurnoutEntity } from './turnout.entity';

@Entity('el_m_turnout_interval_dic')
export class TurnoutIntervalEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('varchar')
    name: string;

    @Column('integer', { name: 'min_percent' })
    minPercent: number;

    @Column('integer', { name: 'max_percent' })
    maxPercent: number;

    @OneToMany(type => TurnoutEntity, turnout => turnout.interval)
    turnoutData: TurnoutEntity;
}

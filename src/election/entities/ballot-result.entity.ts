import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, Index } from 'typeorm';
import { TerritoryEntity } from '../../territory/entities/territory.entity';
import { Versioning } from '../../common/entities/versioning.entity';

@Entity('el_result_ballot')
export class BallotResultEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Index({ unique: true })
    @OneToOne(type => TerritoryEntity, territory => territory.id)
    @JoinColumn({ name: 'to_id' })
    territory: TerritoryEntity;

    @Column('integer', { name: 'ballots_received', nullable: true })
    ballotsReceived: number;

    @Column('integer', { name: 'voters_end_voting', nullable: true })
    votersEndVoting: number;

    @Column('integer', { name: 'voters_home_prev', nullable: true })
    votersHomePrev: number;

    @Column('integer', { name: 'unused_ballots', nullable: true })
    unusedBallots: number;

    @Column('integer', { name: 'voters_station', nullable: true })
    votersStation: number;

    @Column('integer', { name: 'voters_home_fact', nullable: true })
    votersHomeFact: number;

    @Column('integer', { name: 'total_received', nullable: true })
    totalReceived: number;

    @Column('integer', { name: 'ballots_not_accounted', nullable: true })
    ballotsNotAccounted: number;

    @Column('integer', { name: 'voters_participated', nullable: true })
    votersParticipated: number;

    @Column('integer', { name: 'invalid_ballots', nullable: true })
    invalidBallots: number;

    @Column('integer', { name: 'ballots_acts_7375', nullable: true })
    ballotsActs7375: number;

    @Column('integer', { name: 'ballots_acts_78', nullable: true })
    ballotsActs78: number;

    @Column(type => Versioning)
    v: Versioning;

    public toResponseObj() {
        const responseObj = Object.assign({}, this);
        const propsForDelete: string[] = ['id', 'territory', 'v'];
        propsForDelete.forEach(prop => delete responseObj[prop]);

        return responseObj;
    }
}

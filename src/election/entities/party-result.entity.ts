import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TerritoryEntity } from '../../territory/entities/territory.entity';
import { PartyEntity } from './party.entity';
import { Versioning } from '../../common/entities/versioning.entity';

@Entity('el_result_party')
export class PartyResultEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @ManyToOne(type => TerritoryEntity, territory => territory.id)
    @JoinColumn({ name: 'to_id' })
    territory: TerritoryEntity;

    @ManyToOne(type => PartyEntity, party => party.id)
    @JoinColumn({ name: 'party_id' })
    party: PartyEntity;

    @Column('integer', { default: 0 })
    votes: number;

    @Column(type => Versioning)
    v: Versioning;
}

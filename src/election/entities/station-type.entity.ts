import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { StaticDataEntity } from './static-data.entity';

@Entity('el_station_type_dic')
export class StationTypeEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('varchar')
    name: string;

    @OneToMany(type => StaticDataEntity, staticData => staticData.stationType)
    station: StaticDataEntity[];
}

import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { PartyEntity } from './party.entity';
import { TerritoryEntity } from '../../territory/entities/territory.entity';
import { CandidateResultEntity } from './candidate-result.entity';

@Entity('el_candidate')
export class CandidateEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('varchar', { name: 'full_name', length: 200 })
    fullName: string;

    @ManyToOne(type => PartyEntity, party => party.id, { eager: false })
    @JoinColumn({ name: 'party' })
    party: PartyEntity;

    @ManyToOne(type => TerritoryEntity, territory => territory.id, { eager: false })
    @JoinColumn({ name: 'county' })
    county: TerritoryEntity;

    @OneToMany(type => CandidateResultEntity, result => result.candidate)
    result: CandidateResultEntity[];
}

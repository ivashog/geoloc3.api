import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { PartyEntity } from './party.entity';

@Entity('el_list_candidates')
export class CandidatesListEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('varchar', { name: 'full_name' })
    fullName: string;

    @Index('IDX_EL_CAND_LIST__PARTY')
    @ManyToOne(type => PartyEntity, party => party.id, { eager: false })
    @JoinColumn({ name: 'party' })
    party: PartyEntity;

    @Column('integer', { name: 'list_number' })
    lisNumber: number;
}

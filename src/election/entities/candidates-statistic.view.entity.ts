import { ViewColumn, ViewEntity } from 'typeorm';

@ViewEntity({
    name: 'el_candidates_statistic',
    expression: `
        WITH counties_by_regions AS (
            SELECT 
                region, 
                count(*) AS counties 
            FROM geoloc3.to_territory 
            WHERE type_id = 4 
            GROUP BY region
        ), candidates_by_region AS (
            SELECT 
                t.region, 
                count(*) AS candidates
            FROM geoloc3.el_candidate cand
            INNER JOIN geoloc3.to_territory t
                ON t.id = cand.county
            GROUP BY t.region
        ), candidates_by_county AS (
            SELECT 
                t.county, 
                count(*) AS candidates
            FROM geoloc3.el_candidate cand
            INNER JOIN geoloc3.to_territory t
                ON t.id = cand.county
            GROUP BY t.county
        )
        SELECT
            terr.region AS id,
            terr.name,
            terr.region AS region,
            2 AS level,
            cbr.counties::int AS "countiesCount",
            COALESCE(cand.candidates, 0)::int AS "candidatesCount"
        FROM geoloc3.to_territory terr
        INNER JOIN counties_by_regions cbr
            ON terr.region = cbr.region AND terr.type_id = 2
        LEFT JOIN candidates_by_region cand
            ON terr.region = cand.region
        UNION ALL
        SELECT
            terr.county AS id,
            terr.name,
            terr.region AS region,
            4 AS level,
            1 AS "countiesCount",
            COALESCE(cand.candidates, 0)::int AS "candidatesCount"
        FROM geoloc3.to_territory terr
        LEFT JOIN candidates_by_county cand
            ON terr.county = cand.county
        WHERE terr.type_id = 4;
    `,
})
export class CandidatesStatisticViewEntity {
    @ViewColumn()
    id: number;

    @ViewColumn()
    name: string;

    @ViewColumn()
    region: string;

    @ViewColumn()
    level: number;

    @ViewColumn()
    countiesCount: number;

    @ViewColumn()
    candidatesCount: number;
}

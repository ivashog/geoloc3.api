import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TerritoryEntity } from '../../territory/entities/territory.entity';
import { CandidateEntity } from './candidate.entity';
import { Versioning } from '../../common/entities/versioning.entity';

@Entity('el_result_candidate')
export class CandidateResultEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @ManyToOne(type => TerritoryEntity, territory => territory.id)
    @JoinColumn({ name: 'to_id' })
    territory: TerritoryEntity;

    @ManyToOne(type => CandidateEntity, candidate => candidate.id)
    @JoinColumn({ name: 'candidate_id' })
    candidate: CandidateEntity;

    @Column('integer', { default: 0 })
    votes: number;

    @Column(type => Versioning)
    v: Versioning;
}

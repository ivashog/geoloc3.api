import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { PartyResultEntity } from './party-result.entity';

@Entity('el_party')
export class PartyEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('varchar', { length: 200 })
    name: string;

    @Column('varchar', { length: 20 })
    color: string;

    @Column('varchar', { name: 'full_name', length: 256 })
    fullName: string;

    @Column('integer', { name: 'list_number' })
    listNumber: string;

    @OneToMany(type => PartyResultEntity, result => result.party)
    result: PartyResultEntity[];
}

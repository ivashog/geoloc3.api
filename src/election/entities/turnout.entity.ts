import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TerritoryEntity } from '../../territory/entities/territory.entity';
import { TurnoutIntervalEntity } from './turnout-interval.entity';
import { Versioning } from '../../common/entities/versioning.entity';

@Entity('el_monitoring_turnout')
@Index('UQ_EL_MONIT_TURN__TO_ID_INT_ID', ['territory', 'interval'], { unique: true })
export class TurnoutEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Index('IDX_EL_MONIT_TURN__TO_ID')
    @ManyToOne(type => TerritoryEntity, territory => territory.id)
    @JoinColumn({ name: 'to_id' })
    territory: TerritoryEntity;

    @Index('IDX_EL_MONIT_TURN__INT_ID')
    @ManyToOne(type => TurnoutIntervalEntity, interval => interval.id)
    @JoinColumn({ name: 'interval_id' })
    interval: TurnoutIntervalEntity;

    @Column('integer')
    turnout: number;

    @Column(type => Versioning)
    v: Versioning;
}

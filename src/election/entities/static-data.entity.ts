import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { TerritoryTypeEntity } from '../../territory/entities/territory-type.entity';
import { TerritoryEntity } from '../../territory/entities/territory.entity';
import { StationTypeEntity } from './station-type.entity';

@Entity('el_static_data')
export class StaticDataEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @OneToOne(type => TerritoryEntity, territory => territory.id, { eager: false })
    @JoinColumn({ name: 'to_id' })
    territory: TerritoryEntity;

    @ManyToOne(type => TerritoryTypeEntity, type => type.id)
    @JoinColumn({ name: 'type_id' })
    type: TerritoryTypeEntity;

    @Column('integer', { nullable: true })
    counties: number;

    @Column('integer', { nullable: true })
    districts: number;

    @Column('integer', { nullable: true })
    stations: number;

    @Column('integer', { nullable: true })
    voters: number;

    @Column('varchar', { nullable: true })
    address: string;

    @Column('varchar', { nullable: true })
    phone: string;

    @Column('varchar', { nullable: true })
    email: string;

    @Column('varchar', { nullable: true })
    boundaries: string;

    @Column('varchar', { name: 'place_descr', nullable: true })
    placeDescr: string;

    @ManyToOne(type => StationTypeEntity, station => station.id)
    @JoinColumn({ name: 'station_type_id' })
    stationType: StationTypeEntity;
}

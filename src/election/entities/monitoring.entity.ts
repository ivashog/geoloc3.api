import { Column, Entity, Index, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { TerritoryEntity } from '../../territory/entities/territory.entity';
import { Versioning } from '../../common/entities/versioning.entity';

@Entity('el_monitoring_general')
export class MonitoringEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Index({ unique: true })
    @OneToOne(type => TerritoryEntity, territory => territory.id)
    @JoinColumn({ name: 'to_id' })
    territory: TerritoryEntity;

    @Column('integer', { name: 'ballots_send_ovk_dvk', nullable: true })
    ballotsSendOvkDvk: number;

    @Column('integer', { name: 'st_boxes', nullable: true })
    stBoxes: number;

    @Column('integer', { name: 'mob_boxes', nullable: true })
    mobBoxes: number;

    @Column('integer')
    electors: number;

    @Column('integer', { name: 'electors_home', nullable: true })
    electorsHome: number;

    @Column('integer', { nullable: true })
    ballots: number;

    @Column('time without time zone', { name: 'opened_at', nullable: true })
    openedAt: string;

    @Column('time without time zone', { name: 'closed_at', nullable: true })
    closedAt: string;

    @Column('time without time zone', { name: 'meeting_start_at', nullable: true })
    meetingStartAt: string;

    @Column('boolean', { name: 'is_boxes_sealed', nullable: true })
    isBoxesSealed: boolean;

    @Column('boolean', { name: 'is_mob_boxes_returned', nullable: true })
    isMobBoxesReturned: boolean;

    @Column('boolean', { name: 'is_control_list', nullable: true })
    isControlList: boolean;

    @Column(type => Versioning)
    v: Versioning;

    public toResponseObj() {
        const responseObj = Object.assign({}, this);
        const propsForDelete: string[] = ['id', 'territory', 'v'];
        propsForDelete.forEach(prop => delete responseObj[prop]);

        return responseObj;
    }
}

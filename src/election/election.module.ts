import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CandidateResultEntity } from './entities/candidate-result.entity';
import { CandidateEntity } from './entities/candidate.entity';
import { CandidatesStatisticViewEntity } from './entities/candidates-statistic.view.entity';
import { CandidateController } from './controllers/candidate.controller';
import { CandidateService } from './services/candidate.service';
import { TerritoryEntity } from '../territory/entities/territory.entity';
import { PartyEntity } from './entities/party.entity';
import { PartyController } from './controllers/party.controller';
import { PartyService } from './services/party.service';
import { CandidateResultService } from './services/candidate-result.service';
import { ResultController } from './controllers/result.controller';
import { PartyResultService } from './services/party-result.service';
import { PartyResultEntity } from './entities/party-result.entity';
import { BallotResultEntity } from './entities/ballot-result.entity';
import { BallotResultService } from './services/ballot-result.service';
import { MonitoringService } from './services/monitoring.service';
import { MonitoringEntity } from './entities/monitoring.entity';
import { TerritoryRepository } from '../territory/repositories/territory.repository';
import { MonitoringController } from './controllers/monitoring.controller';
import { ReportsService } from './services/reports.service';
import { ReportsController } from './controllers/reports.controller';
import { PartyResultReportRepository } from './repositories/party-result-report.repository';
import { StationTypeEntity } from './entities/station-type.entity';
import { StaticDataEntity } from './entities/static-data.entity';
import { StatisticRepository } from './repositories/statistic.repository';
import { TurnoutService } from './services/turnout.service';
import { TurnoutEntity } from './entities/turnout.entity';
import { TurnoutIntervalEntity } from './entities/turnout-interval.entity';
import { TurnoutReportRepository } from './repositories/turnout-report.repository';
import { CandidateResultReportRepository } from './repositories/candidate-result-report.repository';
import { CandidatesListEntity } from './entities/candidates-list.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            CandidateEntity,
            CandidateResultEntity,
            CandidatesListEntity,
            CandidatesStatisticViewEntity,
            PartyEntity,
            PartyResultEntity,
            BallotResultEntity,
            TerritoryEntity,
            TerritoryRepository,
            MonitoringEntity,
            PartyResultReportRepository,
            StaticDataEntity,
            StationTypeEntity,
            StatisticRepository,
            TurnoutEntity,
            TurnoutIntervalEntity,
            TurnoutReportRepository,
            CandidateResultReportRepository,
        ]),
    ],
    controllers: [
        CandidateController,
        PartyController,
        ResultController,
        MonitoringController,
        ReportsController,
    ],
    providers: [
        CandidateService,
        PartyService,
        CandidateResultService,
        PartyResultService,
        BallotResultService,
        MonitoringService,
        ReportsService,
        TurnoutService,
    ],
})
export class ElectionModule {}

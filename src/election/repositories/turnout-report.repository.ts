import { EntityRepository, Repository } from 'typeorm';

import { TerritoryType } from '../../common/enums/territory-types.enum';
import { TurnoutEntity } from '../entities/turnout.entity';

@EntityRepository(TurnoutEntity)
export class TurnoutReportRepository extends Repository<TurnoutEntity> {
    async calculateReport(interval: string, region?: string, county?: string): Promise<any[]> {
        let params = [interval];
        if (county) params.push(county);
        else if (region) params.push(region);

        return await this.query(
            `
            WITH stations AS (
                ${this.getStationsSql(region, county)}
            ), 
            ${region || county ? '' : ` regions AS ( ${this.getRegionNamesSql()} ),`}
            intervals AS (
                ${this.getIntervalsSql()}
            ), turnout AS (
                ${this.getAggregateTurnoutSql(interval, region, county)}
            ), report AS (
                ${this.getReportSql(region, county)}
            )
            SELECT * FROM report
            UNION ALL
            ${this.getAggregateGeneralSql(region, county)}
        `,
            params,
        );
    }

    private getStationsSql(region?: string, county?: string) {
        return `
            SELECT
                id, region, county, station, uniq_code
            FROM geoloc3.to_territory
            WHERE type_id = ${TerritoryType.Station} 
            ${!region && !county ? '' : county ? `AND county = $2` : `AND region = $2`}
        `;
    }

    private getAggregateTurnoutSql(interval: string, region?: string, county?: string) {
        const groupColumn =
            !region && !county ? 'ter.region' : county ? 'ter.station' : 'ter.county';
        return `
            SELECT
                ${groupColumn ? `${groupColumn} AS id,` : ''}
                SUM(turn.turnout)::int AS turnout,
                SUM(monit.electors)::int AS electors, -- monit.electors -- std.voters
                ((SUM(turn.turnout)::decimal / SUM(monit.electors) FILTER (WHERE turn.turnout IS NOT NULL)::decimal)*100)::decimal(5,2)::float AS percentage,
                ((SUM(monit.electors) FILTER (WHERE turn.turnout IS NOT NULL)::decimal / SUM(monit.electors)::decimal)*100)::decimal(5,2)::float AS "processedPercent",
                SUM(monit.electors) FILTER (WHERE turn.turnout IS NOT NULL)::int AS "processedElectors",
                COUNT(*)::int AS "totalStations",
                COUNT(*) FILTER (WHERE turn.turnout IS NOT NULL)::int AS "sendedStations"
            FROM stations ter 
            -- replace on 
            LEFT JOIN geoloc3.el_monitoring_general monit
                ON monit.to_id = ter.id
            -- LEFT JOIN geoloc3.el_static_data std
            --     ON std.to_id = ter.id
            LEFT JOIN geoloc3.el_monitoring_turnout turn
                ON turn.to_id = ter.id
            LEFT JOIN geoloc3.el_m_turnout_interval_dic int
                ON turn.interval_id = int.id
            WHERE int.name = $1 OR int.name  IS NULL
            ${groupColumn ? `GROUP BY ${county ? 'ter.uniq_code, ter.station' : groupColumn}` : ''}
        `;
    }

    private getReportSql(region?: string, county?: string) {
        return `
            SELECT
                turn.id,
                (${
                    !region && !county
                        ? 'r.name'
                        : county
                        ? "'Виборча дільниця №' || turn.id"
                        : "'Виборчий округ №' || turn.id"
                })::varchar AS name,
                turn.electors::int,
                turn.turnout::int,
                turn.percentage::float AS "turnoutPercent",
                (CASE
                    WHEN turn.percentage > int.max_percent 
                        OR turn.percentage < int.min_percent THEN true
                    WHEN turn.percentage IS NULL THEN null
                    ELSE false
                END)::varchar AS "isAbnormal",
                (CASE
                    WHEN turn.percentage > int.max_percent THEN 'greater'
                    WHEN turn.percentage < int.min_percent THEN 'less'
                    WHEN turn.percentage IS NULL THEN null
                    ELSE 'normal'
                END)::varchar AS "abnormalStatus",
                turn."processedElectors"::int,
                turn."processedPercent"::float,
                turn."totalStations"::int,
                turn."sendedStations"::int
            FROM intervals AS int, turnout AS turn 
            ${!region && !county ? 'LEFT JOIN regions r ON r.id = turn.id' : ''}
            ORDER BY turn.id
        `;
    }

    private getAggregateGeneralSql(region?: string, county?: string) {
        const getTerritoryName = (region: string, county: string) => {
            if (!region && !county) return `'Україна'`;
            else
                return `(
                    SELECT ${
                        county ? `'Виборчий округ ' || name AS name` : `name`
                    } FROM geoloc3.to_territory
                    WHERE type_id = ${region ? TerritoryType.Region : TerritoryType.County}
                    AND ${county ? `county = $2` : `region = $2`}
                )`;
        };

        return `
            SELECT
                '0' AS id,
                ${`${getTerritoryName(region, county)}`} AS name,
                SUM(electors)::int AS electors,
                SUM(turnout)::int AS turnout,
                ((SUM(turnout)::decimal / SUM("processedElectors")::decimal)*100)::decimal(5,2)::float AS "turnoutPercent",
                null AS "isAbnormal",
                null AS "abnormalStatus",
                SUM("processedElectors")::int AS "processedElectors",
                ((SUM("processedElectors")::decimal / SUM(electors)::decimal)*100)::decimal(5,2)::float AS "processedPercent",
                SUM("totalStations")::int AS "totalStations",
                SUM("sendedStations")::int AS "sendedStations"
            FROM report;
        `;
    }

    private getRegionNamesSql() {
        return `
            SELECT
                region AS id, name
            FROM geoloc3.to_territory
            WHERE type_id = ${TerritoryType.Region}
        `;
    }

    private getIntervalsSql() {
        return `
            SELECT 
                max_percent, min_percent
            FROM geoloc3.el_m_turnout_interval_dic
            WHERE name = $1
        `;
    }
}

import { EntityRepository, Repository } from 'typeorm';

import { StaticDataEntity } from '../entities/static-data.entity';
import { TerritoryType } from '../../common/enums/territory-types.enum';
import { HttpException, HttpStatus } from '@nestjs/common';

@EntityRepository(StaticDataEntity)
export class StatisticRepository extends Repository<StaticDataEntity> {
    async getStationInfo(uniqCode: string) {
        const stationInfo = await this.query(
            `
                SELECT 
                    tr.uniq_code AS id, tr.station AS station, stType.name AS "stationType", 
                    tr.county, r.name AS region, std.voters, std.address, std.place_descr AS place, 
                    std.boundaries, std.phone, std.email, u.id AS "userId", u.name AS "userName", 
                    u.phone AS "userPhone", userRole.displayed_name AS "userRole"
                FROM geoloc3.el_static_data std
                LEFT JOIN geoloc3.el_station_type_dic stType ON stType.id = std.station_type_id
                LEFT JOIN geoloc3.to_territory tr ON tr.id = std.to_id
                LEFT JOIN geoloc3.u_permission2to perm2to ON perm2to.to_id = tr.id
                LEFT JOIN geoloc3.u_permission perm ON perm.id = perm2to.u_p_id
                LEFT JOIN geoloc3.u_role_dic userRole ON userRole.id = perm.role_id
                LEFT JOIN geoloc3.u_user u ON u.id = perm.u_id
                LEFT JOIN geoloc3.to_territory r
                    ON r.region = tr.region 
                        AND r.type_id = $2
                WHERE tr.uniq_code = $1
        `,
            [uniqCode, TerritoryType.Region],
        );

        if (!stationInfo.length) {
            throw new HttpException(
                `Station with uniqCode '${uniqCode}' is not exist!`,
                HttpStatus.BAD_REQUEST,
            );
        }

        return {
            id: stationInfo[0].id,
            station: stationInfo[0].station,
            stationType: stationInfo[0].stationType,
            county: stationInfo[0].county,
            region: stationInfo[0].region,
            voters: stationInfo[0].voters,
            address: stationInfo[0].address,
            place: stationInfo[0].place,
            phone: stationInfo[0].phone,
            email: stationInfo[0].email,
            boundaries: stationInfo[0].boundaries,
            users: stationInfo[0].userId
                ? stationInfo.map(user => ({
                      userId: user.userId,
                      userName: user.userName,
                      userPhone: user.userPhone,
                      userRole: user.userRole,
                  }))
                : [],
        };
    }

    async getSendDataStatistic(county: string) {
        return await this.query(
            `
            WITH stations AS (
                SELECT
                    id, region, county, station
                FROM geoloc3.to_territory
                WHERE type_id = $2 AND county = $1
            ), members AS (
            SELECT 
                u.id AS u_id, 
                u.name AS user_name,
                u.phone AS user_phone,
                r.displayed_name AS user_role,
                ter.id AS territory
            FROM geoloc3.u_user u
            LEFT JOIN geoloc3.u_permission perm ON perm.u_id = u.id
            LEFT JOIN geoloc3.u_role_dic r ON r.id = perm.role_id
            LEFT JOIN geoloc3.u_permission2to perm2ter ON perm2ter.u_p_id = perm.id
            LEFT JOIN geoloc3.to_territory ter ON ter.id = perm2ter.to_id
            WHERE u.is_deleted <> true 
                AND perm.can_edit = true
                AND r.group_id = 2
            )
            SELECT
                st.station AS id,
                'Виборча дільниця №' || st.station AS name,
                memb.user_name AS "userName",
                memb.user_role AS "userRole",
                memb.user_phone AS "userPhone",
                (CASE 
                    WHEN monit.opened_at IS NULL THEN false ELSE true
                END)::boolean AS "isSendOpened",
                COUNT(turn.turnout) FILTER (WHERE int.name = '11:00')::int::boolean AS "isSendTurnout11",
                COUNT(turn.turnout) FILTER (WHERE int.name = '15:00')::int::boolean AS "isSendTurnout15",
                COUNT(turn.turnout) FILTER (WHERE int.name = '20:00')::int::boolean AS "isSendTurnout20",
                (CASE 
                    WHEN monit.closed_at IS NULL THEN false ELSE true
                END)::boolean AS "isSendClosed",
                (CASE 
                    WHEN ball.voters_participated IS NULL THEN false ELSE true
                END)::boolean AS "isSendBallotResult",
                (CASE 
                    WHEN party.votes IS NULL THEN false ELSE true
                END)::boolean AS "isSendPartyResult",
                (CASE 
                    WHEN cand.votes IS NULL THEN false ELSE true
                END)::boolean AS "isSendCandResult"
            FROM stations st
            LEFT JOIN members memb ON st.id = memb.territory
            LEFT JOIN geoloc3.el_monitoring_general monit 
                ON st.id = monit.to_id
            LEFT JOIN geoloc3.el_monitoring_turnout turn 
                ON st.id = turn.to_id
            LEFT JOIN geoloc3.el_m_turnout_interval_dic int 
                ON turn.interval_id = int.id
            LEFT JOIN geoloc3.el_result_ballot ball 
                ON st.id = ball.to_id
            LEFT JOIN geoloc3.el_result_party party
                ON st.id = party.to_id 
                    AND party.party_id = (SELECT id FROM geoloc3.el_party ORDER BY id LIMIT 1)
            LEFT JOIN geoloc3.el_result_candidate cand
                ON st.id = cand.to_id 
                    AND cand.candidate_id = (
                        SELECT cn.id FROM geoloc3.el_candidate cn
                        LEFT JOIN geoloc3.to_territory tr ON tr.id = cn.county 
                        WHERE tr.county = $1
                        ORDER BY cn.id LIMIT 1
                    )
            GROUP BY st.station, memb.user_name, memb.user_role, memb.user_phone, monit.opened_at, monit.closed_at, 
                     ball.voters_participated, party.votes, cand.votes
            ORDER BY st.station
        `,
            [county, TerritoryType.Station],
        );
    }
}

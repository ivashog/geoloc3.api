import { EntityRepository, Repository } from 'typeorm';

import { CandidateResultEntity } from '../entities/candidate-result.entity';
import { TerritoryType } from '../../common/enums/territory-types.enum';

@EntityRepository(CandidateResultEntity)
export class CandidateResultReportRepository extends Repository<CandidateResultEntity> {
    async getRegionMultCandReport() {
        return this.query(`
            WITH candidates_by_county AS (
                SELECT
                    COALESCE(MAX(cand.party)::int, 0) AS "partyId",
                    MAX(ter.region::numeric)::varchar AS region,
                    MAX(ter.county::numeric)::varchar AS county,
                    MAX(res.candidate_id)::int AS "candidateId",
                    MAX(cand.full_name)::varchar AS "candidateName",
                    (CASE
                        WHEN party.name IS NULL AND res.candidate_id IS NOT NULL THEN 'Самовисування'
                        WHEN res.candidate_id IS NULL THEN null
                        ELSE party.name
                    END)::varchar AS "partyName",
                    SUM(res.votes)::int AS votes
                FROM geoloc3.el_result_candidate res
                LEFT JOIN geoloc3.to_territory ter ON res.to_id = ter.id
                LEFT JOIN geoloc3.el_candidate cand ON res.candidate_id = cand.id
                LEFT JOIN geoloc3.el_party party ON cand.party = party.id
                GROUP BY res.candidate_id, party.name
            ), candidates_by_region AS (
                SELECT  * FROM (
                    SELECT
                        t."partyId",
                        ter.region AS id,
                        ter.name AS name,
                        t.region, t.county, t."candidateId", t."candidateName", t."partyName", t.votes,
                        ((t.votes::decimal / SUM(t.votes) OVER (PARTITION BY t.county)::decimal)*100)::decimal(5,2)::float AS percentage,
                        DENSE_RANK() OVER (
                            PARTITION BY t.county 
                            ORDER BY t.votes DESC
                        )::int AS position
                    FROM candidates_by_county t
                    LEFT JOIN geoloc3.to_territory ter 
                        ON t.region = ter.region AND ter.type_id = 2
                ) x
                WHERE x.position = 1
                ORDER BY x.region, x.county
            )
            SELECT * FROM candidates_by_region
            UNION ALL
            SELECT
                DISTINCT g."partyId",
                '0' AS id,
                'Україна' AS name,
                null AS region, null AS county, null::int AS "candidateId", null AS "candidateName",--
                g."partyName",
                (SELECT COUNT(*) FROM candidates_by_region r WHERE r."partyId" = g."partyId")::int AS votes,
                (((SELECT COUNT(*) FROM candidates_by_region r WHERE r."partyId" = g."partyId")::decimal 
                  / (SELECT COUNT(*) FROM candidates_by_region )::decimal)*100)::decimal(5,2)::float AS percentage,
                null::int AS position
            FROM candidates_by_region g
        `);
    }

    async getCountyMultCandReport(region: string) {
        return this.query(
            `
            WITH candidates_by_station AS (
                SELECT
                    ter.region,
                    ter.county,
                    ter.station,
                    res.candidate_id AS "candidateId",
                    cand.full_name AS "candidateName",
                    cand.party AS "partyId",
                    (CASE
                        WHEN party.name IS NULL AND res.candidate_id IS NOT NULL THEN 'Самовисування'
                        WHEN res.candidate_id IS NULL THEN null
                        ELSE party.name
                    END)::varchar AS "partyName",
                    res.votes,
                    (CASE
                        WHEN res.votes <= 0 THEN null
                        ELSE (res.votes::decimal / SUM(res.votes) OVER sum_window::decimal) * 100
                    END)::decimal(5,2)::float AS percentage,
                    DENSE_RANK() OVER (
                    PARTITION BY ter.station
                        ORDER BY res.votes DESC
                    )::int AS position
                FROM geoloc3.el_result_candidate res
                FULL JOIN geoloc3.to_territory ter ON res.to_id = ter.id
                LEFT JOIN geoloc3.el_candidate cand ON res.candidate_id = cand.id
                LEFT JOIN geoloc3.el_party party ON cand.party = party.id
                WHERE ter.station IS NOT NULL AND ter.region = $1
                WINDOW sum_window AS (PARTITION BY ter.station)
            ), candidates_by_counties AS (
                SELECT
                    t.*,
                    DENSE_RANK() OVER (
                        PARTITION BY t.county
                            ORDER BY t.votes DESC
                    )::int AS position
                FROM (
                    SELECT 
                        DISTINCT "candidateId",
                        "candidateName",
                        "partyId",
                        "partyName",
                         region,
                         county,
                        SUM(votes) OVER (
                            PARTITION BY county, "candidateId"
                        )::int AS votes,
                        ((
                            SUM(votes) OVER (
                                PARTITION BY county, "candidateId"
                            )::decimal 
                            / 
                            SUM(votes) OVER (
                                PARTITION BY county
                            )::decimal
                        ) * 100 )::decimal(5,2)::float AS percentage
                    FROM candidates_by_station
                    WHERE "candidateId" IS NOT NULL
                ) t
            ), general_by_region AS (
                SELECT
                    t.*,
                    DENSE_RANK() OVER (
                        PARTITION BY t.county
                        ORDER BY t.votes DESC
                    )::int AS position
                FROM (
                    SELECT 
                        DISTINCT "candidateId",
                        "candidateName",
                        "partyId",
                        "partyName",
                        region,
                        county,
                        SUM(votes) OVER (
                            PARTITION BY region, "candidateId"
                        )::int AS votes,
                        ((
                            SUM(votes) OVER (
                                PARTITION BY region, "candidateId"
                            )::decimal 
                            / 
                            SUM(votes) OVER (
                                PARTITION BY region
                            )::decimal
                        ) * 100 )::decimal(5,2)::float AS percentage
                    FROM candidates_by_counties
                    WHERE "candidateId" IS NOT NULL
                ) t
            )
            SELECT 
                i.county AS id,
                'Виборчий округ №' || i.county AS name,
                i.*,
                (SELECT 
                    CASE WHEN x."partyName" = 'Самовисування' THEN 0 ELSE x."partyId" END
                FROM candidates_by_counties x 
                WHERE x.position = 1 
                    AND x.county = i.county
                LIMIT 1) AS "paintValue"
            FROM candidates_by_counties i
            WHERE position <= 5
            UNION ALL
            SELECT 
                '0' AS id,
                (SELECT name FROM geoloc3.to_territory WHERE type_id = $2 AND region = $1) AS name,
                g.*,
                null AS "paintValue"
            FROM general_by_region g
            WHERE position = 1;
        `,
            [region, TerritoryType.Region],
        );
    }

    async getStationsMultCandReport(county: string) {
        return await this.query(
            `
            WITH candidates_by_station AS (
                SELECT
                    ter.region,
                    ter.county,
                    ter.station,
                    res.candidate_id AS "candidateId",
                    cand.full_name AS "candidateName",
                    cand.party AS "partyId",
                    (CASE
                        WHEN party.name IS NULL AND res.candidate_id IS NOT NULL THEN 'Самовисування'
                        WHEN res.candidate_id IS NULL THEN null
                        ELSE party.name
                    END)::varchar AS "partyName",
                    res.votes,
                    ((  
                        res.votes::decimal 
                        / 
                        SUM(res.votes) OVER (
                            PARTITION BY ter.station
                        )::decimal
                    ) * 100)::decimal(5,2)::float AS percentage,
                    DENSE_RANK() OVER (
                        PARTITION BY ter.station
                        ORDER BY res.votes DESC
                    )::int AS position
                FROM geoloc3.el_result_candidate res
                FULL JOIN geoloc3.to_territory ter ON res.to_id = ter.id
                LEFT JOIN geoloc3.el_candidate cand ON res.candidate_id = cand.id
                LEFT JOIN geoloc3.el_party party ON cand.party = party.id
                WHERE ter.station IS NOT NULL AND ter.county = $1
            ), general_by_county AS (
                SELECT
                    t.*,
                    DENSE_RANK() OVER (
                        ORDER BY t.votes DESC
                    )::int AS position
                FROM (
                    SELECT
                        DISTINCT "candidateId" AS "candidateId",
                        "candidateName",
                        "partyId",
                        "partyName",
                        SUM(votes) OVER (
                        PARTITION BY "candidateId"
                        )::int AS votes,
                        (
                            (  
                                SUM(votes) OVER (
                                    PARTITION BY "candidateId"
                                )::decimal
                                / 
                                (SELECT 
                                    SUM(votes)::int AS total_votes 
                                FROM candidates_by_station)::decimal 
                            ) * 100
                        )::decimal(5,2)::float AS percentage
                    FROM candidates_by_station
                    WHERE "candidateId" IS NOT NULL
                ) t
            )
            SELECT 
                i.station AS id,
                'Виборча дільниця №' || i.station AS name,
                i.*,
                (SELECT 
                    CASE WHEN x."partyName" = 'Самовисування' THEN 0 ELSE x."partyId" END
                FROM candidates_by_station x 
                WHERE x.position = 1 
                    AND x.station = i.station
                LIMIT 1) AS "paintValue"  
            FROM candidates_by_station i
            --WHERE i.position <= 5
            UNION ALL
            SELECT
                '0' AS id,
                'Виборчий округ №' || $1 AS name,
                (SELECT region FROM candidates_by_station LIMIT 1) AS region,
                (SELECT county FROM candidates_by_station LIMIT 1) AS county,
                null AS station,
                g.*,
                null AS "paintValue"
            FROM general_by_county g
        `,
            [county],
        );
    }
}

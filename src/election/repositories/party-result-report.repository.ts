import { EntityRepository, Repository } from 'typeorm';

import { TerritoryType } from '../../common/enums/territory-types.enum';
import { PartyResultEntity } from '../entities/party-result.entity';

@EntityRepository(PartyResultEntity)
export class PartyResultReportRepository extends Repository<PartyResultEntity> {
    async getRegionOnePartyReport(partyId: number): Promise<any[]> {
        return await this.query(
            `
            WITH regions AS (
                SELECT region, name
                FROM geoloc3.to_territory
                WHERE type_id = $2
            ), allvotes AS (
                SELECT 
                    r.region AS id,
                    SUM(res.votes) AS votes
                FROM geoloc3.el_result_party res
                INNER JOIN geoloc3.to_territory tr
                    ON tr.id = res.to_id
                INNER JOIN regions r 
                    ON tr.region = r.region
                GROUP BY r.region 
            )
            SELECT 
                r.region AS id,
                MAX(r.name) AS name,
                SUM(res.votes )::int AS votes,
                MAX(allvotes.votes)::int AS "totalVotes",
                (( SUM(res.votes )::decimal / MAX(allvotes.votes) )::decimal * 100 )::decimal(5,2)::float AS percentage
            FROM geoloc3.el_result_party res
            INNER JOIN geoloc3.to_territory tr
                ON tr.id = res.to_id
            INNER JOIN regions r
                ON tr.region = r.region
            INNER JOIN allvotes
                ON allvotes.id = r.region
            WHERE party_id = $1
            GROUP BY r.region
        `,
            [partyId, TerritoryType.Region],
        );
    }

    async getCountiesOnePartyReport(partyId: number, region: string): Promise<any[]> {
        return await this.query(
            `
            WITH counties AS (
                SELECT county, name
                FROM geoloc3.to_territory
                WHERE type_id = $3
            ), allvotes AS (
                SELECT 
                    c.county AS id,
                    MAX(c.name) AS name,
                    SUM(res.votes) AS votes
                FROM geoloc3.el_result_party res
                INNER JOIN geoloc3.to_territory
                    ON to_territory.id = res.to_id
                INNER JOIN counties c 
                    ON to_territory.county = c.county
                WHERE to_territory.region = $2
                GROUP BY c.county
            )
            SELECT c.county AS id,
                'Виборчий округ ' || max(c.name) AS name,
                SUM(el_result_party.votes)::int AS votes,
                MAX(allvotes.votes)::int AS "totalVotes",
                (( SUM(el_result_party.votes )::decimal / MAX(allvotes.votes) )::decimal * 100 )::decimal(5,2)::float AS percentage
            FROM geoloc3.el_result_party
            INNER JOIN geoloc3.to_territory
                ON to_territory.id = el_result_party.to_id
            INNER JOIN counties c 
                ON to_territory.county = c.county
            INNER JOIN allvotes
                ON allvotes.id = c.county
            WHERE party_id = $1
                AND to_territory.region = $2
            GROUP BY  c.county
        `,
            [partyId, region, TerritoryType.County],
        );
    }

    async getStationsOnePartyReport(partyId: number, county: string): Promise<any[]> {
        return this.query(
            `
            WITH station_votes AS (
                SELECT
                    ter.station AS id,
                    par.votes,
                    SUM(par.votes) OVER ( 
                         PARTITION BY ter.uniq_code
                    ) AS total_votes,
                    par.party_id
                FROM geoloc3.to_territory ter
                LEFT JOIN geoloc3.el_result_party par
                    ON ter.id = par.to_id
                WHERE ter.type_id = $3 
                    AND ter.county = $2
                --ORDER BY ter.region::int, ter.county::int, ter.station
            )
            SELECT
                id,
                'Виборча дільниця № ' || id AS name,
                votes::int,
                total_votes::int AS "totalVotes",
                ((votes::decimal / total_votes::decimal)*100)::decimal(5,2)::float AS percentage
            FROM station_votes
            WHERE party_id = $1 
                OR party_id IS NULL
            ORDER BY votes::int
        `,
            [partyId, county, TerritoryType.Station],
        );
    }

    async getRegionMultiplePartiesReport(parties: number[]) {
        return this.query(
            `
            WITH regions AS (
                SELECT region, name
                FROM geoloc3.to_territory
                WHERE type_id = $1
            ), 
            ${this.getPartiesWithSql(parties)} 
            party_others AS (
                ${this.getOtherPartiesVotesSql(parties)}
            )
            SELECT
                r.region AS id,
                MAX(r.name) AS name,
                SUM(res.votes) AS "totalVotes",
                ${this.getPartiesVotesPercentSql(parties)}
                oth.votes AS other_votes,
                ((MAX(oth.votes)::decimal / SUM(res.votes) )::decimal * 100 )::decimal(5,2)::float AS other_percentage
            FROM geoloc3.el_result_party res
            FULL OUTER JOIN geoloc3.to_territory tr
                ON tr.id = res.to_id
            LEFT JOIN regions r
                ON tr.region = r.region
            ${this.getPartiesVotesJoinSql(parties)}
            LEFT JOIN party_others oth
                ON oth.id = r.region
            WHERE r.region IS NOT NULL
            GROUP BY r.region, ${this.getPartiesGroupBySql(parties)} oth.votes
        `,
            [TerritoryType.Region],
        );
    }

    async getCountyMultiplePartiesReport(parties: number[], region: string) {
        return this.query(
            `
            WITH 
            ${this.getPartiesWithSql(parties, region)} 
            party_others AS (
                ${this.getOtherPartiesVotesSql(parties, region)}
            )
            SELECT
                tr.county AS id,
                'Виборчий округ №' || tr.county AS name,
                SUM(res.votes) AS "totalVotes",
                ${this.getPartiesVotesPercentSql(parties)}
                oth.votes AS other_votes,
                ((MAX(oth.votes)::decimal / SUM(res.votes) )::decimal * 100 )::decimal(5,2)::float AS other_percentage
            FROM geoloc3.el_result_party res
            FULL OUTER JOIN geoloc3.to_territory tr
                ON tr.id = res.to_id
            ${this.getPartiesVotesJoinSql(parties, region)}
            LEFT JOIN party_others oth
                ON oth.id = tr.county
            WHERE tr.region = $1 AND tr.county IS NOT NULL
            GROUP BY tr.county, ${this.getPartiesGroupBySql(parties)} oth.votes
        `,
            [region],
        );
    }

    async getStationMultiplePartiesReport(parties: number[], county: string) {
        return this.query(
            `
            WITH 
            ${this.getPartiesWithSql(parties, null, county)} 
            party_others AS (
                ${this.getOtherPartiesVotesSql(parties, null, county)}
            )
            SELECT
                tr.station AS id,
                'Виборча дільниця №' || tr.station AS name,
                SUM(res.votes) AS "totalVotes",
                ${this.getPartiesVotesPercentSql(parties)}
                oth.votes AS other_votes,
                ((MAX(oth.votes)::decimal / SUM(res.votes) )::decimal * 100 )::decimal(5,2)::float AS other_percentage
            FROM geoloc3.el_result_party res
            FULL OUTER JOIN geoloc3.to_territory tr
                ON tr.id = res.to_id
            ${this.getPartiesVotesJoinSql(parties, null, county)}
            LEFT JOIN party_others oth
                ON oth.id = tr.uniq_code
            WHERE tr.county = $1 AND tr.station IS NOT NULL
            GROUP BY tr.station, ${this.getPartiesGroupBySql(parties)} oth.votes
            `,
            [county],
        );
    }

    private getBasePartyVotesSql(condition: string, region?: string, county?: string) {
        const groupColumn =
            !region && !county ? 'r.region' : region ? 'tr.county' : county ? 'tr.uniq_code' : '';

        return `
            SELECT
                ${groupColumn} AS id,
                SUM(res.votes) AS votes
            FROM geoloc3.el_result_party res
            LEFT JOIN geoloc3.to_territory tr
                ON tr.id = res.to_id
            ${region || county ? '' : 'LEFT JOIN regions r ON tr.region = r.region'}
            WHERE res.party_id ${condition}
            GROUP BY ${groupColumn}
        `;
    }

    private getOnePartyVotesSql(partyId: number, region?: string, county?: string) {
        return this.getBasePartyVotesSql(`= ${partyId}`, region, county);
    }

    private getOtherPartiesVotesSql(parties: number[], region?: string, county?: string) {
        return this.getBasePartyVotesSql(`NOT IN (${parties})`, region, county);
    }

    private getPartiesWithSql(parties: number[], region?: string, county?: string) {
        return parties.reduce((sql, party) => {
            return `${sql}
                    party_${party} AS (
                        ${this.getOnePartyVotesSql(party, region, county)}
                    ), `;
        }, '');
    }

    private getPartiesVotesPercentSql(parties: number[]) {
        return parties.reduce((sql, party) => {
            return `${sql}
                    p${party}.votes AS p${party}_votes,
                    ((MAX(p${party}.votes)::decimal / SUM(res.votes) )::decimal * 100 )::decimal(5,2)::float AS p${party}_percentage, \n`;
        }, '');
    }

    private getPartiesVotesJoinSql(parties: number[], region?: string, county?: string) {
        const groupColumn =
            !region && !county ? 'r.region' : region ? 'tr.county' : county ? 'tr.uniq_code' : '';
        return parties.reduce((sql, party) => {
            return `${sql}
                    LEFT JOIN party_${party} p${party}
                    ON p${party}.id = ${groupColumn}\n`;
        }, '');
    }

    private getPartiesGroupBySql(parties: number[]) {
        return parties.reduce((sql, party) => sql + `p${party}.votes, `, '');
    }
}

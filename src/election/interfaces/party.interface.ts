export interface PartyInterface {
    id?: number;
    name: string;
    color: string;
}

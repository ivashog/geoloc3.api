import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CandidateEntity } from '../entities/candidate.entity';
import { CandidateResultEntity } from '../entities/candidate-result.entity';
import { TerritoryEntity } from '../../territory/entities/territory.entity';
import { GeneralRes } from '../../common/interfaces/general-res.interface';
import { resultToResponseObj } from '../../common/utils/helpers';
import { TerritoryType } from '../../common/enums/territory-types.enum';
import { InResultDto } from '../dto/in-result.dto';
import { plainToClass } from 'class-transformer';
import { TerritoryRepository } from '../../territory/repositories/territory.repository';

@Injectable()
export class CandidateResultService {
    constructor(
        @InjectRepository(CandidateResultEntity)
        private readonly candidateResultRepository: Repository<CandidateResultEntity>,
        @InjectRepository(CandidateEntity)
        private readonly candidateRepository: Repository<CandidateEntity>,
        private readonly territoryRepository: TerritoryRepository,
    ) {}

    private async checkOnUnavailableCandidates(results: any[], county): Promise<void> {
        const availableCandidates = await this.candidateRepository
            .createQueryBuilder('cand')
            .select('cand.id', 'id')
            .leftJoin('cand.county', 'to')
            .where('to.county = :county', { county })
            .getRawMany();

        const availableIds = availableCandidates.map(item => item.id);
        const currentIds = results.map(result => result.id);
        const unavailable = currentIds.filter(current => !availableIds.includes(current));

        if (unavailable.length) {
            throw new HttpException(
                `Candidates with ids:'${unavailable}' is not belong to the county '${county}'`,
                HttpStatus.BAD_REQUEST,
            );
        }
    }

    async findByStationId(stationId: string): Promise<GeneralRes> {
        const station = await this.territoryRepository.getStationByUniqId(stationId);

        const result = await this.candidateRepository
            .createQueryBuilder('cand')
            .select('cand.id', 'id')
            .addSelect('cand.fullName', 'name')
            .addSelect('result.votes', 'votes')
            .addSelect(`COALESCE(party.name, 'Самовисування')`, 'party')
            .leftJoin('cand.result', 'result', 'result.territory = :territoryId')
            .leftJoin('cand.party', 'party')
            .where(qb => {
                const subQuery = qb
                    .subQuery()
                    .select('to.id')
                    .from(TerritoryEntity, 'to')
                    .where('to.type = :type')
                    .andWhere('to.county = :county')
                    .getQuery();

                return 'cand.county = ' + subQuery;
            })
            .setParameters({
                type: TerritoryType.County,
                county: station.county,
                territoryId: station.id,
            })
            .orderBy('cand.fullName')
            .getRawMany();

        return resultToResponseObj(result, ['party']);
    }

    async upsert(input: InResultDto): Promise<GeneralRes> {
        const { station, results: candidatesResults } = input;
        const territory = await this.territoryRepository.getStationByUniqId(station);
        await this.checkOnUnavailableCandidates(candidatesResults, territory.county);

        const results = candidatesResults
            .filter(candRes => candRes.votes !== null)
            .map(candRes =>
                plainToClass(CandidateResultEntity, {
                    territory: territory.id,
                    candidate: candRes.id,
                    votes: candRes.votes,
                }),
            );

        const { raw: result } = await this.candidateResultRepository
            .createQueryBuilder()
            .insert()
            .values(results)
            .onConflict(
                `(to_id, candidate_id) DO UPDATE SET 
                    votes = EXCLUDED.votes, 
                    v_created_at = el_result_candidate.v_created_at,
                    v_updated_at = NOW(),
                    v_version = el_result_candidate.v_version + 1
                WHERE el_result_candidate.votes <> EXCLUDED.votes`,
            )
            .returning(['id', 'votes'])
            .execute();

        return resultToResponseObj(result, ['v_created_at', 'v_updated_at', 'v_version']);
    }
}

import { Repository } from 'typeorm';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { PartyEntity } from '../entities/party.entity';
import { PartyInterface } from '../interfaces/party.interface';
import { GeneralRes } from '../../common/interfaces/general-res.interface';
import { resultToResponseObj } from '../../common/utils/helpers';
import { InPartyDto } from '../dto/party.dto';

@Injectable()
export class PartyService {
    constructor(
        @InjectRepository(PartyEntity)
        private readonly partyRepository: Repository<PartyEntity>,
    ) {}

    private async checkExistPartyName(name) {
        const existParty = await this.partyRepository.findOne({ where: { name } });
        if (existParty) {
            throw new HttpException(
                `Party with name '${name}' already exist`,
                HttpStatus.BAD_REQUEST,
            );
        }
    }

    async getAll(): Promise<GeneralRes> {
        const parties = await this.partyRepository.find({
            order: { id: 'ASC' },
        });

        return resultToResponseObj(parties, ['color', 'fullName', 'listNumber']);
    }

    async getOne(id: number): Promise<PartyInterface> {
        const party = await this.partyRepository.findOne(id);
        if (!party) {
            throw new HttpException(`Party with id '${id}' is not exist`, HttpStatus.NOT_FOUND);
        }
        return party;
    }

    async create(party: InPartyDto): Promise<any> {
        await this.checkExistPartyName(party.name);

        return await this.partyRepository.save(party);
    }

    async update(id: number, party: InPartyDto): Promise<any> {
        const existParty = await this.getOne(id);
        if (existParty.name !== party.name) {
            await this.checkExistPartyName(party.name);
        }
        await this.partyRepository.update(existParty.id, party);

        return await this.getOne(existParty.id);
    }

    async delete(id: number): Promise<any> {
        const existParty = await this.getOne(id);
        return await this.partyRepository.delete(existParty.id);
    }
}

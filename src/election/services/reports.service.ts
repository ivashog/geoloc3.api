import { Injectable } from '@nestjs/common';
import { getConnection, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { PartyResultReportRepository } from '../repositories/party-result-report.repository';
import { TerritoryFilterDto } from '../../territory/dto/territory-filter.dto';
import { StatisticRepository } from '../repositories/statistic.repository';
import { PartyEntity } from '../entities/party.entity';
import { TurnoutReportRepository } from '../repositories/turnout-report.repository';
import { CandidateResultReportRepository } from '../repositories/candidate-result-report.repository';

@Injectable()
export class ReportsService {
    constructor(
        private readonly partiesReportRepository: PartyResultReportRepository,
        private readonly candidateResultRepository: CandidateResultReportRepository,
        private readonly statisticRepository: StatisticRepository,
        private readonly turnoutReportRepository: TurnoutReportRepository,
        @InjectRepository(PartyEntity)
        private readonly partyRepository: Repository<PartyEntity>,
    ) {}

    async getSendDataStatistic(county: string) {
        const report = await this.statisticRepository.getSendDataStatistic(county);

        const suffixPercent = 'Percent';
        const suffixCount = 'Count';
        const stations = report.length;

        const generalPropsObj = Object.keys(report[0])
            .filter(prop => /^is/.test(prop))
            .reduce((obj, prop) => ({ ...obj, [`${prop}${suffixCount}`]: 0 }), {});

        const generalWithCount = report.reduce((general, item) => {
            Object.keys(general).forEach(prop => {
                general[prop] += +item[prop.slice(0, -suffixCount.length)];
            });
            return general;
        }, generalPropsObj);

        const general = Object.keys(generalWithCount).reduce((general, item) => {
            Object.keys(general).forEach(prop => {
                general[prop.replace(suffixCount, suffixPercent)] = +(
                    (general[prop] / stations) *
                    100
                ).toFixed(2);
            });
            return general;
        }, generalWithCount);

        return {
            general: {
                id: county,
                name: 'Виборчий округ №' + county,
                stations,
                ...general,
            },
            items: report,
        };
    }

    async getStationInfo(uniqCode: string) {
        return await this.statisticRepository.getStationInfo(uniqCode);
    }

    async getTurnoutReport(interval: string, territoryFilter: TerritoryFilterDto) {
        const { region, county } = territoryFilter;

        let report = await this.turnoutReportRepository.calculateReport(interval, region, county);

        return {
            general: report.find(item => item.id === '0'),
            items: report.filter(item => item.id !== '0'),
        };
    }

    async getOnePartyReport(partyId: number, territoryFilter: TerritoryFilterDto) {
        const { region, county } = territoryFilter;

        let report =
            !region && !county
                ? await this.partiesReportRepository.getRegionOnePartyReport(partyId)
                : county
                ? await this.partiesReportRepository.getStationsOnePartyReport(partyId, county)
                : region
                ? await this.partiesReportRepository.getCountiesOnePartyReport(partyId, region)
                : [];

        return this.singleReportToResponse(report);
    }

    async getMultiplePartiesReport(parties: number[], territoryFilter: TerritoryFilterDto) {
        const { region, county } = territoryFilter;

        const partiesNames = await this.partyRepository
            .createQueryBuilder('p')
            .select(['p.id', 'p.name'])
            .where('id IN (:...parties)', { parties })
            .getMany();

        if (parties.length !== partiesNames.length) {
            const existParties = partiesNames.map(p => p.id);
            parties = parties.filter(party => existParties.includes(party));
        }

        const data =
            !region && !county
                ? await this.partiesReportRepository.getRegionMultiplePartiesReport(parties)
                : county
                ? await this.partiesReportRepository.getStationMultiplePartiesReport(
                      parties,
                      county,
                  )
                : region
                ? await this.partiesReportRepository.getCountyMultiplePartiesReport(parties, region)
                : [];

        const report = data.map(territory => {
            const reportItem = {
                id: territory.id,
                name: territory.name,
                totalVotes: +territory.totalVotes,
                paintValue: null,
                parties: parties
                    .map(party => {
                        const partyName = partiesNames.find(pName => pName.id === party);
                        return {
                            id: party,
                            name: partyName ? partyName.name : '',
                            votes: +territory[`p${party}_votes`],
                            percentage: +territory[`p${party}_percentage`],
                        };
                    })
                    .concat({
                        id: 0,
                        name: 'Інші',
                        votes: +territory['other_votes'],
                        percentage: +territory['other_percentage'],
                    }),
            };
            const winnerIdx = reportItem.parties.findIndex(party => {
                const maxVotes = Math.max(
                    ...reportItem.parties.filter(item => item.id !== 0).map(p => p.votes),
                );
                return maxVotes ? party.votes === maxVotes : false;
            });

            reportItem.paintValue = winnerIdx === -1 ? null : reportItem.parties[winnerIdx].id;

            return reportItem;
        });

        return this.multipleReportToResponse(report);
    }

    async getMultipleCandidatesReport(territoryFilter: TerritoryFilterDto) {
        const { region, county } = territoryFilter;

        const report =
            !region && !county
                ? await this.candidateResultRepository.getRegionMultCandReport()
                : county
                ? await this.candidateResultRepository.getStationsMultCandReport(county)
                : await this.candidateResultRepository.getCountyMultCandReport(region);

        return this.multiCandReportToResponse(
            report,
            !region && !county ? 'region' : county ? 'station' : 'county',
        );
    }

    async getSCResult() {
        const conn = getConnection();

        // Общее количество проголосовавших за партии
        const query1Result = await conn.query(
            'SELECT sum(votes) AS total_votes FROM geoloc3.el_result_party',
        );
        const { total_votes } = query1Result[0];

        // Партии, что преодолели барьер в 5%
        let more5Parties = await conn.query(`
            WITH MAIN_QUERY as (
                SELECT party_id, sum(votes) AS total_votes, max(el_party.name) as party,
                    (100 * sum(votes)::decimal / ${total_votes} )::decimal(10, 2) AS percentage
                FROM geoloc3.el_result_party
                LEFT JOIN geoloc3.el_party ON el_party.id = el_result_party.party_id
                GROUP BY party_id
            )
            
            SELECT * FROM MAIN_QUERY
            WHERE percentage >= 5
            ORDER BY percentage DESC
        `);

        // Массив кандидатов от партий
        const listCandidates = await conn.query('SELECT * FROM geoloc3.el_list_candidates');

        // Сумма голосов за прошедшие партии
        const total5 = await more5Parties.reduce((s, e) => s + +e.total_votes, 0);

        // Добавляем количество прошедших по партиям в ВРУ
        more5Parties = more5Parties.map(e => ({
            ...e,
            count: Math.round((225 * e.total_votes) / total5),
        }));

        const result = [];
        more5Parties.forEach(e => {
            for (let i = 1; i <= e.count; i++) {
                const fn = listCandidates.find(
                    item => item.party === e.party_id && item.list_number === i,
                ).full_name;
                result.push({
                    name: fn,
                    partyID: e.party_id,
                    details: `№${i} за списками партій`,
                });
            }
        });

        // Кандидаты победители в округах
        let candidatesWins = await conn.query(`
            WITH MAIN_QUERY as (
                SELECT
                    max(to_id) AS to_id,
                    max(to_territory.county::numeric) AS county,
                    max(candidate_id) AS candidate_id,
                    sum(votes) AS votes,
                    max(el_candidate.full_name) as full_name,
                    max(el_candidate.party) as party_id
                FROM geoloc3.el_result_candidate
                INNER JOIN geoloc3.to_territory ON to_territory.id = el_result_candidate.to_id
                INNER JOIN geoloc3.el_candidate ON el_candidate.id = el_result_candidate.candidate_id
                GROUP BY candidate_id
                ORDER BY county, votes DESC
            )
            
            SELECT DISTINCT ON (county) * FROM MAIN_QUERY
        `);

        candidatesWins.forEach(e => {
            result.push({
                name: `${e.full_name}`,
                partyID: e.party_id,
                details: `ВО №${e.county}`,
            });
        });

        return result;
    }

    private singleReportToResponse(report) {
        const general = report.reduce(
            (result, item) => {
                Object.keys(result).forEach(prop => (result[prop] += +item[prop]));
                return result;
            },
            { votes: 0, totalVotes: 0 },
        );

        return {
            general: {
                ...general,
                percentage: +((general.votes / general.totalVotes) * 100).toFixed(2),
            },
            items: report,
        };
    }

    private multipleReportToResponse(report) {
        const totalVotes = report.reduce((result, item) => result + item.totalVotes, 0);
        let parties = report[0].parties.map(party => ({
            id: party.id,
            name: party.name,
            votes: 0,
            percentage: 0,
        }));

        parties = parties.reduce((parties, party) => {
            const generalParty = report.reduce((gParty, reportItem) => {
                const itemParty = reportItem.parties.find(p => p.id === party.id);
                gParty.votes += +itemParty.votes;
                return gParty;
            }, party);

            generalParty.percentage = +((generalParty.votes / totalVotes) * 100).toFixed(2);

            return [...parties, generalParty];
        }, []);

        return {
            general: { totalVotes, parties },
            items: report,
        };
    }

    private mapCandidatesRowDataToObject(
        candidates: any[],
        isForGeneral: boolean = false,
        level?: string,
    ) {
        return !isForGeneral || level !== 'region'
            ? {
                  id: candidates[0].id,
                  name: candidates[0].name,
                  region: candidates[0].region,
                  county: !isForGeneral || level === 'station' ? candidates[0].county : null,
                  paintValue: !isForGeneral ? candidates[0].paintValue : null,
                  candidates: candidates[0].candidateId
                      ? candidates.map(gItem => ({
                            id: gItem.candidateId,
                            name: gItem.candidateName,
                            partyId: gItem.partyId,
                            partyName: gItem.partyName,
                            county: gItem.county,
                            votes: gItem.votes,
                            percentage: gItem.percentage,
                            position: gItem.position,
                        }))
                      : [],
              }
            : {
                  id: candidates[0].id,
                  name: candidates[0].name,
                  parties: candidates[0].partyName
                      ? candidates
                            .map(gItem => ({
                                id: gItem.partyId,
                                name: gItem.partyName,
                                candidatesCount: gItem.votes,
                                percentage: gItem.percentage,
                            }))
                            .sort((a, b) => b.candidatesCount - a.candidatesCount)
                      : [],
              };
    }

    private multiCandReportToResponse(report, level: string) {
        const generalItems = report.filter(item => item.id === '0');
        const reportItems = report.filter(item => item.id !== '0');

        const territorySet = new Set(reportItems.map(item => item[level]));

        return {
            general: this.mapCandidatesRowDataToObject(generalItems, true, level),
            items: [...territorySet].map(territory => {
                const candidates = reportItems.filter(item => item[level] === territory);

                return this.mapCandidatesRowDataToObject(candidates);
            }),
        };
    }
}

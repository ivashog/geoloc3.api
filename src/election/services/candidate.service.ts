import { Repository } from 'typeorm';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';

import { CandidateEntity } from '../entities/candidate.entity';
import { InCandidateDto } from '../dto/candidate.dto';
import { CandidatesStatisticViewEntity } from '../entities/candidates-statistic.view.entity';
import { resultToResponseObj } from '../../common/utils/helpers';
import { GeneralRes } from '../../common/interfaces/general-res.interface';
import { TerritoryType } from '../../common/enums/territory-types.enum';
import { TerritoryRepository } from '../../territory/repositories/territory.repository';

@Injectable()
export class CandidateService {
    constructor(
        @InjectRepository(CandidateEntity)
        private readonly candidatesRepository: Repository<CandidateEntity>,
        @InjectRepository(CandidatesStatisticViewEntity)
        private readonly candidatesStatisticRepository: Repository<CandidatesStatisticViewEntity>,
        private readonly territoryRepository: TerritoryRepository,
    ) {}

    async getOne(id: number): Promise<CandidateEntity> {
        const candidate = await this.candidatesRepository.findOne(id, {
            relations: ['party', 'county'],
        });
        if (!candidate) {
            throw new HttpException(`Candidate with id '${id}' is not exist`, HttpStatus.NOT_FOUND);
        }
        return candidate;
    }

    async getCandidatesByCountyId(countyId: string): Promise<any> {
        const candidates = await this.candidatesRepository
            .createQueryBuilder('c')
            .select('c.id', 'id')
            .addSelect('c.fullName', 'fullName')
            .addSelect('c.party', 'party')
            .addSelect('territory.county', 'county')
            .leftJoin('c.party', 'party')
            .leftJoin('c.county', 'territory')
            .where('territory.county = :id', { id: countyId })
            .andWhere('territory.type = :type', { type: TerritoryType.County })
            .orderBy('c.fullName', 'ASC')
            .getRawMany();

        return resultToResponseObj(candidates, ['fullName', 'county', 'party']);
    }

    async getCandidatesStatistic(region?: string): Promise<GeneralRes> {
        const filter = region
            ? { level: TerritoryType.County, region }
            : { level: TerritoryType.Region };

        const result = await this.candidatesStatisticRepository.find({
            select: ['id', 'name', 'countiesCount', 'candidatesCount'],
            where: filter,
            cache: 1000,
        });

        return resultToResponseObj(result);
    }

    async create(candidate: InCandidateDto): Promise<any> {
        const { county, fullName } = candidate;
        const { id: territoryId } = await this.territoryRepository.getCountyByCountyId(county);

        const cratedCandidate = await this.candidatesRepository.save(
            plainToClass(CandidateEntity, {
                ...candidate,
                county: territoryId,
            }),
        );

        return { ...cratedCandidate, county };
    }

    async update(id: number, candidate: InCandidateDto): Promise<any> {
        const { county, fullName } = candidate;
        const existCandidate = await this.getOne(id);
        const { id: territoryId } = await this.territoryRepository.getCountyByCountyId(county);

        const updatedCandidate = await this.candidatesRepository.save(
            plainToClass(CandidateEntity, {
                id: existCandidate.id,
                ...candidate,
                county: territoryId,
            }),
        );

        return { ...updatedCandidate, county };
    }

    async delete(id: number): Promise<any> {
        const existCandidate = await this.getOne(id);
        return await this.candidatesRepository.delete(existCandidate.id);
    }
}

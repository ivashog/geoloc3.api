import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';

import { TerritoryRepository } from '../../territory/repositories/territory.repository';
import { TurnoutEntity } from '../entities/turnout.entity';
import { InTurnoutDto, TurnoutDto } from '../dto/turnout.dto';
import { TurnoutIntervalEntity } from '../entities/turnout-interval.entity';
import { resultToResponseObj } from '../../common/utils/helpers';
import { MonitoringEntity } from '../entities/monitoring.entity';

@Injectable()
export class TurnoutService {
    constructor(
        @InjectRepository(TurnoutEntity)
        private readonly turnoutRepository: Repository<TurnoutEntity>,
        @InjectRepository(TurnoutIntervalEntity)
        private readonly turnoutIntervalRepository: Repository<TurnoutIntervalEntity>,
        private readonly territoryRepository: TerritoryRepository,
        @InjectRepository(MonitoringEntity)
        private readonly monitoringRepository: Repository<MonitoringEntity>,
    ) {}

    async findByStationId(stationId: string) {
        const station = await this.territoryRepository.getStationByUniqId(stationId);

        const turnData = await this.turnoutIntervalRepository
            .createQueryBuilder('int')
            .select('int.name', 'interval')
            .addSelect('turn.turnout', 'turnout')
            .leftJoin('int.turnoutData', 'turn', 'turn.territory = :territoryId', {
                territoryId: station.id,
            })
            .leftJoin('turn.territory', 'to')
            .getRawMany();

        const monitoring = await this.monitoringRepository.findOne({
            select: ['electors'],
            where: { territory: station.id },
        });

        return {
            turnout: turnData,
            electors: monitoring ? monitoring.electors : null,
        };
    }

    async upsert(input: InTurnoutDto) {
        const { station, turnout: turnoutData, electors } = input;
        const territory = await this.territoryRepository.getStationByUniqId(station);
        const intervals = await this.turnoutIntervalRepository.find();

        this.validateTurnoutData(turnoutData, electors);

        const turnout = turnoutData
            .filter(turnData => turnData.turnout !== null)
            .map(turnData => {
                const interval = intervals.find(int => int.name === turnData.interval);

                return plainToClass(TurnoutEntity, {
                    territory: territory.id,
                    interval: interval.id,
                    turnout: turnData.turnout,
                });
            });

        const { raw: turnoutResult } = turnout.length
            ? await this.turnoutRepository
                  .createQueryBuilder()
                  .insert()
                  .values(turnout)
                  .onConflict(
                      `(to_id, interval_id) DO UPDATE SET 
                        turnout = EXCLUDED.turnout, 
                        v_created_at = el_monitoring_turnout.v_created_at,
                        v_updated_at = NOW(),
                        v_version = el_monitoring_turnout.v_version + 1
                    WHERE el_monitoring_turnout.turnout <> EXCLUDED.turnout`,
                  )
                  .returning(['id', 'turnout'])
                  .execute()
            : { raw: [] };

        const monitoring = plainToClass(MonitoringEntity, {
            territory: territory.id,
            electors,
        });

        const { raw: monitoringResult } = monitoring.electors
            ? await this.monitoringRepository
                  .createQueryBuilder()
                  .insert()
                  .values(monitoring)
                  .onConflict(
                      `(to_id) DO UPDATE SET
                        electors = EXCLUDED.electors, 
                        v_created_at = el_monitoring_general.v_created_at,
                        v_updated_at = NOW(),
                        v_version = el_monitoring_general.v_version + 1
                    WHERE el_monitoring_general.electors <> EXCLUDED.electors`,
                  )
                  .returning(['electors'])
                  .execute()
            : { raw: [] };

        const result = monitoringResult[0]
            ? turnoutResult.concat(monitoringResult[0])
            : turnoutResult;

        return resultToResponseObj(result, ['turnout']);
    }

    private validateTurnoutData(turnoutData: TurnoutDto[], electors: number) {
        turnoutData
            .sort((a, b) => (a.interval > b.interval ? 1 : b.interval > a.interval ? -1 : 0))
            .forEach((interval, idx, turnData) => {
                if (electors === null && interval.turnout !== null) {
                    throw new HttpException(
                        'You can not set turnout without electors!',
                        HttpStatus.BAD_REQUEST,
                    );
                }
                if (electors && interval.turnout > electors) {
                    throw new HttpException(
                        'Turnout value can not be greater than electors count!',
                        HttpStatus.BAD_REQUEST,
                    );
                }
                if (
                    idx &&
                    interval.turnout !== null &&
                    interval.turnout < turnData[idx - 1].turnout
                ) {
                    throw new HttpException(
                        'Turnout value can not be less than previous interval value!',
                        HttpStatus.BAD_REQUEST,
                    );
                }
            });
    }
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';

import { PartyResultEntity } from '../entities/party-result.entity';
import { PartyEntity } from '../entities/party.entity';
import { InResultDto } from '../dto/in-result.dto';
import { resultToResponseObj } from '../../common/utils/helpers';
import { GeneralRes } from '../../common/interfaces/general-res.interface';
import { TerritoryRepository } from '../../territory/repositories/territory.repository';

@Injectable()
export class PartyResultService {
    constructor(
        @InjectRepository(PartyResultEntity)
        private readonly partyResultRepository: Repository<PartyResultEntity>,
        @InjectRepository(PartyEntity)
        private readonly partyRepository: Repository<PartyEntity>,
        private readonly territoryRepository: TerritoryRepository,
    ) {}

    async findByStationId(stationId: string): Promise<GeneralRes> {
        const territory = await this.territoryRepository.getStationByUniqId(stationId);

        const result = await this.partyRepository
            .createQueryBuilder('party')
            .select('party.id', 'id')
            .addSelect('party.name', 'name')
            .addSelect('result.votes', 'votes')
            .leftJoin('party.result', 'result', 'result.territory = :territoryId', {
                territoryId: territory.id,
            })
            .where('party.listNumber IS NOT NULL')
            .orderBy('party.listNumber')
            .getRawMany();

        return resultToResponseObj(result);
    }

    async upsert(input: InResultDto): Promise<GeneralRes> {
        const { station, results: partiesResults } = input;
        const territory = await this.territoryRepository.getStationByUniqId(station);

        const results = partiesResults
            .filter(partyRes => partyRes.votes !== null)
            .map(partyRes =>
                plainToClass(PartyResultEntity, {
                    territory: territory.id,
                    party: partyRes.id,
                    votes: partyRes.votes,
                }),
            );

        const { raw: result } = await this.partyResultRepository
            .createQueryBuilder()
            .insert()
            .values(results)
            .onConflict(
                `(to_id, party_id) DO UPDATE SET 
                    votes = EXCLUDED.votes, 
                    v_created_at = el_result_party.v_created_at,
                    v_updated_at = NOW(),
                    v_version = el_result_party.v_version + 1
                WHERE el_result_party.votes <> EXCLUDED.votes`,
            )
            .returning(['id', 'votes'])
            .execute();

        return resultToResponseObj(result, ['v_created_at', 'v_updated_at', 'v_version']);
    }
}

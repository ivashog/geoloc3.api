import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';

import { TerritoryRepository } from '../../territory/repositories/territory.repository';
import { MonitoringEntity } from '../entities/monitoring.entity';
import { MonitoringDto, InMonitoringDto } from '../dto/monitoring.dto';

import { BallotResultEntity } from '../entities/ballot-result.entity';

@Injectable()
export class MonitoringService {
    constructor(
        @InjectRepository(MonitoringEntity)
        private readonly monitoringRepository: Repository<MonitoringEntity>,
        private readonly territoryRepository: TerritoryRepository,
    ) {}

    private camelToSnakeCase(str) {
        return str.replace(/[A-Z]|\d+/g, $1 => {
            return $1.replace($1, `_${$1}`).toLowerCase();
        });
    }

    async findByStationId(stationId: string) {
        const station = await this.territoryRepository.getStationByUniqId(stationId);

        const monitData = await this.monitoringRepository
            .createQueryBuilder('monit')
            .leftJoin('monit.territory', 'to')
            .where('to.id = :territoryId', { territoryId: station.id })
            .getOne();

        return monitData ? monitData.toResponseObj() : new MonitoringDto();
    }

    async upsert(input: InMonitoringDto) {
        const { station, results: monitData } = input;
        const territory = await this.territoryRepository.getStationByUniqId(station);

        const monitoring = plainToClass(BallotResultEntity, {
            territory: territory.id,
            ...monitData,
        });

        const monitoringDbColumns = Object.keys(monitData).map(prop => this.camelToSnakeCase(prop));

        const setValuesSql = monitoringDbColumns.reduce((sql, prop) => {
            return sql + `${prop} = EXCLUDED.${prop},\n`;
        }, '');

        const whereOnConflictSql = monitoringDbColumns
            .reduce((sql, prop) => {
                return (
                    sql +
                    `(el_monitoring_general.${prop} <> EXCLUDED.${prop} 
                    OR EXCLUDED.${prop} IS NOT NULL) OR\n`
                );
            }, '')
            .slice(0, -3);

        const { generatedMaps: result } = await this.monitoringRepository
            .createQueryBuilder()
            .insert()
            .values(monitoring)
            .onConflict(
                `(to_id) DO UPDATE SET
                    ${setValuesSql}
                    v_created_at = el_monitoring_general.v_created_at,
                    v_updated_at = NOW(),
                    v_version = el_monitoring_general.v_version + 1
                WHERE ${whereOnConflictSql}`,
            )
            .returning('*')
            .execute();

        return plainToClass(MonitoringEntity, result[0]).toResponseObj();
    }
}

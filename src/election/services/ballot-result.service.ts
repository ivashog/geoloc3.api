import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';

import { BallotResultEntity } from '../entities/ballot-result.entity';
import { BallotResultDto, InBallotResultDto } from '../dto/ballot-result.dto';
import { TerritoryRepository } from '../../territory/repositories/territory.repository';

@Injectable()
export class BallotResultService {
    constructor(
        @InjectRepository(BallotResultEntity)
        private readonly ballotResultRepository: Repository<BallotResultEntity>,
        private readonly territoryRepository: TerritoryRepository,
    ) {}

    private camelToSnakeCase(str) {
        return str.replace(/[A-Z]|\d+/g, $1 => {
            return $1.replace($1, `_${$1}`).toLowerCase();
        });
    }

    async findByStationId(stationId: string) {
        const station = await this.territoryRepository.getStationByUniqId(stationId);

        const results = await this.ballotResultRepository
            .createQueryBuilder('ball')
            .leftJoin('ball.territory', 'to')
            .where('to.id = :territoryId', { territoryId: station.id })
            .getOne();

        return results ? results.toResponseObj() : new BallotResultDto();
    }

    async upsert(input: InBallotResultDto) {
        const { station, results: ballotResult } = input;
        const territory = await this.territoryRepository.getStationByUniqId(station);

        const results = plainToClass(BallotResultEntity, {
            territory: territory.id,
            ...ballotResult,
        });

        const ballotResultDbColumns = Object.keys(ballotResult).map(prop =>
            this.camelToSnakeCase(prop),
        );

        const setValuesSql = ballotResultDbColumns.reduce((sql, prop) => {
            return sql + `${prop} = EXCLUDED.${prop},\n`;
        }, '');

        const { generatedMaps: result } = await this.ballotResultRepository
            .createQueryBuilder()
            .insert()
            .values(results)
            .onConflict(
                `(to_id) DO UPDATE SET
                    ${setValuesSql}
                    v_created_at = el_result_ballot.v_created_at,
                    v_updated_at = NOW(),
                    v_version = el_result_ballot.v_version + 1
                `,
            )
            .returning('*')
            .execute();

        return plainToClass(BallotResultEntity, result[0]).toResponseObj();
    }
}

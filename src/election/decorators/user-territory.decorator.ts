import { createParamDecorator } from '@nestjs/common';

export const UserTerritory = createParamDecorator((data, req) => {
    if (!req.user || !req.user.permissions) return false;

    const actualPermission = req.user.permissions.find(perm => perm.canEdit === true);

    return actualPermission.permission2to.territory.uniqCode;
});

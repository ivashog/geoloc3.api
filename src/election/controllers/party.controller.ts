import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    UsePipes,
} from '@nestjs/common';
import { PartyService } from '../services/party.service';
import { InPartyDto } from '../dto/party.dto';
import { ValidationPipe } from '../../common/pipes/validation.pipe';
import { ParseIntPipe } from '../../common/pipes/parse-int.pipe';
import { Roles } from '../../user/decorators/roles.decorator';
import { RoleNames } from '../../common/enums/roles.enum';
import { USERS_ROLES } from '../../common/configs/roles.config';

@Controller('parties')
@UsePipes(ValidationPipe)
export class PartyController {
    constructor(private readonly partyService: PartyService) {}

    @Roles(...USERS_ROLES)
    @Get()
    getAll() {
        return this.partyService.getAll();
    }

    @Roles(...USERS_ROLES)
    @Get(':id')
    getOne(@Param('id', ParseIntPipe) id: number) {
        return this.partyService.getOne(id);
    }

    @Roles(RoleNames.ADMIN)
    @HttpCode(HttpStatus.CREATED)
    @Post()
    create(@Body() data: InPartyDto) {
        return this.partyService.create(data);
    }

    @Roles(RoleNames.ADMIN)
    @Patch(':id')
    update(@Param('id', ParseIntPipe) id: number, @Body() data: InPartyDto) {
        return this.partyService.update(id, data);
    }

    @Roles(RoleNames.ADMIN)
    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete(':id')
    delete(@Param('id', ParseIntPipe) id: number) {
        return this.partyService.delete(id);
    }
}

import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    Query,
    UsePipes,
} from '@nestjs/common';

import { CandidateService } from '../services/candidate.service';
import { InCandidateDto } from '../dto/candidate.dto';
import { CandidatesValidationPipe } from '../pipes/candidates-validation.pipe';
import { ValidationPipe } from '../../common/pipes/validation.pipe';
import { TerritoryFilterDto } from '../../territory/dto/territory-filter.dto';
import { Roles } from '../../user/decorators/roles.decorator';
import { RoleNames } from '../../common/enums/roles.enum';
import { USERS_ROLES } from '../../common/configs/roles.config';

@Controller('candidates')
export class CandidateController {
    constructor(private readonly candidatesService: CandidateService) {}

    @Roles(...USERS_ROLES)
    @Get()
    async getCandidates(@Query(new ValidationPipe()) filter: TerritoryFilterDto) {
        let { region, county } = filter;
        return county
            ? await this.candidatesService.getCandidatesByCountyId(county)
            : await this.candidatesService.getCandidatesStatistic(region);
    }

    @Roles(RoleNames.ADMIN)
    @UsePipes(ValidationPipe)
    @HttpCode(HttpStatus.CREATED)
    @Post()
    async createCandidate(@Body(CandidatesValidationPipe) candidate: InCandidateDto) {
        return await this.candidatesService.create(candidate);
    }

    @Roles(RoleNames.ADMIN)
    @UsePipes(ValidationPipe)
    @Patch(':id')
    updateCandidate(
        @Param('id') id: number,
        @Body(CandidatesValidationPipe) candidate: InCandidateDto,
    ) {
        return this.candidatesService.update(id, candidate);
    }

    @Roles(RoleNames.ADMIN)
    @UsePipes(ValidationPipe)
    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete(':id')
    deleteCandidate(@Param('id') id: number) {
        return this.candidatesService.delete(id);
    }
}

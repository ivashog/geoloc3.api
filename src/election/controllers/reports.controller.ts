import { Body, Controller, Get, Post, Param, Query, HttpCode, HttpStatus } from '@nestjs/common';
import { ReportsService } from '../services/reports.service';
import { ValidationPipe } from '../../common/pipes/validation.pipe';
import { TerritoryFilterDto } from '../../territory/dto/territory-filter.dto';
import { InPartiesListDto } from '../dto/in-parties-list.dto';
import { InIntervalDto } from '../dto/in-interval.dto';
import { Roles } from '../../user/decorators/roles.decorator';
import { USERS_ROLES } from '../../common/configs/roles.config';

@Controller('reports')
export class ReportsController {
    constructor(private readonly reportService: ReportsService) {}

    @Roles(...USERS_ROLES)
    @Get('send-data-statistic/:county')
    async getSendDataStatistic(@Param('county') county: string) {
        return await this.reportService.getSendDataStatistic(county);
    }

    @Roles(...USERS_ROLES)
    @Get('station-info/:station')
    async getStationInfo(@Param('station') uniqCode: string) {
        return await this.reportService.getStationInfo(uniqCode);
    }

    @Roles(...USERS_ROLES)
    @Get('turnout/:interval')
    async getTurnoutReport(
        @Param(ValidationPipe) param: InIntervalDto,
        @Query(ValidationPipe) territoryFilter: TerritoryFilterDto,
    ) {
        return await this.reportService.getTurnoutReport(param.interval, territoryFilter);
    }

    @Roles(...USERS_ROLES)
    @Get('one-party/:party')
    async getOnePartyReport(
        @Param('party') partyId: number,
        @Query(ValidationPipe) territoryFilter: TerritoryFilterDto,
    ) {
        return await this.reportService.getOnePartyReport(+partyId, territoryFilter);
    }

    @Roles(...USERS_ROLES)
    @HttpCode(HttpStatus.OK)
    @Post('/multiple-parties')
    async getMultiplePartiesReport(
        @Query(ValidationPipe) territoryFilter: TerritoryFilterDto,
        @Body(ValidationPipe) body: InPartiesListDto,
    ) {
        return await this.reportService.getMultiplePartiesReport(body.parties, territoryFilter);
    }

    @Roles(...USERS_ROLES)
    @Get('multiple-candidates')
    async getMultipleCandidatesReport(@Query(ValidationPipe) territoryFilter: TerritoryFilterDto) {
        return await this.reportService.getMultipleCandidatesReport(territoryFilter);
    }

    @Roles(...USERS_ROLES)
    @Get('supreme-council')
    async getSCResult() {
        return await this.reportService.getSCResult();
    }
}

import { Controller, Get, Post, Param, UsePipes, Body, HttpCode, HttpStatus } from '@nestjs/common';
import { MonitoringService } from '../services/monitoring.service';
import { ValidationPipe } from '../../common/pipes/validation.pipe';
import { InMonitoringDto } from '../dto/monitoring.dto';
import { TurnoutService } from '../services/turnout.service';
import { InTurnoutDto } from '../dto/turnout.dto';
import { Roles } from '../../user/decorators/roles.decorator';
import { RoleNames } from '../../common/enums/roles.enum';
import { MEMBERS_ROLES } from '../../common/configs/roles.config';

@Controller('monitoring')
export class MonitoringController {
    constructor(
        private readonly monitoringService: MonitoringService,
        private readonly turnoutService: TurnoutService,
    ) {}

    @Get('general/:station')
    async getMonitoringData(@Param('station') stationId: string) {
        return await this.monitoringService.findByStationId(stationId);
    }

    @Roles(RoleNames.ADMIN, ...MEMBERS_ROLES)
    @HttpCode(HttpStatus.OK)
    @Post('general')
    async upsertMonitoringData(@Body(ValidationPipe) data: InMonitoringDto) {
        return await this.monitoringService.upsert(data);
    }

    @Get('turnout/:station')
    async getTurnoutData(@Param('station') stationId: string) {
        return await this.turnoutService.findByStationId(stationId);
    }

    @Roles(RoleNames.ADMIN, ...MEMBERS_ROLES)
    @HttpCode(HttpStatus.OK)
    @Post('turnout')
    async upsertTurnoutData(@Body(ValidationPipe) data: InTurnoutDto) {
        return await this.turnoutService.upsert(data);
    }
}

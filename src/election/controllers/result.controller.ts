import { Body, Controller, Get, HttpCode, HttpStatus, Param, Post, UsePipes } from '@nestjs/common';

import { ValidationPipe } from '../../common/pipes/validation.pipe';
import { PartyResultService } from '../services/party-result.service';
import { InResultDto } from '../dto/in-result.dto';
import { CandidateResultService } from '../services/candidate-result.service';
import { BallotResultService } from '../services/ballot-result.service';
import { InBallotResultDto } from '../dto/ballot-result.dto';
import { Roles } from '../../user/decorators/roles.decorator';
import { MEMBERS_ROLES } from '../../common/configs/roles.config';
import { RoleNames } from '../../common/enums/roles.enum';

@Controller('results')
@UsePipes(ValidationPipe)
export class ResultController {
    constructor(
        private readonly partyResultService: PartyResultService,
        private readonly candidateResultService: CandidateResultService,
        private readonly ballotResultService: BallotResultService,
    ) {}

    @Get('parties/:id')
    async getPartyResults(@Param('id') stationId: string) {
        return await this.partyResultService.findByStationId(stationId);
    }

    @Get('candidates/:id')
    async getCandidateResults(@Param('id') stationId: string) {
        return await this.candidateResultService.findByStationId(stationId);
    }

    @Get('ballots/:id')
    async getBallotResults(@Param('id') stationId: string) {
        return await this.ballotResultService.findByStationId(stationId);
    }

    @Roles(RoleNames.ADMIN, ...MEMBERS_ROLES)
    @HttpCode(HttpStatus.OK)
    @Post('parties')
    upsertPartyResults(@Body() data: InResultDto) {
        return this.partyResultService.upsert(data);
    }

    @Roles(RoleNames.ADMIN, ...MEMBERS_ROLES)
    @HttpCode(HttpStatus.OK)
    @Post('candidates')
    upsertCandidateResults(@Body() data: InResultDto) {
        return this.candidateResultService.upsert(data);
    }

    @Roles(RoleNames.ADMIN, ...MEMBERS_ROLES)
    @HttpCode(HttpStatus.OK)
    @Post('ballots')
    upsertBallotResults(@Body() data: InBallotResultDto) {
        return this.ballotResultService.upsert(data);
    }
}

import { ArrayUnique, ArrayMaxSize, IsDefined, ArrayMinSize } from 'class-validator';
import { IsNumberArray } from '../../common/decorators/is-number-array.decorator';

export class InPartiesListDto {
    @ArrayUnique()
    @ArrayMinSize(2)
    @ArrayMaxSize(7)
    @IsNumberArray()
    @IsDefined()
    parties: number[];
}

import {
    IsBoolean,
    IsDefined,
    IsNotEmpty,
    IsNumber,
    IsOptional,
    IsString,
    Matches,
    Max,
    Min,
    ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

const stationUniqCodeRegexp = /\d{3}:\d{2,3}:\d{6}$/;
const timeValidationRegexp = /^(((0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]))|((0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])))$/;

export class MonitoringDto {
    @Max(3500)
    @Min(1)
    @IsNumber()
    @IsOptional()
    ballotsSendOvkDvk: number = null;

    @Max(12)
    @Min(2)
    @IsNumber()
    @IsOptional()
    stBoxes: number = null;

    @Max(6)
    @Min(2)
    @IsNumber()
    @IsOptional()
    mobBoxes: number = null;

    @Max(3500)
    @Min(1)
    @IsNumber()
    @IsDefined()
    electors: number = null;

    @Max(3500)
    @Min(0)
    @IsNumber()
    @IsOptional()
    electorsHome: number = null;

    @Max(3500)
    @Min(1)
    @IsNumber()
    @IsOptional()
    ballots: number = null;

    @Matches(timeValidationRegexp, {
        message:
            'Invalid time format. Valid must be in format HH:MM or HH:MM:SS, but actual is $value',
    })
    @IsString()
    @IsOptional()
    openedAt: string = null;

    @Matches(timeValidationRegexp, {
        message:
            'Invalid time format. Valid must be in format HH:MM or HH:MM:SS, but actual is $value',
    })
    @IsString()
    @IsOptional()
    closedAt: string = null;

    @Matches(timeValidationRegexp, {
        message:
            'Invalid time format. Valid must be in format HH:MM or HH:MM:SS, but actual is $value',
    })
    @IsString()
    @IsOptional()
    meetingStartAt: string = null;

    @IsBoolean()
    @IsOptional()
    isBoxesSealed: boolean = null;

    @IsBoolean()
    @IsOptional()
    isMobBoxesReturned: boolean = null;

    @IsBoolean()
    @IsOptional()
    isControlList: boolean = null;
}

export class InMonitoringDto {
    @Matches(stationUniqCodeRegexp, {
        message:
            'Invalid station uniqCode. Valid mast be in format NNN:NN(N):NNNNNN, but actual is $value',
    })
    @IsString()
    @IsDefined()
    station: string;

    @Type(() => MonitoringDto)
    @ValidateNested()
    @IsNotEmpty()
    @IsDefined()
    results: MonitoringDto = new MonitoringDto();
}

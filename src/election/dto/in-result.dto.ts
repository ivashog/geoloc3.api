import {
    IsDefined,
    IsNumber,
    IsString,
    Matches,
    Min,
    Max,
    ValidateNested,
    IsArray,
    ArrayNotEmpty,
} from 'class-validator';
import { Type } from 'class-transformer';

const stationUniqCodeRegexp = /\d{3}:\d{2,3}:\d{6}$/;

export class InResultDto {
    @Matches(stationUniqCodeRegexp, {
        message:
            'Invalid station uniqCode. Valid mast be in format NNN:NN(N):NNNNNN, but actual is $value',
    })
    @IsString()
    @IsDefined()
    station: string;

    @Type(() => ResultDto)
    @ValidateNested({ each: true })
    @ArrayNotEmpty()
    @IsArray()
    @IsDefined()
    results: ResultDto[];
}

class ResultDto {
    @IsNumber()
    @IsDefined()
    id: number;

    @Max(3500)
    @Min(0)
    @IsNumber()
    @IsDefined()
    votes: number;
}

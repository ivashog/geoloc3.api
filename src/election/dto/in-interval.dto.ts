import { IsDefined, IsString, Matches } from 'class-validator';

const intervals = ['11:00', '15:00', '20:00'];
const intervalValidationRegexp = new RegExp(`^(${intervals.join('|')})$`);

export class InIntervalDto {
    @Matches(intervalValidationRegexp, {
        message: 'Invalid interval $value',
    })
    @IsString()
    @IsDefined()
    interval: string = null;
}

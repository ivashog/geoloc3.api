import {
    IsDefined,
    IsHexColor,
    IsNotEmpty,
    IsNumber,
    Equals,
    IsString,
    IsOptional,
} from 'class-validator';

class PartyDto {
    @IsNumber()
    @IsOptional()
    id: number;

    @IsNotEmpty()
    @IsString()
    @IsDefined()
    name: string;

    @IsHexColor()
    @IsString()
    @IsDefined()
    color: string;
}

export class InPartyDto extends PartyDto {
    @Equals(undefined, {
        message: `You can not change row ID, delete 'id' from request body!`,
    })
    id: undefined;
}

import {
    IsDefined,
    IsNotEmpty,
    IsString,
    Matches,
    Min,
    Max,
    ValidateNested,
    IsNumber,
    IsOptional,
} from 'class-validator';
import { Type } from 'class-transformer';

const stationUniqCodeRegexp = /\d{3}:\d{2,3}:\d{6}$/;

export class BallotResultDto {
    @Max(3500)
    @Min(0)
    @IsNumber()
    @IsOptional()
    ballotsReceived: number = null;

    @Max(3500)
    @Min(0)
    @IsNumber()
    @IsOptional()
    votersEndVoting: number = null;

    @Max(3500)
    @Min(0)
    @IsNumber()
    @IsOptional()
    votersHomePrev: number = null;

    @Max(3500)
    @Min(0)
    @IsNumber()
    @IsOptional()
    unusedBallots: number = null;

    @Max(3500)
    @Min(0)
    @IsNumber()
    @IsOptional()
    votersStation: number = null;

    @Max(3500)
    @Min(0)
    @IsNumber()
    @IsOptional()
    votersHomeFact: number = null;

    @Max(3500)
    @Min(0)
    @IsNumber()
    @IsOptional()
    totalReceived: number = null;

    @Max(3500)
    @Min(0)
    @IsNumber()
    @IsOptional()
    ballotsNotAccounted: number = null;

    @Max(3500)
    @Min(0)
    @IsNumber()
    @IsOptional()
    votersParticipated: number = null;

    @Max(3500)
    @Min(0)
    @IsNumber()
    @IsOptional()
    invalidBallots: number = null;

    @Max(3500)
    @Min(-3500)
    @IsNumber()
    @IsOptional()
    ballotsActs7375: number = null;

    @Max(3500)
    @Min(-3500)
    @IsNumber()
    @IsOptional()
    ballotsActs78: number = null;
}

export class InBallotResultDto {
    @Matches(stationUniqCodeRegexp, {
        message:
            'Invalid station uniqCode. Valid mast be in format NNN:NN(N):NNNNNN, but actual is $value',
    })
    @IsString()
    @IsDefined()
    station: string = '';

    @Type(() => BallotResultDto)
    @ValidateNested()
    @IsNotEmpty()
    @IsDefined()
    results: BallotResultDto = new BallotResultDto();
}

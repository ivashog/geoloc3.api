import {
    Equals,
    IsDefined,
    IsNotEmpty,
    IsNotIn,
    IsNumber,
    IsOptional,
    IsString,
} from 'class-validator';

class CandidateDto {
    @IsNumber()
    @IsOptional()
    id?: number;

    @IsNotEmpty()
    @IsString()
    @IsDefined()
    fullName: string;

    @IsNotIn([0])
    @IsNumber()
    @IsOptional()
    party?: number;

    @IsNotEmpty()
    @IsString()
    @IsDefined()
    county: string;
}

export class InCandidateDto extends CandidateDto {
    @Equals(undefined, {
        message: `You can not change row ID, delete 'id' from request body!`,
    })
    id: undefined;
}

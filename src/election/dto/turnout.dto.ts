import {
    ArrayMaxSize,
    ArrayMinSize,
    ArrayNotEmpty,
    IsArray,
    IsDefined,
    IsNotEmpty,
    IsNumber,
    IsOptional,
    IsString,
    Matches,
    Max,
    Min,
    ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

const stationUniqCodeRegexp = /\d{3}:\d{2,3}:\d{6}$/;
const intervals = ['11:00', '15:00', '20:00'];
const intervalValidationRegexp = new RegExp(`^(${intervals.join('|')})$`);

export class TurnoutDto {
    @Matches(intervalValidationRegexp, {
        message: 'Invalid interval $value',
    })
    @IsString()
    @IsDefined()
    interval: string = null;

    @Max(3500)
    @Min(0)
    @IsNumber()
    @IsOptional()
    turnout: number = null;
}

export class InTurnoutDto {
    @Matches(stationUniqCodeRegexp, {
        message:
            'Invalid station uniqCode. Valid mast be in format NNN:NN(N):NNNNNN, but actual is $value',
    })
    @IsString()
    @IsDefined()
    station: string;

    @Type(() => TurnoutDto)
    @ValidateNested({ each: true })
    @ArrayMaxSize(3)
    @ArrayNotEmpty()
    @IsArray()
    @IsNotEmpty()
    @IsDefined()
    turnout: TurnoutDto[] = new Array(3)
        .fill(new TurnoutDto())
        .map((turn, i) => ({ ...turn, interval: intervals[i] }));

    @Max(3500)
    @Min(1)
    @IsNumber()
    @IsOptional()
    electors: number = null;
}

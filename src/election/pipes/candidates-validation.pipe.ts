import {
    ArgumentMetadata,
    HttpException,
    HttpStatus,
    Injectable,
    PipeTransform,
} from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { PartyEntity } from '../entities/party.entity';

@Injectable()
export class CandidatesValidationPipe implements PipeTransform {
    constructor(
        @InjectRepository(PartyEntity)
        private readonly partyRepository: Repository<PartyEntity>,
    ) {}
    async transform(value: any, metadata: ArgumentMetadata) {
        if (value.party) {
            const foundParty = await this.partyRepository.findOne(value.party);
            if (!foundParty)
                throw new HttpException(
                    `Party with ID ${value.party} is not exist`,
                    HttpStatus.BAD_REQUEST,
                );
        }

        return value;
    }
}

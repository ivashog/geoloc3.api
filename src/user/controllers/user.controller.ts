import {
    Controller,
    Post,
    Body,
    Patch,
    Delete,
    Get,
    Param,
    HttpCode,
    HttpStatus,
    Logger,
    Query,
} from '@nestjs/common';

import { UserService } from '../services/user.service';
import { ValidationPipe } from '../../common/pipes/validation.pipe';
import { UserTransformPipe } from '../pipes/user-transform.pipe';
import { InUserDto } from '../dto/in-user.dto';
import { ParseIntPipe } from '../../common/pipes/parse-int.pipe';
import { InMemberDto } from '../dto/in-member.dto';
import { LoginDto } from '../dto/login.dto';
import { LoginCredentialsTransformPipe } from '../pipes/login-credentials-transform.pipe';
import { Roles } from '../decorators/roles.decorator';
import { MEMBERS_ROLES, USERS_ROLES } from '../../common/configs/roles.config';
import { SetPasswordDto } from '../dto/set-password.dto';
import { InLawyerDto } from '../dto/in-lawyer.dto';
import { User } from '../decorators/user.decorator';
import { UserPermissionDto } from '../dto/user-permission.dto';
import { TerritoryFilterDto } from '../../territory/dto/territory-filter.dto';
import { RoleNames } from '../../common/enums/roles.enum';

@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Get('roles')
    async getRoles() {
        return this.userService.getRoles();
    }

    @Post('login')
    async login(
        @Body(new LoginCredentialsTransformPipe(), new ValidationPipe())
        credentials: LoginDto,
    ) {
        return await this.userService.login(credentials);
    }

    @Roles(...MEMBERS_ROLES)
    @Post('login/set-password/:code')
    async setPassword(
        @Param('code') passwordCode: string,
        @Body(new ValidationPipe()) data: SetPasswordDto,
    ) {
        return await this.userService.setPassword(data, passwordCode);
    }

    @Roles(...USERS_ROLES)
    @Get()
    async getUsersList(@Query() filter) {
        return this.userService.findAll({ group: 'users' });
    }

    @Roles(...MEMBERS_ROLES)
    @Get('members/whoiam')
    async getCurrentMemberInfo(@User('id', new ParseIntPipe()) memberId: number) {
        return await this.userService.findById(memberId, true);
    }

    @Roles(...USERS_ROLES)
    @Get('members')
    async getMembersList(
        @User('permissions') permissions: UserPermissionDto[],
        @Query(new ValidationPipe()) filter: TerritoryFilterDto,
    ) {
        let { region, county } = filter;

        // const [ {
        //     level: { name: accessLevel },
        //     permission2to: { territory }
        // }, ] = permissions;
        //
        // if (accessLevel === 'region') region = territory.region;
        // if (accessLevel === 'county') county = territory.county;

        return region && county
            ? await this.userService.findAll({ group: 'members', region, county })
            : await this.userService.getMembersStatistic(region);
    }

    @Roles(...USERS_ROLES)
    @Get(':id')
    async getOne(@Param('id', new ParseIntPipe()) userId) {
        return await this.userService.findById(userId);
    }

    @Roles(RoleNames.ADMIN)
    @HttpCode(HttpStatus.CREATED)
    @Post()
    async createUser(@Body(new UserTransformPipe(), new ValidationPipe()) input: InUserDto) {
        const { user, permissions } = input;

        return await this.userService.createUser(user, permissions);
    }

    @Roles(RoleNames.ADMIN)
    @Post('members')
    @HttpCode(HttpStatus.CREATED)
    async createMember(@Body(new UserTransformPipe(), new ValidationPipe()) input: InMemberDto) {
        const { user, permissions } = input;
        return await this.userService.createMember(user, permissions);
    }

    @Roles(RoleNames.ADMIN)
    @Patch(':id')
    async updateUser(
        @Param('id', new ParseIntPipe()) userId: number,
        @Body(new UserTransformPipe(), new ValidationPipe()) input: InUserDto,
    ) {
        const { user, permissions } = input;
        return await this.userService.updateUser(userId, user, permissions);
    }

    @Roles(RoleNames.ADMIN)
    @Patch('members/:id')
    async updateMember(
        @Param('id', new ParseIntPipe()) userId: number,
        @Body(new UserTransformPipe(), new ValidationPipe()) input: InMemberDto,
    ) {
        const { user, permissions } = input;
        return await this.userService.updateMember(userId, user, permissions);
    }

    @Roles(RoleNames.ADMIN)
    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteUser(@Param('id', new ParseIntPipe()) userId: number) {
        return await this.userService.delete(userId);
    }

    @Post('lawyers')
    async createLawyer(@Body(new UserTransformPipe(), new ValidationPipe()) input: InLawyerDto) {
        Logger.log(input);
        return input;
    }
}

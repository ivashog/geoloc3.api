import { IsString, IsNotEmpty, IsDefined } from 'class-validator';
import { SetPasswordReq } from '../interfaces/set-password-req.interface';

export class SetPasswordDto implements SetPasswordReq {
    @IsString()
    @IsNotEmpty()
    @IsDefined()
    password: string;

    @IsString()
    @IsNotEmpty()
    @IsDefined()
    passwordConfirmation: string;
}

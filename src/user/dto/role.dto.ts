import { IsString, IsNotEmpty, Length, IsDefined } from 'class-validator';

export class RoleDto {
    @Length(3, 20)
    @IsString()
    @IsNotEmpty()
    @IsDefined()
    name: string;

    @Length(3, 50)
    @IsString()
    @IsNotEmpty()
    @IsDefined()
    displayedName: string;
}

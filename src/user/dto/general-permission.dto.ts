import {IsOptional, IsBoolean, IsInt, IsNotEmpty, IsDefined} from 'class-validator';

export class GeneralPermissionDto {
    @IsInt()
    @IsNotEmpty()
    @IsDefined()
    role: number;

    @IsBoolean()
    @IsOptional()
    canEdit?: boolean = false;

    @IsBoolean()
    @IsOptional()
    isBanned?: boolean = false;
}

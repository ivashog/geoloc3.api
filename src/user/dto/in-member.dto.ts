import { Type } from 'class-transformer';

import { UserDto } from './user.dto';
import { MemberPermissionDto } from './member-permission.dto';
import { ValidateNested } from 'class-validator';

export class InMemberDto {
    @Type(() => UserDto)
    @ValidateNested()
    user: UserDto;

    @Type(() => MemberPermissionDto)
    @ValidateNested({ each: true })
    permissions: MemberPermissionDto[];
}

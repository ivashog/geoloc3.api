import { IsOptional, IsString, IsNumberString, IsDefined, IsIn, IsInt, Matches } from 'class-validator';
import { GeneralPermissionDto } from './general-permission.dto';

const stationUniqCodeRegexp = /\d{3}:\d{2,3}:\d{6}$/;

export class MemberPermissionDto extends GeneralPermissionDto {
    @IsIn([5])
    @IsInt()
    level: number = 5;

    @IsIn([3, 4, 5, 6])
    role: number = 3;

    @IsNumberString()
    @IsString()
    @IsOptional()
    region?: string = null;

    @IsNumberString()
    @IsString()
    @IsOptional()
    county?: string = null;

    @IsString()
    @IsDefined()
    station: string = null;

    @Matches(stationUniqCodeRegexp, {
        message: 'Invalid uniqCode. Valid mast be in format NNN:NN(N):NNNNNN, but actual is $value'
    })
    @IsString()
    @IsDefined()
    uniqCode: string = null;
}

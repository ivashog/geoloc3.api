import { Type } from 'class-transformer';

import { UserDto } from './user.dto';
import { UserPermissionDto } from './user-permission.dto';
import { ValidateNested } from 'class-validator';

export class InUserDto {
    @Type(() => UserDto)
    @ValidateNested()
    user: UserDto;

    @Type(() => UserPermissionDto)
    @ValidateNested({ each: true })
    permissions: UserPermissionDto[];
}

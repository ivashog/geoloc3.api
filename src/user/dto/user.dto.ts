import {
    Matches,
    IsString,
    IsMobilePhone,
    IsNotEmpty,
    Length,
    IsDefined,
    IsOptional,
} from 'class-validator';
// valid date in format YYYY-MM-DD
const dateValidationRegexp = /^(19[2-9][0-9]|20[0-4][0-9]|2010)[-/](0?[1-9]|1[0-2])[-/](0?[1-9]|[12][0-9]|3[01])$/;

export class UserDto {
    @Length(3, 200)
    @IsString()
    @IsNotEmpty()
    @IsDefined()
    name: string = null;

    //@IsMobilePhone('uk-UA')
    @Length(9)
    @IsString()
    @IsNotEmpty()
    @IsDefined()
    phone: string = null;

    @Length(5, 20)
    @IsString()
    @IsOptional()
    password: string = null;

    @Matches(/m|w/)
    @Length(1, 1)
    @IsOptional()
    gender?: string = null;

    @Matches(dateValidationRegexp, {
        message: 'dateOfBirth must be valid date in format YYYY-MM-DD and in range 1920...2010'
    })
    @IsString()
    @IsOptional()
    dateOfBirth?: string = null;
}

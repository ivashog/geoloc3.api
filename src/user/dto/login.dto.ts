import { IsString, IsNotEmpty, Length, IsDefined } from 'class-validator';
import { LoginReq } from '../interfaces/login-req.interface';

export class LoginDto implements LoginReq {
    @Length(9)
    @IsString()
    @IsNotEmpty()
    @IsDefined()
    login: string;

    @IsString()
    @IsNotEmpty()
    @IsDefined()
    password: string;
}

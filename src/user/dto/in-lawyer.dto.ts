import { Type } from 'class-transformer';

import { UserDto } from './user.dto';
import { ValidateNested } from 'class-validator';
import { LawyerPermissionDto } from './lawyer-permission.dto';

export class InLawyerDto {
    @Type(() => UserDto)
    @ValidateNested()
    user: UserDto;

    @Type(() => LawyerPermissionDto)
    @ValidateNested({ each: true })
    permissions: LawyerPermissionDto[];
}

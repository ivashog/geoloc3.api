import { IsOptional, IsString, IsNumberString, IsInt, IsNotEmpty, IsDefined, IsIn } from 'class-validator';
import { GeneralPermissionDto } from './general-permission.dto';

export class UserPermissionDto extends GeneralPermissionDto {
    @IsIn([1, 2, 4])
    @IsInt()
    @IsNotEmpty()
    @IsDefined()
    level: number = null;

    @IsIn([1, 2])
    role: number = null;

    @IsNumberString()
    @IsString()
    @IsOptional()
    region?: string = null;

    @IsNumberString()
    @IsString()
    @IsOptional()
    county?: string = null;
}

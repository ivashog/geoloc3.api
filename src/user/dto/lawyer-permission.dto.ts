import { IsString, IsDefined, IsIn, IsInt, IsNotEmpty } from 'class-validator';
import { GeneralPermissionDto } from './general-permission.dto';

export class LawyerPermissionDto extends GeneralPermissionDto {
    @IsIn([4])
    @IsInt()
    level: number = 4;

    @IsIn([7])
    role: number = 7;

    @IsString()
    @IsNotEmpty()
    @IsDefined()
    county: string = '';
}

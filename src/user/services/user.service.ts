import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UserEntity } from '../entities/user.entity';
import { PermissionEntity } from '../entities/permission.entity';
import { RoleEntity } from '../entities/role.entity';
import { Permission2toEntity } from '../entities/permission2to.entity';
import { TerritoryEntity } from '../../territory/entities/territory.entity';
import { UserDto } from '../dto/user.dto';
import { UserPermissionDto } from '../dto/user-permission.dto';
import { MemberPermissionDto } from '../dto/member-permission.dto';
import { TerritoryDto } from '../../territory/dto/territory.dto';
import { UserGeneralRes } from '../interfaces/user-res.interface';
import { MemberItemRes, UserItemRes } from '../interfaces/user-item-res.interface';
import { LoginDto } from '../dto/login.dto';
import Utils from '../../common/utils/utils';
import { SetPasswordDto } from '../dto/set-password.dto';
import { LoginRes } from '../interfaces/login-res.interface';
import { UserFilter } from '../interfaces/user-filter.interface';
import { MembersStatisticViewEntity } from '../entities/members-statistic.view.entity';
import { SetPasswordRes } from '../interfaces/set-password-res.interface';
import { UserItemResType } from '../interfaces/user-item-res.interface';
import { GeneralRes } from '../../common/interfaces/general-res.interface';

import { resultToResponseObj } from '../../common/utils/helpers';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserEntity)
        private userRepository: Repository<UserEntity>,
        @InjectRepository(PermissionEntity)
        private permissionRepository: Repository<PermissionEntity>,
        @InjectRepository(Permission2toEntity)
        private permission2toRepository: Repository<Permission2toEntity>,
        @InjectRepository(TerritoryEntity)
        private territoryRepository: Repository<TerritoryEntity>,
        @InjectRepository(RoleEntity)
        private roleRepository: Repository<RoleEntity>,
        @InjectRepository(MembersStatisticViewEntity)
        private membersStatisticRepository: Repository<MembersStatisticViewEntity>,
    ) {}

    private async getUserById(id) {
        const user = await this.userRepository.findOne({
            where: { id, isDeleted: false },
            relations: ['permissions', 'permissions.role', 'permissions.level'],
        });
        if (!user) {
            throw new HttpException(`User with id:${id} not found`, HttpStatus.NOT_FOUND);
        }
        return user;
    }

    private async checkExistUserPhone(phone) {
        const existUserPhone = await this.userRepository.findOne({
            where: { phone, isDeleted: false },
        });
        if (existUserPhone) {
            throw new HttpException(
                `User with phone ${phone} already exist!`,
                HttpStatus.BAD_REQUEST,
            );
        }
    }

    private async getTerritory(searchParams: Partial<TerritoryDto>) {
        const territory = await this.territoryRepository.findOne({
            where: searchParams,
        });
        if (!territory) {
            throw new HttpException(
                Object.keys(searchParams)
                    .reduce((str, param) => {
                        return `${str}${param}=${searchParams[param]}, `;
                    }, 'Invalid territory search params: ')
                    .slice(0, -2),
                HttpStatus.BAD_REQUEST,
            );
        }
        return territory;
    }

    private extractSearchParams(permission): Partial<TerritoryDto> {
        const { level, region = null, county = null, station = null, uniqCode = null } = permission;
        let searchParams: Partial<TerritoryDto> = { type: level, region, county };

        if (permission instanceof MemberPermissionDto) {
            searchParams = { ...searchParams, station, uniqCode };
        }

        return searchParams;
    }

    private async hasTerritoryCanEditMembers(
        territory: TerritoryEntity,
        isUpdate: boolean,
    ): Promise<boolean> {
        const canEditMembersCount = await this.userRepository
            .createQueryBuilder('user')
            .leftJoin('user.permissions', 'permission')
            .leftJoin('permission.permission2to', 'perm2to')
            .leftJoin('perm2to.territory', 'to')
            .where('user.isDeleted = :isDeleted', { isDeleted: false })
            .andWhere('permission.canEdit = :canEdit', { canEdit: true })
            .andWhere('to.id = :territory', { territory: territory.id })
            .getCount();
        return isUpdate ? canEditMembersCount > 1 : canEditMembersCount > 0;
    }

    private async validateTerritoryPermission(
        territory: TerritoryEntity,
        permission,
        isUpdate: boolean = false,
    ) {
        if (
            permission instanceof MemberPermissionDto &&
            permission.canEdit &&
            territory &&
            (await this.hasTerritoryCanEditMembers(territory, isUpdate))
        ) {
            throw new HttpException(
                `Station with uniqCode ${permission.uniqCode} already have user that can send data`,
                HttpStatus.BAD_REQUEST,
            );
        }
    }

    private async prepareNewPermissions(permissions) {
        return await Promise.all(
            permissions.map(async permission => {
                const searchParams = this.extractSearchParams(permission);
                const territory = await this.getTerritory(searchParams);
                await this.validateTerritoryPermission(territory, permission);
                const permission2to = await this.permission2toRepository.create({ territory });

                return { ...permission, permission2to };
            }),
        );
    }

    private async prepareExistPermissions(existPermissions, newPermissions) {
        return await Promise.all(
            existPermissions.map(async (permission, idx) => {
                const newPermission = newPermissions[idx];
                const searchParams = this.extractSearchParams(newPermission);
                const territory = await this.getTerritory(searchParams);
                await this.validateTerritoryPermission(territory, newPermission, true);
                const newPermission2to = await this.permission2toRepository.preload({
                    ...permission.permission2to,
                    territory,
                });

                return {
                    ...permission,
                    ...newPermission,
                    permission2to: { ...newPermission2to },
                };
            }),
        );
    }

    private async prepareNewUser(
        user: UserDto,
        permissions: UserPermissionDto[] | MemberPermissionDto[],
    ): Promise<UserEntity> {
        await this.checkExistUserPhone(user.phone);

        if (permissions[0] instanceof UserPermissionDto && !user.password) {
            throw new HttpException('For Users password is required', HttpStatus.BAD_REQUEST);
        }

        const newUserPermission = await this.prepareNewPermissions(permissions);
        return await this.userRepository.create({ ...user, permissions: newUserPermission });
    }

    private userToResponseObject(user: UserEntity): UserItemResType {
        const {
            permissions: [
                {
                    role: {
                        group: { name: userGroup },
                    },
                },
            ],
        } = user;

        let response: UserItemResType;
        switch (userGroup) {
            case 'users':
                response = user.toUserResponseObject();
                break;
            case 'members':
                response = user.toMemberToResponseObject();
                break;
            case 'lawyers':
                response = user.toLawyerToResponseObject();
        }
        return response;
    }

    async login(credentials: LoginDto): Promise<LoginRes | SetPasswordRes> {
        const { login, password } = credentials;
        const user = await this.userRepository.findOne({
            where: { phone: login },
            relations: ['permissions', 'permissions.role', 'permissions.level'],
        });

        if (!user) {
            throw new HttpException(
                `User with login(phone) '${login}' does not exist!`,
                HttpStatus.NOT_FOUND,
            );
        }

        if (user.password !== null) {
            if (await user.comparePassword(password)) {
                return user.toLoginResponse();
            } else {
                throw new HttpException(
                    `Unauthorized: wrong password '${password}' for current user!`,
                    HttpStatus.UNAUTHORIZED,
                );
            }
        } else if (login === password) {
            const passwordCode = Utils.generateRandomString(50);
            await this.userRepository.save({ ...user, passwordCode });
            return { code: passwordCode };
        } else {
            throw new HttpException(
                `You not have password and logging in first time! 
                For setup you password send your phone as a password.`,
                HttpStatus.UNAUTHORIZED,
            );
        }
    }

    async setPassword(data: SetPasswordDto, passwordCode: string) {
        const { password, passwordConfirmation } = data;

        if (password !== passwordConfirmation) {
            throw new HttpException('Passwords do not match!', HttpStatus.BAD_REQUEST);
        }

        const user = await this.userRepository.findOne({
            where: { passwordCode },
            relations: ['permissions', 'permissions.role', 'permissions.level'],
        });
        if (!user) {
            throw new HttpException(`Invalid password code!`, HttpStatus.BAD_REQUEST);
        }
        const updatedUser = await this.userRepository.preload({
            ...user,
            password,
            passwordCode: '',
        });
        const loginUser = await this.userRepository.save(updatedUser);
        return loginUser.toLoginResponse();
    }

    async findById(id: number, isLoginResponse: boolean = false) {
        const user = await this.getUserById(id);
        return isLoginResponse ? user.toLoginResponse(false) : this.userToResponseObject(user);
    }

    async findAll(filter: UserFilter = {}): Promise<UserGeneralRes> {
        let qb = this.userRepository
            .createQueryBuilder('user')
            .leftJoinAndSelect('user.permissions', 'permission')
            .leftJoinAndSelect('permission.level', 'level')
            .leftJoinAndSelect('permission.role', 'role')
            .leftJoinAndSelect('role.group', 'group')
            .leftJoinAndSelect('permission.permission2to', 'perm2to')
            .leftJoinAndSelect('perm2to.territory', 'to')
            .where('user.isDeleted = :isDeleted', { isDeleted: false });

        if (filter.group) {
            qb.andWhere(qb => {
                const subQuery = qb
                    .subQuery()
                    .select('role.id')
                    .from(RoleEntity, 'role')
                    .innerJoin('role.group', 'g', 'g.name = :name', {
                        name: filter.group,
                    })
                    .getQuery();
                return `permission.role IN ${subQuery}`;
            });
        }
        if (filter.role) {
            qb.andWhere('permission.role = :role', { role: filter.role });
        }
        if (filter.region) {
            qb.andWhere('to.region = :region', { region: filter.region });
        }
        if (filter.county) {
            qb.andWhere('to.county = :county', { county: filter.county });
        }
        let [users, usersCount]: [UserEntity[], number] = await qb.getManyAndCount();

        return {
            general: { count: usersCount },
            items: users.map(user => this.userToResponseObject(user)),
        };
    }

    async getMembersStatistic(region?: string): Promise<GeneralRes> {
        const filter = region ? { level: 4, region } : { level: 2 };
        const result = await this.membersStatisticRepository.find({
            select: [
                'id',
                'name',
                'dvkMembersCount',
                'spectatorsCount',
                'spectatorsMediaCount',
                'representativesGoCount',
                'totalMembersCount',
            ],
            where: filter,
            cache: 5000,
        });

        return resultToResponseObj(result);
    }

    async createUser(user: UserDto, permissions: UserPermissionDto[]): Promise<UserItemRes> {
        const newUser = await this.prepareNewUser(user, permissions);
        const createdUser = await this.userRepository.save(newUser);

        return createdUser.toUserResponseObject();
    }

    async createMember(user: UserDto, permissions: MemberPermissionDto[]): Promise<MemberItemRes> {
        const newMember = await this.prepareNewUser(user, permissions);
        const createdMember = await this.userRepository.save(newMember);

        return createdMember.toMemberToResponseObject();
    }

    async updateUser(id: number, user: UserDto, permissions: UserPermissionDto[]) {
        const existUser = await this.getUserById(id);
        if (existUser.phone !== user.phone) {
            await this.checkExistUserPhone(user.phone);
        }

        const updatedPermissions = await this.prepareExistPermissions(
            existUser.permissions,
            permissions,
        );

        const userForUpdate = await this.userRepository.preload({
            ...existUser,
            ...user,
            permissions: updatedPermissions,
        });

        // delete password from user for update
        if (userForUpdate.password) delete userForUpdate.password;

        const updatedUser = await this.userRepository.save(userForUpdate);
        return updatedUser.toUserResponseObject();
    }

    async updateMember(id: number, user: UserDto, permissions: MemberPermissionDto[]) {
        return await this.updateUser(id, user, permissions);
    }

    async delete(id: number) {
        const existUser = await this.getUserById(id);
        return await this.userRepository.update(id, {
            phone: `--DELETED-- ${existUser.phone}`,
            isDeleted: true,
        });
    }

    async getRoles(): Promise<RoleEntity[]> {
        return await this.roleRepository.find({ select: ['id', 'name', 'displayedName'] });
    }

    // async createLawyer()
}

export interface GeneralUserItemRes {
    id: number;
    name: string;
    phone: string;
    gender?: string;
    dateOfBirth?: string;
}

export interface UserItemRes extends GeneralUserItemRes {
    canEdit: boolean;
    level: number;
    region: string;
    county: string;
}

export interface MemberItemRes extends GeneralUserItemRes {
    canEdit: boolean;
    role: number;
    station: string;
    county?: string;
    region?: string;
}

export interface LawyerItemRes extends GeneralUserItemRes {
    counties: string[];
}

export declare type UserItemResType = UserItemRes | MemberItemRes | LawyerItemRes;

export interface UserFilter {
    group?: string;
    role?: number;
    region?: string;
    county?: string;
}

export interface SetPasswordReq {
    password: string;
    passwordConfirmation: string;
}

export interface LoginRes {
    id: number;
    role: number;
    token?: string;
    access: {
        canEdit: boolean;
        level?: number;
        region?: string;
        county?: string;
        station?: string;
        counties?: string[];
    };
}

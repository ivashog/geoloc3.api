import { GeneralRes } from '../../common/interfaces/general-res.interface';
import { UserItemResType } from './user-item-res.interface';

export interface UserGeneralRes extends GeneralRes {
    general?: { count: number };
    items: UserItemResType[];
}

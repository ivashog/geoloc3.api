import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';
import { UserEntity } from './entities/user.entity';
import { PermissionEntity } from './entities/permission.entity';
import { TerritoryEntity } from '../territory/entities/territory.entity';
import { Permission2toEntity } from './entities/permission2to.entity';
import { RoleEntity } from './entities/role.entity';
import { MembersStatisticViewEntity } from './entities/members-statistic.view.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            UserEntity,
            PermissionEntity,
            Permission2toEntity,
            TerritoryEntity,
            RoleEntity,
            MembersStatisticViewEntity,
        ]),
    ],
    controllers: [UserController],
    providers: [UserService],
})
export class UserModule {}

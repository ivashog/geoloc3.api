import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';

import { UserDto } from '../dto/user.dto';
import { UserPermissionDto } from '../dto/user-permission.dto';
import { MemberPermissionDto } from '../dto/member-permission.dto';
import { LawyerPermissionDto } from '../dto/lawyer-permission.dto';
import Utils from '../../common/utils/utils';

@Injectable()
export class UserTransformPipe implements PipeTransform<any> {
    transform(user: any, metadata: ArgumentMetadata) {
        const context: string = metadata.metatype.name;
        const { phone, name } = user;

        user.phone = Utils.phoneTransform(phone);
        user.name = Utils.nameTransform(name);

        return context !== 'InLawyerDto'
            ? this.transformToDto(user, context)
            : this.transformLawyerToDto(user);
    }

    private transformToDto(user, context) {
        const userDTO: UserDto = new UserDto();
        const userGeneralProps: string[] = Object.keys(userDTO);
        let permission: UserPermissionDto | MemberPermissionDto | LawyerPermissionDto;

        switch (context) {
            case 'InUserDto':
                permission = new UserPermissionDto();
                break;
            case 'InMemberDto':
                permission = new MemberPermissionDto();
                break;
            case 'InLawyerDto':
                permission = new LawyerPermissionDto();
                break;
        }

        Object.keys(user)
            .filter(prop => !userGeneralProps.includes(prop))
            .forEach(prop => {
                permission[prop] = user[prop];
                delete user[prop];
            });

        if (!permission.role) {
            permission.role =
                context === 'InMemberDto'
                    ? 3
                    : context === 'InLawyerDto'
                    ? 7
                    : permission.canEdit
                    ? 1
                    : 2;
        }

        return { user: { ...user }, permissions: [permission] };
    }
    private transformLawyerToDto(user) {
        const permissions: LawyerPermissionDto[] = user.counties.map(county => ({
            ...new LawyerPermissionDto(),
            county,
        }));

        delete user.counties;
        return { user, permissions };
    }
}

import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';

import Utils from '../../common/utils/utils';
import { LoginDto } from '../dto/login.dto';

@Injectable()
export class LoginCredentialsTransformPipe implements PipeTransform<any> {
    transform(credentials: any, metadata: ArgumentMetadata): LoginDto {
        const { login } = credentials;
        credentials.login = Utils.phoneTransform(login);

        return credentials;
    }
}

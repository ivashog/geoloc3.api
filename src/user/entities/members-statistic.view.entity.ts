import { ViewColumn, ViewEntity } from 'typeorm';

@ViewEntity({
    name: 'u_members_statistic',
    expression: `
        WITH regions AS (
            SELECT
              region, name
            FROM geoloc3.to_territory
            WHERE type_id = 2
        ), counties AS (
            SELECT
              county, name, region
            FROM geoloc3.to_territory
            WHERE type_id = 4
        ), members AS (
            SELECT 
                u_user.id as u_id, 
                u_user.name as user_name,
                u_role_dic.displayed_name AS user_role,
                u_role_dic.id AS role_id,
                to_territory.name AS to_name, 
                to_territory.region AS region, 
                to_territory.county AS county
            FROM geoloc3.u_user
            LEFT JOIN geoloc3.u_permission ON u_permission.u_id = u_user.id
            LEFT JOIN geoloc3.u_role_dic ON u_role_dic.id = u_permission.role_id
            LEFT JOIN geoloc3.u_permission2to ON u_permission2to.u_p_id = u_permission.id
            LEFT JOIN geoloc3.to_territory ON to_territory.id = u_permission2to.to_id
            WHERE u_user.is_deleted <> true AND u_role_dic.group_id = 2
        )
        SELECT
            r.region AS id,
            r.name AS name,
            r.region AS region,
            2 AS level,
            COUNT(DISTINCT m.u_id) FILTER (WHERE m.role_id = 3)::integer AS "dvkMembersCount",
            COUNT(DISTINCT m.u_id) FILTER (WHERE m.role_id = 4)::integer AS "spectatorsCount",
            COUNT(DISTINCT m.u_id) FILTER (WHERE m.role_id = 5)::integer AS "spectatorsMediaCount",
            COUNT(DISTINCT m.u_id) FILTER (WHERE m.role_id = 6)::integer AS "representativesGoCount",
            COUNT(DISTINCT m.u_id)::integer AS "totalMembersCount"
        FROM members m
        FULL OUTER JOIN regions r ON m.region = r.region
        GROUP BY r.name, r.region
        UNION ALL
        SELECT
            c.county AS id,
            c.name AS name,
            c.region AS region,
            4 AS level,
            COUNT(DISTINCT m.u_id) FILTER (WHERE m.role_id = 3)::integer AS "dvkMembersCount",
            COUNT(DISTINCT m.u_id) FILTER (WHERE m.role_id = 4)::integer AS "spectatorsCount",
            COUNT(DISTINCT m.u_id) FILTER (WHERE m.role_id = 5)::integer AS "spectatorsMediaCount",
            COUNT(DISTINCT m.u_id) FILTER (WHERE m.role_id = 6)::integer AS "representativesGoCount",
            COUNT(DISTINCT m.u_id)::integer AS "totalMembersCount"
        FROM members m
        FULL OUTER JOIN counties c ON m.county = c.county
        GROUP BY c.region, c.name, c.county;
    `,
})
export class MembersStatisticViewEntity {
    @ViewColumn()
    id: number;

    @ViewColumn()
    name: string;

    @ViewColumn()
    region: string;

    @ViewColumn()
    level: number;

    @ViewColumn()
    dvkMembersCount: number;

    @ViewColumn()
    spectatorsCount: number;

    @ViewColumn()
    spectatorsMediaCount: number;

    @ViewColumn()
    representativesGoCount: number;

    @ViewColumn()
    totalMembersCount: number;
}

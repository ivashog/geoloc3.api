import { Entity, PrimaryGeneratedColumn, JoinColumn, OneToOne, ManyToOne } from 'typeorm';

import { PermissionEntity } from './permission.entity';
import { TerritoryEntity } from '../../territory/entities/territory.entity';

@Entity('u_permission2to')
export class Permission2toEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @OneToOne(type => PermissionEntity, permission => permission.permission2to, {
        onDelete: 'CASCADE',
    })
    @JoinColumn({ name: 'u_p_id' })
    permission: PermissionEntity;

    @ManyToOne(type => TerritoryEntity, territory => territory.id, {
        eager: true,
    })
    @JoinColumn({ name: 'to_id' })
    territory: TerritoryEntity;
}

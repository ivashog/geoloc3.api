import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

import { RoleEntity } from './role.entity';

@Entity('u_role_group_dic')
export class RoleGroupEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('varchar', {
        length: 20,
    })
    name: string;

    @OneToMany(type => RoleEntity, role => role.group)
    role: RoleEntity[];
}

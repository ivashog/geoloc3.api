import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, OneToOne } from 'typeorm';

import { Versioning } from '../../common/entities/versioning.entity';
import { UserEntity } from './user.entity';
import { RoleEntity } from './role.entity';
import { Permission2toEntity } from './permission2to.entity';
import { TerritoryTypeEntity } from '../../territory/entities/territory-type.entity';

@Entity('u_permission')
export class PermissionEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('boolean', { name: 'can_edit', default: false })
    canEdit: boolean;

    @Column('boolean', { name: 'is_banned', default: false })
    isBanned: boolean;

    @ManyToOne(type => TerritoryTypeEntity, level => level.id)
    @JoinColumn({ name: 'to_level' })
    level: TerritoryTypeEntity;

    @ManyToOne(type => UserEntity, user => user.id, {
        onDelete: 'CASCADE',
    })
    @JoinColumn({ name: 'u_id' })
    user: UserEntity;

    @ManyToOne(type => RoleEntity, role => role.id, {
        cascade: true,
        eager: true,
    })
    @JoinColumn({ name: 'role_id' })
    role: RoleEntity;

    @OneToOne(type => Permission2toEntity, permission2to => permission2to.permission, {
        eager: true,
        cascade: true,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    permission2to: Permission2toEntity;

    @Column(type => Versioning)
    v: Versioning;
}

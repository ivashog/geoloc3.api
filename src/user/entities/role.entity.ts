import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, OneToMany } from 'typeorm';

import { PermissionEntity } from './permission.entity';
import { RoleGroupEntity } from './role-group.entity';

@Entity('u_role_dic')
export class RoleEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('varchar', {
        length: 20,
    })
    name: string;

    @Column('varchar', {
        name: 'displayed_name',
        length: 100,
    })
    displayedName: string;

    @ManyToOne(type => RoleGroupEntity, group => group.id, {
        eager: true
    })
    @JoinColumn({ name: 'group_id' })
    group: RoleGroupEntity;

    @OneToMany(type => PermissionEntity, permission => permission.user, {
        cascade: true
    })
    permission: PermissionEntity[];
}

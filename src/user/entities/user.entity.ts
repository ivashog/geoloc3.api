import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToMany,
    BeforeInsert,
    BeforeUpdate,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';

import { Versioning } from '../../common/entities/versioning.entity';
import { PermissionEntity } from './permission.entity';
import {
    GeneralUserItemRes,
    LawyerItemRes,
    MemberItemRes,
    UserItemRes,
} from '../interfaces/user-item-res.interface';
import { LoginRes } from '../interfaces/login-res.interface';
import {
    LAWYERS_ROLES_IDS,
    MEMBERS_ROLES_IDS,
    USERS_ROLES_IDS,
} from '../../common/configs/roles.config';

@Entity('u_user')
export class UserEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('varchar', { length: 200 })
    name: string;

    @Column('varchar', { length: 30, unique: true })
    phone: string;

    @Column('varchar', { nullable: true })
    password: string;

    @Column('varchar', { name: 'password_code', nullable: true })
    passwordCode: string;

    @Column('varchar', { length: 1, nullable: true })
    gender: string;

    @Column('date', { name: 'date_of_birth', nullable: true })
    dateOfBirth: string;

    @Column('boolean', { name: 'is_deleted', default: false })
    isDeleted: boolean;

    @OneToMany(type => PermissionEntity, permission => permission.user, {
        cascade: true,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    permissions: PermissionEntity[];

    @Column(type => Versioning)
    v: Versioning;

    @BeforeInsert()
    async beforeInsert() {
        await this.hashPassword();
    }

    @BeforeUpdate()
    async beforeUpdate() {
        await this.hashPassword();
    }

    private async hashPassword() {
        if (this.password) {
            this.password = await bcrypt.hash(this.password, 5);
        }
    }

    public async comparePassword(input: string): Promise<boolean> {
        return await bcrypt.compare(input, this.password);
    }

    public toLoginResponse(showToken: boolean = true): LoginRes {
        const {
            id,
            permissions: [permission],
            token,
        } = this;
        const {
            permission2to: { territory },
            role: { id: role },
            level,
        } = permission;

        if (!permission || !role || !level || !territory) {
            throw new Error('User entity not joined to all of his relations!');
        }

        const loginResponse: LoginRes = {
            id,
            role,
            access: {
                canEdit: permission.canEdit,
            },
        };

        if (showToken) loginResponse.token = token;

        if (LAWYERS_ROLES_IDS.includes(role)) {
            loginResponse.access.counties = this.permissions.map(permission => {
                return permission.permission2to.territory.county;
            });
        } else if (MEMBERS_ROLES_IDS.includes(role)) {
            loginResponse.access.station = territory.station;
            loginResponse.access.level = permission.level.id;
            loginResponse.access.region = territory.region;
            loginResponse.access.county = territory.county;
        } else if (USERS_ROLES_IDS.includes(role)) {
            loginResponse.access.level = permission.level.id;
            loginResponse.access.region = territory.region;
            loginResponse.access.county = territory.county;
        }

        return loginResponse;
    }

    public toUserResponseObject(): UserItemRes {
        const {
            generalResponseObject,
            permissions: [permission],
        } = this;
        const {
            permission2to: { territory },
        } = permission;

        return {
            ...generalResponseObject,
            canEdit: permission.canEdit,
            level: permission.level.id,
            region: territory.region,
            county: territory.county,
        };
    }

    public toMemberToResponseObject(): MemberItemRes {
        const {
            generalResponseObject,
            permissions: [permission],
        } = this;
        const {
            permission2to: { territory },
        } = permission;

        return {
            ...generalResponseObject,
            canEdit: permission.canEdit,
            role: permission.role.id,
            region: territory.region,
            county: territory.county,
            station: territory.station,
        };
    }

    public toLawyerToResponseObject(): LawyerItemRes {
        const { generalResponseObject, permissions } = this;

        return {
            ...generalResponseObject,
            counties: permissions.map(permission => {
                return permission.permission2to.territory.county;
            }),
        };
    }

    private get generalResponseObject(): GeneralUserItemRes {
        const {
            id,
            name,
            phone,
            gender,
            dateOfBirth,
            permissions: [permission],
            token,
        } = this;
        const {
            permission2to: { territory },
        } = permission;

        if (!permission || !permission.role || !permission.level || !territory) {
            throw new Error('User entity not joined to all of his relations!');
        }

        return { id, name, phone, gender, dateOfBirth };
    }

    private get token() {
        return jwt.sign({ ...this, password: '' }, process.env.JWT_SECRET, {
            expiresIn: process.env.JWT_EXPIRATION,
        });
    }
}

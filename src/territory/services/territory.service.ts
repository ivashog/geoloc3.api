import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import fetch, { RequestInit } from 'node-fetch';

import { GeneralRes } from '../../common/interfaces/general-res.interface';
import { TerritoryGeomType, TerritoryListType } from '../interfaces/territory-types';
import { TerritoryFilterDto } from '../dto/territory-filter.dto';
import { TerritoryEntity } from '../entities/territory.entity';

@Injectable()
export class TerritoryService {
    constructor(
        @InjectRepository(TerritoryEntity)
        private readonly territoryRepository: Repository<TerritoryEntity>,
    ) {}

    async getList(
        type: TerritoryListType,
        territoryFilter: TerritoryFilterDto,
    ): Promise<GeneralRes> {
        const list = await this.territoryRepository.find({
            select: ['name', 'region', 'county', 'district', 'station'],
            where: {
                type: this.territoryTypeToId(type),
                ...territoryFilter,
            },
        });
        const territoryIdField: string = this.territoryTypeToName(type);

        return {
            general: { count: list.length },
            items: list.map(item => ({
                id: item[territoryIdField],
                name: item.name,
            })),
        };
    }

    async getGeometry(
        type: TerritoryGeomType,
        territoryFilter: TerritoryFilterDto,
    ): Promise<GeneralRes> {
        const query: string = Object.keys(territoryFilter)
            .reduce((query, prop) => {
                return `${query}${prop}_id=${territoryFilter[prop]}&`;
            }, `${this.territoryTypeToName(type)}?`)
            .slice(0, -1);

        const territories = await this.getApiData(`${process.env.GEO_API_URL}/geometry/${query}`);

        return {
            general: { count: territories.length },
            items: territories,
        };
    }

    private async getApiData(url: string) {
        const requestOptions: RequestInit = {
            headers: { 'Content-Type': 'application/json' },
            method: 'GET',
            compress: true,
            timeout: 10000,
        };
        const response = await fetch(url, requestOptions);

        if (response.status === 404) {
            throw new HttpException(`Cannot get data from API '${url}'`, HttpStatus.NOT_FOUND);
        } else if (response.status !== 200) {
            throw new HttpException('Geometry service unavailable', HttpStatus.SERVICE_UNAVAILABLE);
        }

        try {
            return await response.json();
        } catch (error) {
            Logger.error(`Cannot get data from API '${url}'`, error.message);
        }
    }

    private territoryTypeToName(type: string): string {
        switch (type) {
            case 'regions':
                return 'region';
            case 'counties':
                return 'county';
            case 'districts':
                return 'district';
            case 'stations':
                return 'station';
        }
    }

    private territoryTypeToId(type: string): number {
        switch (type) {
            case 'regions':
                return 2;
            case 'districts':
                return 3;
            case 'counties':
                return 4;
            case 'stations':
                return 5;
        }
    }
}

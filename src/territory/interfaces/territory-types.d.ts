export declare type TerritoryListType = 'regions' | 'counties' | 'districts' | 'stations';
export declare type TerritoryGeomType = 'regions' | 'counties' | 'stations';

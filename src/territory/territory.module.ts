import { CacheModule, Module } from '@nestjs/common';
import * as redisStore from 'cache-manager-redis-store';

import { TerritoryController } from './controllers/territory.controller';
import { TerritoryService } from './services/territory.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TerritoryEntity } from './entities/territory.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([TerritoryEntity]),
        CacheModule.register({
            store: redisStore,
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT,
            ttl: +process.env.REDIS_TTL,
        }),
    ],
    controllers: [TerritoryController],
    providers: [TerritoryService],
})
export class TerritoryModule {}

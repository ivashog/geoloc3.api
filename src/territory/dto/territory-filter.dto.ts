import { IsNotEmpty, IsNumberString, IsOptional, IsString, Length } from 'class-validator';

export class TerritoryFilterDto {
    @Length(3, 3)
    @IsNumberString()
    @IsNotEmpty()
    @IsString()
    @IsOptional()
    region?: string;

    @Length(2, 3)
    @IsNumberString()
    @IsNotEmpty()
    @IsString()
    @IsOptional()
    county?: string;

    @Length(9, 9)
    @IsNumberString()
    @IsNotEmpty()
    @IsString()
    @IsOptional()
    district?: string;
}

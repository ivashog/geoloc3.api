export class TerritoryDto {
    // name: string;
    // koatuu: string;
    type: number;
    region: string;
    district: string;
    county: string;
    station: string;
    uniqCode: string;
    // districtToCounties: string[];
    // isTot: boolean = false;
    // isIgnored: boolean = false;
}

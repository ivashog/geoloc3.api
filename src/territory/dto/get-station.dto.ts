import { IsNumberString, IsOptional } from 'class-validator';

export class GetStationDto {
    @IsOptional()
    @IsNumberString()
    county: string;

    @IsOptional()
    @IsNumberString()
    district: string;
}

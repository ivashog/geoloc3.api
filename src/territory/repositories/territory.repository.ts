import { EntityRepository, Repository } from 'typeorm';
import { HttpException, HttpStatus } from '@nestjs/common';

import { TerritoryEntity } from '../entities/territory.entity';
import { TerritoryType } from '../../common/enums/territory-types.enum';

@EntityRepository(TerritoryEntity)
export class TerritoryRepository extends Repository<TerritoryEntity> {
    async getStationByUniqId(stationId: string): Promise<TerritoryEntity> {
        const station = await this.findOne({
            where: {
                uniqCode: stationId,
                type: TerritoryType.Station,
            },
        });

        if (!station) {
            throw new HttpException(
                `Station with uniqCode '${stationId}' is not exist`,
                HttpStatus.BAD_REQUEST,
            );
        }
        return station;
    }

    async getCountyByCountyId(countyId): Promise<TerritoryEntity> {
        const county = await this.findOne({
            where: {
                county: countyId,
                type: TerritoryType.County,
            },
        });

        if (!county)
            throw new HttpException(
                `County with number '${countyId}' is not exist`,
                HttpStatus.BAD_REQUEST,
            );
        return county;
    }
}

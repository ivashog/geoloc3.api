import {
    ArgumentMetadata,
    HttpException,
    HttpStatus,
    Injectable,
    PipeTransform,
} from '@nestjs/common';

@Injectable()
export class TerritoryValidatePipe implements PipeTransform {
    transform(value: any, { metatype }: ArgumentMetadata) {
        if (!metatype || !this.toValidate(metatype)) {
            return value;
        }
        if (!this.validateParams(value)) {
            throw new HttpException(
                'Bad request. Expect either "county" or "district" parameter.',
                HttpStatus.BAD_REQUEST,
            );
        }
        return value;
    }
    private validateParams(value) {
        const isObject: boolean = typeof value === 'object';
        const objectKeys: Array<any> = Object.keys(value);
        const objectKeysLength: number = objectKeys.length;
        const availableParamsList = ['county', 'district'];

        return isObject && objectKeysLength === 1 && availableParamsList.includes(objectKeys[0]);
    }
    private toValidate(metatype: Function): boolean {
        const types: Function[] = [String, Boolean, Number, Array, Object];
        return !types.includes(metatype);
    }
}

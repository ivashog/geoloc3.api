import {
    PipeTransform,
    Injectable,
    ArgumentMetadata,
    HttpException,
    HttpStatus,
} from '@nestjs/common';
import { TerritoryListType } from '../interfaces/territory-types';

@Injectable()
export class TerritoryTypePipe implements PipeTransform<string, TerritoryListType> {
    transform(value: TerritoryListType, metadata: ArgumentMetadata): TerritoryListType {
        const availableTerritoryTypes: TerritoryListType[] = [
            'regions',
            'counties',
            'districts',
            'stations',
        ];

        if (!availableTerritoryTypes.includes(value)) {
            throw new HttpException(
                `Validation failed: available territory type param must be one of the following: '${availableTerritoryTypes}', but actual is '${value}'`,
                HttpStatus.BAD_REQUEST,
            );
        }

        return value;
    }
}

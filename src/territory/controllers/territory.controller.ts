import { CacheInterceptor, Controller, Get, Param, Query, UseInterceptors } from '@nestjs/common';

import { TerritoryService } from '../services/territory.service';
import { TerritoryGeomType, TerritoryListType } from '../interfaces/territory-types';
import { Roles } from '../../user/decorators/roles.decorator';
import { USERS_ROLES } from '../../common/configs/roles.config';
import { TerritoryTypePipe } from '../pipes/territory-type.pipe';
import { TerritoryFilterDto } from '../dto/territory-filter.dto';
import { ValidationPipe } from '../../common/pipes/validation.pipe';
import { GeneralRes } from '../../common/interfaces/general-res.interface';

@Controller('territory')
@UseInterceptors(CacheInterceptor)
export class TerritoryController {
    constructor(private readonly territoryService: TerritoryService) {}

    @Roles(...USERS_ROLES)
    @Get(':type/list')
    async getTerritoriesList(
        @Param('type', new TerritoryTypePipe()) type: TerritoryListType,
        @Query(ValidationPipe) territoryFilter: TerritoryFilterDto,
    ): Promise<GeneralRes> {
        return await this.territoryService.getList(type, territoryFilter);
    }

    @Roles(...USERS_ROLES)
    @Get(':type/geometry')
    async getTerritoriesGeometry(
        @Param('type', new TerritoryTypePipe()) type: TerritoryGeomType,
        @Query(ValidationPipe) territoryFilter: TerritoryFilterDto,
    ): Promise<GeneralRes> {
        return await this.territoryService.getGeometry(type, territoryFilter);
    }
}

import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { TerritoryEntity } from './territory.entity';

@Entity('to_type_dic')
export class TerritoryTypeEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('varchar', {
        length: 20,
    })
    name: string;

    @Column('varchar', {
        name: 'displayed_name',
        length: 200,
    })
    displayedName: string;

    @OneToMany(type => TerritoryEntity, territory => territory.type)
    territories: TerritoryEntity[];
}

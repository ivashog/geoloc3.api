import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Versioning } from '../../common/entities/versioning.entity';
import { AutoSyncEntity } from '../../common/entities/auto-sync.entity';
import { TerritoryTypeEntity } from './territory-type.entity';
import { CandidateEntity } from '../../election/entities/candidate.entity';
import { Permission2toEntity } from '../../user/entities/permission2to.entity';

@Entity('to_territory')
export class TerritoryEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('varchar', { length: 200 })
    name: string;

    @Column('varchar', { length: 10, nullable: true })
    koatuu: string;

    @Column('varchar', { nullable: true })
    region: string;

    @Column('varchar', { nullable: true })
    district: string;

    @Column('varchar', { nullable: true })
    county: string;

    @Column('varchar', { nullable: true })
    station: string;

    @Column('varchar', {
        name: 'district_to_counties',
        array: true,
        nullable: true,
    })
    districtToCounties: string[];

    @Column('varchar', { name: 'uniq_code', nullable: true })
    uniqCode: string;

    @Column('boolean', { name: 'is_tot', default: false })
    isTot: boolean;

    @Column('boolean', { name: 'is_ignored', default: false })
    isIgnored: boolean;

    @ManyToOne(type => TerritoryTypeEntity, type => type.id)
    @JoinColumn({ name: 'type_id' })
    type: number;

    @OneToMany(type => Permission2toEntity, permission2to => permission2to.territory)
    permission2to: Permission2toEntity[];

    @OneToMany(type => CandidateEntity, candidate => candidate.county)
    candidates: CandidateEntity[];

    @Column(type => Versioning)
    v: Versioning;

    @Column(type => AutoSyncEntity)
    parse: AutoSyncEntity;
}

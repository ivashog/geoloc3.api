export enum TerritoryType {
    Country = 1,
    Region,
    District,
    County,
    Station,
}

export const enum RoleIds {
    ADMIN = 1,
    ANALYTIC,
    PST_MEMBER,
    SPECTATOR,
    SPECTATOR_MEDIA,
    PO_MEMBER,
    LAWYER,
}

export const enum RoleNames {
    ADMIN = 'admin',
    ANALYTIC = 'analytic',
    PST_MEMBER = 'pst_member',
    SPECTATOR = 'spectator',
    SPECTATOR_MEDIA = 'spectator_media',
    PO_MEMBER = 'po_member',
    LAWYER = 'lawyer',
}

import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class TransformBodyCasePipe implements PipeTransform {
    transform(value: any, metadata: ArgumentMetadata) {
        if (metadata.type === 'body') {
            return this.keysToCamel(value);
        } else {
            return value;
        }
    }

    private toCamel(s) {
        return s.replace(/([-_][a-z])/ig, $1 => {
            return $1.toUpperCase()
                .replace('-', '')
                .replace('_', '');
        });
    };

    private isArray(arr) {
        return Array.isArray(arr);
    };

    private isObject(obj) {
        return obj === Object(obj) && !this.isArray(obj) && typeof obj !== 'function';
    };

    private keysToCamel(obj) {
        if (this.isObject(obj)) {
            const newObj = {};
            Object.keys(obj).forEach(key => {
                newObj[this.toCamel(key)] = this.keysToCamel(obj[key]);
            });
            return newObj;
        } else if (this.isArray(obj)) {
            return obj.map(item => this.keysToCamel(item));
        }
        return obj;
    };
}

import {
    PipeTransform,
    Injectable,
    ArgumentMetadata,
    HttpException,
    HttpStatus,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
    async transform(value: any, metadata: ArgumentMetadata) {
        const { metatype, type } = metadata;

        if (value instanceof Object && this.isEmpty(value) && type !== 'query') {
            throw new HttpException('Validation failed: No body submitted', HttpStatus.BAD_REQUEST);
        }

        if (!metatype || !this.toValidate(metatype)) {
            return value;
        }

        const object = plainToClass(metatype, value);
        const errors = await validate(object, {
            whitelist: true,
            forbidNonWhitelisted: true,
        });
        if (errors.length > 0) {
            throw new HttpException(
                `Validation failed: ${this.formatErrors(errors)}`,
                HttpStatus.BAD_REQUEST,
            );
        }
        return value;
    }

    private toValidate(metatype): boolean {
        const types = [String, Boolean, Number, Array, Object];
        return !types.includes(metatype);
    }

    private formatErrors(errors: any[]) {
        return errors
            .map(err => {
                if (err.constraints) {
                    for (const property in err.constraints) {
                        return err.constraints[property];
                    }
                } else {
                    return this.formatErrors(err.children);
                }
            })
            .join('; ');
    }

    private isEmpty(obj: object) {
        return !Object.keys(obj).length;
    }
}

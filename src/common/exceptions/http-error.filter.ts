import {
    ExceptionFilter,
    HttpException,
    HttpStatus,
    ArgumentsHost,
    Catch,
    Logger,
} from '@nestjs/common';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {
        const context = host.switchToHttp();
        const request = context.getRequest();
        const response = context.getResponse();

        const status =
            exception instanceof HttpException
                ? exception.getStatus()
                : HttpStatus.INTERNAL_SERVER_ERROR;

        const errorResponse = {
            statusCode: status,
            path: request.url,
            method: request.method,
            timestamp: new Date().toLocaleString('ru-RU'),
            error:
                exception instanceof HttpException
                    ? exception.message.error || exception.message || null
                    : 'Internal server error',
        };

        if (status === HttpStatus.INTERNAL_SERVER_ERROR) {
            // prettier-ignore
            Logger.error(
                `${request.method} ${request.url}`,
                exception.stack, 'ExceptionFilter',
            );
        } else {
            Logger.error(
                `${request.method} ${request.url}`,
                JSON.stringify(errorResponse),
                'ExceptionFilter',
            );
        }

        response.status(status).json(errorResponse);
    }
}

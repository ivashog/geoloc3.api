import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

import { MEMBERS_ROLES } from '../configs/roles.config';
import { RoleNames } from '../enums/roles.enum';

@Injectable()
export class AccessGuard implements CanActivate {
    constructor(private readonly reflector: Reflector) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        if (request.path.split('/').includes('login')) {
            return true;
        }
        const roles = this.reflector.get<string[]>('roles', context.getHandler());
        const user = request.user;

        if (!user) {
            throw new Error('Error in AccessGuard: not user in request');
        }

        const userPermissions = user.permissions.map(permission =>
            permission.role.name.toLowerCase(),
        );

        const hasRole = roles
            ? roles.some(roleName => userPermissions.includes(roleName.toLowerCase()))
            : null;
        const isAccessForMember = roles
            ? roles.some(roleName => MEMBERS_ROLES.includes(roleName.toLowerCase()))
            : null;
        const canEdit = user.permissions.some(permission => permission.canEdit === true);

        const {
            role: { name: userRole },
            permission2to: { territory: userTerritory },
        } = user.permissions.find(perm => perm.canEdit === true);

        const hasAccessToTerritory =
            userRole === RoleNames.ADMIN ||
            !request.body.station ||
            request.body.station === userTerritory.uniqCode;

        return isAccessForMember
            ? hasRole && canEdit && hasAccessToTerritory
            : hasRole === true || hasRole === null;
    }
}

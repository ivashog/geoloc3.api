import { Injectable, CanActivate, ExecutionContext, HttpException, HttpStatus } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthGuard implements CanActivate {
    async canActivate(context: ExecutionContext,): Promise<boolean> {
        const request = context.switchToHttp().getRequest();

        if (request.path.split('/').includes('login')) {
            return true;
        }

        if (!request.headers.authorization) {
            throw new HttpException('Forbidden: authorization token is required.', HttpStatus.FORBIDDEN);
        }
        request.user = await this.validateToken(request.headers.authorization);
        // TODO: return always true? Wtf???
        return true;
    }

    private async validateToken(auth: string) {
        const [ keyword, token ] = auth.split(' ');
        if (keyword !== process.env.AUTH_TYPE) {
            throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
        }
        try {
            return await jwt.verify(token, process.env.JWT_SECRET);
        } catch (err) {
            const message = 'Token error: ' + (err.message || err.name);
            throw new HttpException(message, HttpStatus.UNAUTHORIZED);
        }
    }
}

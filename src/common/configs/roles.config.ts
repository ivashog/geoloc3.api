import { RoleIds, RoleNames } from '../enums/roles.enum';

export const USERS_ROLES_IDS: number[] = [RoleIds.ADMIN, RoleIds.ANALYTIC];

export const MEMBERS_ROLES_IDS: number[] = [
    RoleIds.PST_MEMBER,
    RoleIds.SPECTATOR,
    RoleIds.SPECTATOR_MEDIA,
    RoleIds.PO_MEMBER,
];

export const LAWYERS_ROLES_IDS: number[] = [RoleIds.LAWYER];

export const USERS_ROLES: string[] = [RoleNames.ADMIN, RoleNames.ANALYTIC];

export const MEMBERS_ROLES: string[] = [
    RoleNames.PST_MEMBER,
    RoleNames.SPECTATOR,
    RoleNames.SPECTATOR_MEDIA,
    RoleNames.PO_MEMBER,
];

export const LAWYERS_ROLES: string[] = [RoleNames.LAWYER];

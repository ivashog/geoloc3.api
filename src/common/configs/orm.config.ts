import {TypeOrmModuleOptions} from '@nestjs/typeorm';
const SOURCE_PATH = process.env.NODE_ENV === 'production' ? 'dist/' : '';

const config: TypeOrmModuleOptions = {
    type: 'postgres',
    host: process.env.DB_HOST || 'localhost',
    port: +process.env.DB_PORT || 5432,
    username: process.env.DB_USER || 'postgres',
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME || 'postgres',
    schema: process.env.DB_SCHEMA || 'public',
    synchronize: process.env.NODE_ENV !== 'production',
    logging:  process.env.NODE_ENV === 'production' ? [ 'error', 'log' ] : 'all',
    entities: [`${SOURCE_PATH}src/**/!(*.entity.d)*.entity.{ts,js}`],
    migrations: [`${SOURCE_PATH}migration/!(*.d).{ts,js}`],
    cli: {
        migrationsDir: `${SOURCE_PATH}migration`
    }
};
export default config;

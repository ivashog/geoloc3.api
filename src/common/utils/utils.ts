import { HttpException, HttpStatus } from '@nestjs/common';

export default class Utils {
    static phoneTransform(phone: string): string {
        let resultPhone = phone.replace(/\D/g, ''); // remove all non-digit

        if (resultPhone.length >= 10) {
            resultPhone = resultPhone.match(/\d{10}$/g)[0]; // take last 10 digits
        } else if (resultPhone.length === 9) {
            resultPhone = resultPhone.padStart(10, '0'); // padding by '0' on start
        } else {
            throw new HttpException(
                `Transform failed: phone '${phone}' is not valid`,
                HttpStatus.BAD_REQUEST
            );
        }

        return resultPhone;
    }

    static nameTransform(name: string): string {
        const newName = name
            .trim() // remove whitespaces at the beginning and end of the string
            .replace(/\s+/g, ' '); // replace more then one whitespaces on one

        if (newName.length < 3) {
            throw new HttpException(
                `Transform failed: name '${name}' is not valid`,
                HttpStatus.BAD_REQUEST
            );
        }

        return newName;
    }

    static generateRandomString(length: number): string {
        return [...Array(length)]
            .map(i => (~~(Math.random() * 36)).toString(36))
            .join('');
    }
}

import { GeneralRes } from '../interfaces/general-res.interface';

const versionProps = ['v_created_at', 'v_updated_at', 'v_version'];

export const resultToResponseObj = (result: any[], excludeProps: string[] = []) =>
    <GeneralRes>{
        general: {
            ...result.reduce(
                (response, item) => {
                    Object.keys(response).forEach(prop => (response[prop] += +item[prop]));
                    return response;
                },
                result.length
                    ? Object.keys(result[0])
                          .filter(
                              prop =>
                                  !['id', 'name', ...versionProps, ...excludeProps].includes(prop),
                          )
                          .reduce((general, prop) => ({ ...general, [prop]: 0 }), {})
                    : { count: 0 },
            ),
            count: result.length,
        },
        items: result,
    };

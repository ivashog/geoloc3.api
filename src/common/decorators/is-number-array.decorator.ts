import {
    registerDecorator,
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationOptions,
    ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'IsNumberArray', async: false })
export class IsNumberArrayConstraint implements ValidatorConstraintInterface {
    public validate(value: any, args: ValidationArguments) {
        if (!Array.isArray(value) || !value.length) return false;
        else return !value.some(item => Number.isNaN(+item));
    }

    public defaultMessage(args: ValidationArguments) {
        return `'$property' must be a number array, but actual is '${args.value}'`;
    }
}

export function IsNumberArray(validationOptions?: ValidationOptions) {
    return (object: Object, propertyName: string) => {
        registerDecorator({
            name: 'IsNumberArray',
            target: object.constructor,
            propertyName,
            constraints: [],
            options: validationOptions,
            validator: IsNumberArrayConstraint,
        });
    };
}

import { CreateDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';

export class Versioning {
    @CreateDateColumn({
        name: '_created_at',
        comment: 'Date of creation',
    })
    createdAt: Date;

    @UpdateDateColumn({
        name: '_updated_at',
        comment: 'Date of last update',
    })
    updatedAt: Date;

    @VersionColumn({
        name: '_version',
        comment: 'Current version number of this record (represent count of record updates)',
    })
    version: number;
}

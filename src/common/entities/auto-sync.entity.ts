import { Column } from 'typeorm';

export class AutoSyncEntity {
    @Column('boolean', {
        name: '_is_sync',
        default: false,
    })
    isSync: boolean = false;

    @Column('date', {
        name: '_date_auto_sync',
        nullable: true,
    })
    autoSyncAt: string;

    @Column('boolean', {
        name: '_is_drop',
        default: false,
    })
    isDrop: boolean = false;
}

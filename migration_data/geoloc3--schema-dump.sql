--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Ubuntu 11.3-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.3 (Ubuntu 11.3-1.pgdg18.04+1)
-- Thu May 23 2019 14:27:16 GMT+0300 (Eastern European Summer Time)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: geoloc3; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA IF NOT EXISTS geoloc3;


ALTER SCHEMA geoloc3 OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: to_territory; Type: TABLE; Schema: geoloc3; Owner: postgres
--

CREATE TABLE IF NOT EXISTS geoloc3.to_territory (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    koatuu character varying(10),
    region character varying,
    district character varying,
    county character varying,
    station character varying,
    district_to_counties character varying[],
    uniq_code character varying,
    is_tot boolean DEFAULT false NOT NULL,
    is_ignored boolean DEFAULT false NOT NULL,
    type_id integer,
    v_created_at timestamp without time zone DEFAULT now() NOT NULL,
    v_updated_at timestamp without time zone DEFAULT now() NOT NULL,
    v_version integer NOT NULL,
    parse_is_sync boolean DEFAULT false NOT NULL,
    parse_date_auto_sync date,
    parse_is_drop boolean DEFAULT false NOT NULL
);


ALTER TABLE geoloc3.to_territory OWNER TO postgres;

--
-- Name: to_territory_id_seq; Type: SEQUENCE; Schema: geoloc3; Owner: postgres
--

CREATE SEQUENCE geoloc3.to_territory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geoloc3.to_territory_id_seq OWNER TO postgres;

--
-- Name: to_territory_id_seq; Type: SEQUENCE OWNED BY; Schema: geoloc3; Owner: postgres
--

ALTER SEQUENCE geoloc3.to_territory_id_seq OWNED BY geoloc3.to_territory.id;


--
-- Name: to_type_dic; Type: TABLE; Schema: geoloc3; Owner: postgres
--

CREATE TABLE IF NOT EXISTS geoloc3.to_type_dic (
    id integer NOT NULL,
    name character varying(20) NOT NULL,
    displayed_name character varying(200) NOT NULL
);


ALTER TABLE geoloc3.to_type_dic OWNER TO postgres;

--
-- Name: to_type_dic_id_seq; Type: SEQUENCE; Schema: geoloc3; Owner: postgres
--

CREATE SEQUENCE geoloc3.to_type_dic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geoloc3.to_type_dic_id_seq OWNER TO postgres;

--
-- Name: to_type_dic_id_seq; Type: SEQUENCE OWNED BY; Schema: geoloc3; Owner: postgres
--

ALTER SEQUENCE geoloc3.to_type_dic_id_seq OWNED BY geoloc3.to_type_dic.id;


--
-- Name: u_permission; Type: TABLE; Schema: geoloc3; Owner: postgres
--

CREATE TABLE IF NOT EXISTS geoloc3.u_permission (
    id integer NOT NULL,
    can_edit boolean DEFAULT false NOT NULL,
    is_banned boolean DEFAULT false NOT NULL,
    u_id integer,
    v_created_at timestamp without time zone DEFAULT now() NOT NULL,
    v_updated_at timestamp without time zone DEFAULT now() NOT NULL,
    v_version integer NOT NULL,
    role_id integer,
    to_level integer
);


ALTER TABLE geoloc3.u_permission OWNER TO postgres;

--
-- Name: u_permission2to; Type: TABLE; Schema: geoloc3; Owner: postgres
--

CREATE TABLE IF NOT EXISTS geoloc3.u_permission2to (
    id integer NOT NULL,
    u_p_id integer,
    to_id integer
);


ALTER TABLE geoloc3.u_permission2to OWNER TO postgres;

--
-- Name: u_permission2to_id_seq; Type: SEQUENCE; Schema: geoloc3; Owner: postgres
--

CREATE SEQUENCE geoloc3.u_permission2to_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geoloc3.u_permission2to_id_seq OWNER TO postgres;

--
-- Name: u_permission2to_id_seq; Type: SEQUENCE OWNED BY; Schema: geoloc3; Owner: postgres
--

ALTER SEQUENCE geoloc3.u_permission2to_id_seq OWNED BY geoloc3.u_permission2to.id;


--
-- Name: u_permission_id_seq; Type: SEQUENCE; Schema: geoloc3; Owner: postgres
--

CREATE SEQUENCE geoloc3.u_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geoloc3.u_permission_id_seq OWNER TO postgres;

--
-- Name: u_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: geoloc3; Owner: postgres
--

ALTER SEQUENCE geoloc3.u_permission_id_seq OWNED BY geoloc3.u_permission.id;


--
-- Name: u_role_dic; Type: TABLE; Schema: geoloc3; Owner: postgres
--

CREATE TABLE IF NOT EXISTS geoloc3.u_role_dic (
    id integer NOT NULL,
    name character varying(20) NOT NULL,
    displayed_name character varying(100) NOT NULL,
    group_id integer
);


ALTER TABLE geoloc3.u_role_dic OWNER TO postgres;

--
-- Name: u_role_dic_id_seq; Type: SEQUENCE; Schema: geoloc3; Owner: postgres
--

CREATE SEQUENCE geoloc3.u_role_dic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geoloc3.u_role_dic_id_seq OWNER TO postgres;

--
-- Name: u_role_dic_id_seq; Type: SEQUENCE OWNED BY; Schema: geoloc3; Owner: postgres
--

ALTER SEQUENCE geoloc3.u_role_dic_id_seq OWNED BY geoloc3.u_role_dic.id;


--
-- Name: u_role_group_dic; Type: TABLE; Schema: geoloc3; Owner: postgres
--

CREATE TABLE IF NOT EXISTS geoloc3.u_role_group_dic (
    id integer NOT NULL,
    name character varying(20) NOT NULL
);


ALTER TABLE geoloc3.u_role_group_dic OWNER TO postgres;

--
-- Name: u_role_group_dic_id_seq; Type: SEQUENCE; Schema: geoloc3; Owner: postgres
--

CREATE SEQUENCE geoloc3.u_role_group_dic_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geoloc3.u_role_group_dic_id_seq OWNER TO postgres;

--
-- Name: u_role_group_dic_id_seq; Type: SEQUENCE OWNED BY; Schema: geoloc3; Owner: postgres
--

ALTER SEQUENCE geoloc3.u_role_group_dic_id_seq OWNED BY geoloc3.u_role_group_dic.id;


--
-- Name: u_user; Type: TABLE; Schema: geoloc3; Owner: postgres
--

CREATE TABLE IF NOT EXISTS geoloc3.u_user (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    phone character varying(30) NOT NULL,
    password_code character varying,
    gender character varying(1),
    date_of_birth date,
    is_deleted boolean DEFAULT false NOT NULL,
    v_created_at timestamp without time zone DEFAULT now() NOT NULL,
    v_updated_at timestamp without time zone DEFAULT now() NOT NULL,
    v_version integer NOT NULL,
    password character varying
);


ALTER TABLE geoloc3.u_user OWNER TO postgres;

--
-- Name: u_user_id_seq; Type: SEQUENCE; Schema: geoloc3; Owner: postgres
--

CREATE SEQUENCE geoloc3.u_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geoloc3.u_user_id_seq OWNER TO postgres;

--
-- Name: u_user_id_seq; Type: SEQUENCE OWNED BY; Schema: geoloc3; Owner: postgres
--

ALTER SEQUENCE geoloc3.u_user_id_seq OWNED BY geoloc3.u_user.id;

--
-- Name: to_territory id; Type: DEFAULT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.to_territory ALTER COLUMN id SET DEFAULT nextval('geoloc3.to_territory_id_seq'::regclass);


--
-- Name: to_type_dic id; Type: DEFAULT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.to_type_dic ALTER COLUMN id SET DEFAULT nextval('geoloc3.to_type_dic_id_seq'::regclass);


--
-- Name: u_permission id; Type: DEFAULT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_permission ALTER COLUMN id SET DEFAULT nextval('geoloc3.u_permission_id_seq'::regclass);


--
-- Name: u_permission2to id; Type: DEFAULT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_permission2to ALTER COLUMN id SET DEFAULT nextval('geoloc3.u_permission2to_id_seq'::regclass);


--
-- Name: u_role_dic id; Type: DEFAULT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_role_dic ALTER COLUMN id SET DEFAULT nextval('geoloc3.u_role_dic_id_seq'::regclass);


--
-- Name: u_role_group_dic id; Type: DEFAULT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_role_group_dic ALTER COLUMN id SET DEFAULT nextval('geoloc3.u_role_group_dic_id_seq'::regclass);


--
-- Name: u_user id; Type: DEFAULT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_user ALTER COLUMN id SET DEFAULT nextval('geoloc3.u_user_id_seq'::regclass);


--
-- Name: u_role_dic PK_480ffbdace0ebcdcfb0059cd62e; Type: CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_role_dic
    ADD CONSTRAINT "PK_480ffbdace0ebcdcfb0059cd62e" PRIMARY KEY (id);


--
-- Name: to_type_dic PK_7ce1c9955f242658f2fee88353c; Type: CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.to_type_dic
    ADD CONSTRAINT "PK_7ce1c9955f242658f2fee88353c" PRIMARY KEY (id);

--
-- Name: u_user PK_9ca2142dc25dfea8a8c3ec09ea1; Type: CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_user
    ADD CONSTRAINT "PK_9ca2142dc25dfea8a8c3ec09ea1" PRIMARY KEY (id);


--
-- Name: u_permission PK_b54eea10837278c33857cf3d7e9; Type: CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_permission
    ADD CONSTRAINT "PK_b54eea10837278c33857cf3d7e9" PRIMARY KEY (id);


--
-- Name: u_permission2to PK_c05d0358491e3857c7668eefc3d; Type: CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_permission2to
    ADD CONSTRAINT "PK_c05d0358491e3857c7668eefc3d" PRIMARY KEY (id);


--
-- Name: u_role_group_dic PK_cebddd73c62fd2d38ade36ac0c2; Type: CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_role_group_dic
    ADD CONSTRAINT "PK_cebddd73c62fd2d38ade36ac0c2" PRIMARY KEY (id);


--
-- Name: to_territory PK_cfd00bc960c7cb73ab3b4650995; Type: CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.to_territory
    ADD CONSTRAINT "PK_cfd00bc960c7cb73ab3b4650995" PRIMARY KEY (id);


--
-- Name: u_permission2to REL_f4ef0e89163d1964abd89b8f33; Type: CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_permission2to
    ADD CONSTRAINT "REL_f4ef0e89163d1964abd89b8f33" UNIQUE (u_p_id);


--
-- Name: u_user UQ_24efca81b7f01c3fe02ad4a77e5; Type: CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_user
    ADD CONSTRAINT "UQ_24efca81b7f01c3fe02ad4a77e5" UNIQUE (phone);


--
-- Name: u_permission FK_067bf4f70241bae3f2a9e1278b1; Type: FK CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_permission
    ADD CONSTRAINT "FK_067bf4f70241bae3f2a9e1278b1" FOREIGN KEY (u_id) REFERENCES geoloc3.u_user(id);


--
-- Name: u_role_dic FK_07749c9686ef5de4141c6e491a8; Type: FK CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_role_dic
    ADD CONSTRAINT "FK_07749c9686ef5de4141c6e491a8" FOREIGN KEY (group_id) REFERENCES geoloc3.u_role_group_dic(id);


--
-- Name: u_permission FK_165f4fa6e498e7d7de8739f120c; Type: FK CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_permission
    ADD CONSTRAINT "FK_165f4fa6e498e7d7de8739f120c" FOREIGN KEY (role_id) REFERENCES geoloc3.u_role_dic(id);


--
-- Name: u_permission FK_4e3e63d447f469da671af98622a; Type: FK CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_permission
    ADD CONSTRAINT "FK_4e3e63d447f469da671af98622a" FOREIGN KEY (to_level) REFERENCES geoloc3.to_type_dic(id);


--
-- Name: u_permission2to FK_562ef4c64907b76bc3c9d4ce58c; Type: FK CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_permission2to
    ADD CONSTRAINT "FK_562ef4c64907b76bc3c9d4ce58c" FOREIGN KEY (u_p_id) REFERENCES geoloc3.u_permission(id);


--
-- Name: to_territory FK_9a41f5a5096e31b4a42568aa9bf; Type: FK CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.to_territory
    ADD CONSTRAINT "FK_9a41f5a5096e31b4a42568aa9bf" FOREIGN KEY (type_id) REFERENCES geoloc3.to_type_dic(id);


--
-- Name: u_permission2to FK_ae1ee4531c0ba3b6061853d312d; Type: FK CONSTRAINT; Schema: geoloc3; Owner: postgres
--

ALTER TABLE ONLY geoloc3.u_permission2to
    ADD CONSTRAINT "FK_ae1ee4531c0ba3b6061853d312d" FOREIGN KEY (to_id) REFERENCES geoloc3.to_territory(id);


--
-- PostgreSQL database dump complete
--


--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Ubuntu 11.3-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.3 (Ubuntu 11.3-1.pgdg18.04+1)

-- Started on 2019-05-23 15:43:27 EEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4440 (class 0 OID 57898)
-- Dependencies: 238
-- Data for Name: u_role_dic; Type: TABLE DATA; Schema: geoloc3; Owner: postgres
--

INSERT INTO geoloc3.u_role_dic VALUES (1, 'admin', 'Адміністратор', 1);
INSERT INTO geoloc3.u_role_dic VALUES (2, 'analytic', 'Аналітик', 1);
INSERT INTO geoloc3.u_role_dic VALUES (3, 'pst_member', 'Член комісії', 2);
INSERT INTO geoloc3.u_role_dic VALUES (4, 'spectator', 'Спостерігач', 2);
INSERT INTO geoloc3.u_role_dic VALUES (5, 'spectator_media', 'Спостерігач ЗМІ', 2);
INSERT INTO geoloc3.u_role_dic VALUES (6, 'po_member', 'Представник ГО', 2);
INSERT INTO geoloc3.u_role_dic VALUES (7, 'lawyer', 'Юрист', 3);


--
-- TOC entry 4447 (class 0 OID 0)
-- Dependencies: 242
-- Name: u_role_dic_id_seq; Type: SEQUENCE SET; Schema: geoloc3; Owner: postgres
--

SELECT pg_catalog.setval('geoloc3.u_role_dic_id_seq', 7, true);


-- Completed on 2019-05-23 15:43:27 EEST

--
-- PostgreSQL database dump complete
--


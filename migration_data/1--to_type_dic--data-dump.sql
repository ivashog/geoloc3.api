--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Ubuntu 11.3-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.3 (Ubuntu 11.3-1.pgdg18.04+1)

-- Started on 2019-05-23 15:40:05 EEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4439 (class 0 OID 57431)
-- Dependencies: 230
-- Data for Name: to_type_dic; Type: TABLE DATA; Schema: geoloc3; Owner: postgres
--

INSERT INTO geoloc3.to_type_dic VALUES (1, 'country', 'Країна');
INSERT INTO geoloc3.to_type_dic VALUES (2, 'region', 'Регіон');
INSERT INTO geoloc3.to_type_dic VALUES (3, 'district', 'Район, міста обласного підпорядкування');
INSERT INTO geoloc3.to_type_dic VALUES (4, 'county', 'Виборчий округ');
INSERT INTO geoloc3.to_type_dic VALUES (5, 'polling_station', 'Виборча дільниця');
INSERT INTO geoloc3.to_type_dic VALUES (6, 'settlement', 'Населений пункт');
INSERT INTO geoloc3.to_type_dic VALUES (7, 'tot', 'Тимчасово окупована територія');


--
-- TOC entry 4446 (class 0 OID 0)
-- Dependencies: 239
-- Name: to_type_dic_id_seq; Type: SEQUENCE SET; Schema: geoloc3; Owner: postgres
--

SELECT pg_catalog.setval('geoloc3.to_type_dic_id_seq', 7, false);


-- Completed on 2019-05-23 15:40:05 EEST

--
-- PostgreSQL database dump complete
--


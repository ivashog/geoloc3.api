--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Ubuntu 11.3-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.3 (Ubuntu 11.3-1.pgdg18.04+1)

-- Started on 2019-05-23 15:42:53 EEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4440 (class 0 OID 57890)
-- Dependencies: 237
-- Data for Name: u_role_group_dic; Type: TABLE DATA; Schema: geoloc3; Owner: postgres
--

INSERT INTO geoloc3.u_role_group_dic VALUES (1, 'users');
INSERT INTO geoloc3.u_role_group_dic VALUES (2, 'members');
INSERT INTO geoloc3.u_role_group_dic VALUES (3, 'lawyers');


--
-- TOC entry 4446 (class 0 OID 0)
-- Dependencies: 236
-- Name: u_role_group_dic_id_seq; Type: SEQUENCE SET; Schema: geoloc3; Owner: postgres
--

SELECT pg_catalog.setval('geoloc3.u_role_group_dic_id_seq', 3, true);


-- Completed on 2019-05-23 15:42:53 EEST

--
-- PostgreSQL database dump complete
--


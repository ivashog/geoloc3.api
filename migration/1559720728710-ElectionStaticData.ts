import { MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex } from 'typeorm';

export class ElectionStaticData1559720728710 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(
            new Table({
                name: 'el_static_data',
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    { name: 'to_id', type: 'integer', isUnique: true },
                    { name: 'type_id', type: 'integer', isNullable: true },
                    { name: 'counties', type: 'integer', isNullable: true },
                    { name: 'districts', type: 'integer', isNullable: true },
                    { name: 'stations', type: 'integer', isNullable: true },
                    { name: 'voters', type: 'integer', isNullable: true },
                    { name: 'address', type: 'varchar', isNullable: true },
                    { name: 'phone', type: 'varchar', isNullable: true },
                    { name: 'email', type: 'varchar', isNullable: true },
                    { name: 'boundaries', type: 'varchar', isNullable: true },
                    { name: 'place_descr', type: 'varchar', isNullable: true },
                    { name: 'station_type_id', type: 'integer', isNullable: true },
                ],
            }),
            true,
        );
        await queryRunner.createTable(
            new Table({
                name: 'el_station_type_dic',
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    { name: 'name', type: 'varchar' },
                ],
            }),
            true,
        );

        await queryRunner.createForeignKey(
            'el_static_data',
            new TableForeignKey({
                name: 'FK_EL_STAT_DATA__TYPE_ID',
                columnNames: ['type_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'to_type_dic',
                onDelete: 'CASCADE',
            }),
        );
        await queryRunner.createIndex(
            'el_static_data',
            new TableIndex({
                name: 'IDX_EL_STAT_DATA__TYPE_ID',
                columnNames: ['type_id'],
            }),
        );
        await queryRunner.createForeignKey(
            'el_static_data',
            new TableForeignKey({
                name: 'FK_EL_STAT_DATA__TO_ID',
                columnNames: ['to_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'to_territory',
                onDelete: 'CASCADE',
            }),
        );
        await queryRunner.createIndex(
            'el_static_data',
            new TableIndex({
                name: 'IDX_EL_STAT_DATA__TO_ID',
                columnNames: ['to_id'],
            }),
        );

        await queryRunner.createForeignKey(
            'el_static_data',
            new TableForeignKey({
                name: 'FK_EL_STAT_DATA__ST_T_ID',
                columnNames: ['station_type_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'el_station_type_dic',
                onDelete: 'CASCADE',
            }),
        );
        await queryRunner.createIndex(
            'el_static_data',
            new TableIndex({
                name: 'IDX_EL_STAT_DATA__ST_T_ID',
                columnNames: ['station_type_id'],
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('el_station_type_dic');
        await queryRunner.dropTable('el_static_data');
    }
}

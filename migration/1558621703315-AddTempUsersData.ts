import { MigrationInterface, QueryRunner } from 'typeorm';
import { plainToClass } from 'class-transformer';

import { UserEntity } from '../src/user/entities/user.entity';
import { PermissionEntity } from '../src/user/entities/permission.entity';
import { TerritoryEntity } from '../src/territory/entities/territory.entity';
import { Permission2toEntity } from '../src/user/entities/permission2to.entity';

export class AddTempUsersData1558621703315 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        const countryTerritory = await queryRunner.manager
            .getRepository<TerritoryEntity>(TerritoryEntity)
            .findOne({
                where: { type: 1 },
            });

        const adminPermission = await queryRunner.manager
            .getRepository<PermissionEntity>(PermissionEntity)
            .save(
                plainToClass(PermissionEntity, {
                    canEdit: true,
                    level: 1,
                    role: 1,
                    permission2to: plainToClass(Permission2toEntity, {
                        territory: countryTerritory,
                    }),
                }),
            );

        const tempUsers = await queryRunner.manager.getRepository<UserEntity>(UserEntity).save(
            plainToClass(UserEntity, [
                {
                    name: 'Global Admin Temp',
                    phone: '0505005050',
                    password: 'gAdmin_0505005050',
                    gender: 'm',
                    dateOfBirth: '1977-07-07',
                    permissions: [adminPermission],
                },
            ]),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        // delete CASCADE not work!
        // await queryRunner.manager
        //     .getRepository<UserEntity>(UserEntity)
        //     .delete({
        //         phone: '0505005050'
        //     })

        // TODO replace on previews query after adding 'ON DELETE CASCADE' references to FK
        await queryRunner.query(`
            DELETE FROM geoloc3.u_permission2to 
            WHERE u_p_id IN (
                SELECT id FROM geoloc3.u_permission
                WHERE u_id IN (
                        SELECT id FROM geoloc3.u_user
                        WHERE phone = '0505005050'
                    )
                );
            DELETE FROM geoloc3.u_permission
            WHERE u_id IN (
                SELECT id FROM geoloc3.u_user
                    WHERE phone = '0505005050'
                );
            DELETE FROM geoloc3.u_user
            WHERE phone = '0505005050';
        `);
    }
}

import { MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex } from 'typeorm';

export class ElCandidateResult1559891249944 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(
            new Table({
                name: 'el_result_candidate',
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    { name: 'to_id', type: 'integer' },
                    { name: 'candidate_id', type: 'integer' },
                    { name: 'votes', type: 'integer', default: 0 },
                    { name: 'v_created_at', type: 'timestamp without time zone', default: 'now()' },
                    { name: 'v_updated_at', type: 'timestamp without time zone', default: 'now()' },
                    { name: 'v_version', type: 'integer' },
                ],
            }),
            true,
        );

        await queryRunner.createForeignKey(
            'el_result_candidate',
            new TableForeignKey({
                name: 'FK_EL_RES_CAND__TO_ID',
                columnNames: ['to_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'to_territory',
            }),
        );
        await queryRunner.createForeignKey(
            'el_result_candidate',
            new TableForeignKey({
                name: 'FK_EL_RES_CAND__CAND_ID',
                columnNames: ['candidate_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'el_candidate',
            }),
        );

        await queryRunner.createIndex(
            'el_result_candidate',
            new TableIndex({
                name: 'IDX_EL_RES_CAND__TO_ID',
                columnNames: ['to_id'],
            }),
        );
        await queryRunner.createIndex(
            'el_result_candidate',
            new TableIndex({
                name: 'IDX_EL_RES_CAND__CAND_ID',
                columnNames: ['candidate_id'],
            }),
        );
        await queryRunner.createIndex(
            'el_result_candidate',
            new TableIndex({
                name: 'UQ_EL_RES_CAND__TO_ID_CAND_ID',
                isUnique: true,
                columnNames: ['to_id', 'candidate_id'],
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('el_result_candidate');
    }
}

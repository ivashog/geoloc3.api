import { MigrationInterface, QueryRunner } from 'typeorm';

const monitGeneralNullableColumns = [
    'ballots_send_ovk_dvk',
    'st_boxes',
    'mob_boxes',
    'electors_home',
    'ballots',
    'opened_at',
    'meeting_start_at',
    'is_boxes_sealed',
    'is_control_list',
    'v_version',
];

const ballotResultNullableColumns = [
    'ballots_received',
    'voters_end_voting',
    'voters_home_prev',
    'unused_ballots',
    'voters_station',
    'voters_home_fact',
    'total_received',
    'ballots_not_accounted',
    'voters_participated',
    'invalid_ballots',
    'ballots_acts_7375',
    'ballots_acts_78',
    'v_version',
];

export class DropNotNullConstraints1562742034346 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        // Drop not null constraints from el_monitoring_general
        await queryRunner.query(
            this.generateDropNotNullSql('el_monitoring_general', monitGeneralNullableColumns),
        );

        // Drop not null constraints from el_result_ballot
        await queryRunner.query(
            this.generateDropNotNullSql('el_result_ballot', ballotResultNullableColumns),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        // Add not null constraints to el_monitoring_general
        await queryRunner.query(
            this.generateSetNotNullSql('el_monitoring_general', monitGeneralNullableColumns),
        );

        // Add not null constraints to el_result_ballot
        await queryRunner.query(
            this.generateSetNotNullSql('el_result_ballot', ballotResultNullableColumns),
        );
    }

    private generateDropNotNullSql(table: string, nullableColumns: string[]): string {
        return nullableColumns.reduce((sql, column) => {
            return `
                    ${sql}
                    ALTER TABLE geoloc3.${table}
                        ALTER COLUMN ${column} DROP NOT NULL;
                `;
        }, '');
    }

    private generateSetNotNullSql(table: string, nullableColumns: string[]): string {
        return nullableColumns.reduce((sql, column) => {
            return `
                ${sql}
                ALTER TABLE geoloc3.${table}
                    ALTER COLUMN ${column} SET NOT NULL;
            `;
        }, '');
    }
}

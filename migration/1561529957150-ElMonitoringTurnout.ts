import { MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex } from 'typeorm';

export class ElMonitoringTurnout1561529957150 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(
            new Table({
                name: 'el_m_turnout_interval_dic',
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    { name: 'name', type: 'varchar' },
                    { name: 'min_percent', type: 'integer' },
                    { name: 'max_percent', type: 'integer' },
                ],
            }),
            true,
        );

        await queryRunner.query(`
            INSERT INTO geoloc3.el_m_turnout_interval_dic (id, name, min_percent, max_percent) VALUES
                (1, '11:00', 10, 24),
                (2, '15:00', 30, 50),
                (3, '20:00', 45, 75);
        `);

        await queryRunner.createTable(
            new Table({
                name: 'el_monitoring_turnout',
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    { name: 'to_id', type: 'integer' },
                    { name: 'interval_id', type: 'integer' },
                    { name: 'turnout', type: 'integer' },
                    { name: 'v_created_at', type: 'timestamp without time zone', default: 'now()' },
                    { name: 'v_updated_at', type: 'timestamp without time zone', default: 'now()' },
                    { name: 'v_version', type: 'integer' },
                ],
            }),
            true,
        );

        await queryRunner.createForeignKey(
            'el_monitoring_turnout',
            new TableForeignKey({
                name: 'FK_EL_MONIT_TURN__TO_ID',
                columnNames: ['to_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'to_territory',
            }),
        );
        await queryRunner.createIndex(
            'el_monitoring_turnout',
            new TableIndex({
                name: 'IDX_EL_MONIT_TURN__TO_ID',
                columnNames: ['to_id'],
            }),
        );

        await queryRunner.createForeignKey(
            'el_monitoring_turnout',
            new TableForeignKey({
                name: 'FK_EL_MONIT_TURN__INT_ID',
                columnNames: ['interval_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'el_m_turnout_interval_dic',
            }),
        );

        await queryRunner.createIndex(
            'el_monitoring_turnout',
            new TableIndex({
                name: 'IDX_EL_MONIT_TURN__INT_ID',
                columnNames: ['interval_id'],
            }),
        );

        await queryRunner.createIndex(
            'el_monitoring_turnout',
            new TableIndex({
                name: 'UQ_EL_MONIT_TURN__TO_ID_INT_ID',
                isUnique: true,
                columnNames: ['to_id', 'interval_id'],
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('el_monitoring_turnout');
        await queryRunner.dropTable('el_m_turnout_interval_dic');
    }
}

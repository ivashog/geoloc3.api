import {MigrationInterface, QueryRunner} from "typeorm";
import { promises as fsPromises } from 'fs';

export class FillDataDic1558599809755 implements MigrationInterface {
    private dumpFilesFolder = `${process.cwd()}/migration_data`;

    private async getDumpFiles(): Promise<string[]> {
        const files = await fsPromises.readdir(this.dumpFilesFolder, { encoding: 'utf8' });
        return files.filter(file => file.includes('--data-dump.sql')).sort();
    }

    private getTableNames(fileNames: string[]): string[] {
        return fileNames.map(file => file.split('--')[1]);
    }

    public async up(queryRunner: QueryRunner): Promise<any> {
        const dumpFiles = await this.getDumpFiles();
        dumpFiles.forEach( async dumpFile => {
            const dumpSql =  await fsPromises.readFile(`${this.dumpFilesFolder}/${dumpFile}`, {
                encoding: 'utf8', flag: 'r'
            });
            await queryRunner.query(dumpSql);
        })

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const dumpFiles = await this.getDumpFiles();
        const tableNames = this.getTableNames(dumpFiles);

        tableNames.forEach( async table => {
            await queryRunner.query(`TRUNCATE geoloc3.${table} CASCADE;`);
        })


    }

}

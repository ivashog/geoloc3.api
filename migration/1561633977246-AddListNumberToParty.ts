import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddListNumberToParty1561633977246 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `ALTER TABLE geoloc3.el_party ADD COLUMN IF NOT EXISTS full_name varchar(256);`,
        );
        await queryRunner.query(
            `ALTER TABLE geoloc3.el_party ADD COLUMN IF NOT EXISTS list_number integer;`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE geoloc3.el_party DROP COLUMN IF EXISTS list_number;`);
        await queryRunner.query(`ALTER TABLE geoloc3.el_party DROP COLUMN IF EXISTS full_name;`);
    }
}

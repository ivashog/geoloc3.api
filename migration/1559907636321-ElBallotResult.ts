import { MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex } from 'typeorm';

export class ElBallotResult1559907636321 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(
            new Table({
                name: 'el_result_ballot',
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    { name: 'to_id', type: 'integer' },
                    { name: 'ballots_received', type: 'integer' },
                    { name: 'voters_end_voting', type: 'integer' },
                    { name: 'voters_home_prev', type: 'integer' },
                    { name: 'unused_ballots', type: 'integer' },
                    { name: 'voters_station', type: 'integer' },
                    { name: 'voters_home_fact', type: 'integer' },
                    { name: 'total_received', type: 'integer' },
                    { name: 'ballots_not_accounted', type: 'integer' },
                    { name: 'voters_participated', type: 'integer' },
                    { name: 'invalid_ballots', type: 'integer' },
                    { name: 'ballots_acts_7375', type: 'integer' },
                    { name: 'ballots_acts_78', type: 'integer' },
                    { name: 'v_created_at', type: 'timestamp without time zone', default: 'now()' },
                    { name: 'v_updated_at', type: 'timestamp without time zone', default: 'now()' },
                    { name: 'v_version', type: 'integer' },
                ],
            }),
            true,
        );

        await queryRunner.createForeignKey(
            'el_result_ballot',
            new TableForeignKey({
                name: 'FK_EL_RES_BAL__TO_ID',
                columnNames: ['to_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'to_territory',
            }),
        );
        await queryRunner.createIndex(
            'el_result_ballot',
            new TableIndex({
                name: 'UQ_EL_RES_BAL__TO_ID',
                isUnique: true,
                columnNames: ['to_id'],
            }),
        );
        // Add comments to columns
        await queryRunner.query(`
            COMMENT ON TABLE geoloc3.el_result_ballot
                IS 'info about ballots results from protocols';
            
            COMMENT ON COLUMN geoloc3.el_result_ballot.ballots_received
                IS 'Кількість виборчих бюлетенів, одержаних дільничною виборчою комісією';
            
            COMMENT ON COLUMN geoloc3.el_result_ballot.voters_end_voting
                IS 'Кількість виборців, внесених до списку виборців на виборчій дільниці (на момент закінчення голосування)';
            
            COMMENT ON COLUMN geoloc3.el_result_ballot.voters_home_prev
                IS 'Кількість виборців, внесених на виборчій дільниці до витягу із списку виборців для голосування за місцем перебування';
            
            COMMENT ON COLUMN geoloc3.el_result_ballot.unused_ballots
                IS 'Кількість невикористаних виборчих бюлетенів';
            
            COMMENT ON COLUMN geoloc3.el_result_ballot.voters_station
                IS 'Кількість виборців, які отримали виборчі бюлетені у приміщенні для голосування';
            
            COMMENT ON COLUMN geoloc3.el_result_ballot.voters_home_fact
                IS 'Кількість виборців, які отримали виборчі бюлетені за місцем перебування';
            
            COMMENT ON COLUMN geoloc3.el_result_ballot.total_received
                IS 'Загальна кількість виборців, які отримали виборчі бюлетені';
            
            COMMENT ON COLUMN geoloc3.el_result_ballot.ballots_not_accounted
                IS 'Кількість виборчих бюлетенів, що не підлягають врахуванню';
            
            COMMENT ON COLUMN geoloc3.el_result_ballot.voters_participated
                IS 'Кількість виборців, які взяли участь у голосуванні на виборчій дільниці';
            
            COMMENT ON COLUMN geoloc3.el_result_ballot.invalid_ballots
                IS 'Кількість виборчих бюлетенів, визнаних недійсними';
            
            COMMENT ON COLUMN geoloc3.el_result_ballot.ballots_acts_7375
                IS 'Кількість виборчих бюлетенів, щодо якої складено акт невідповідності (частина десята статті 73, частина п’ята статті 75 Закону)';
            
            COMMENT ON COLUMN geoloc3.el_result_ballot.ballots_acts_78
                IS 'Кількість виборчих бюлетенів, щодо якої складено акт невідповідності (частина чотирнадцята статті 78 Закону)';
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('el_result_ballot');
    }
}

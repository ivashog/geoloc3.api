import { MigrationInterface, QueryRunner } from 'typeorm';

export class FillElectionStaticData1559723679118 implements MigrationInterface {
    private TEMP_TABLE: string = 'temp_el_station_static_data';

    public async up(queryRunner: QueryRunner): Promise<any> {
        // 1. Fill el_station_type_dic table
        await queryRunner.query(`
            INSERT INTO geoloc3.el_station_type_dic (id, name) VALUES
                (1, 'Звичайна'),
                (2, 'Лікувальний заклад'),
                (3, 'Орган виконання покарань'),
                (4, 'Закордонна'),
                (5, 'В/Ч за кордоном'),
                (6, 'На полярній станції');
        `);
        // 2. Create temporary table for saving station static data
        await queryRunner.query(`
            CREATE TABLE geoloc3.${this.TEMP_TABLE}
            (
                station character varying(6),
                uniq_id character varying,
                voters_count integer,
                station_type_id integer,
                address character varying,
                boundaries character varying,
                place_descr character varying
            )    
        `);
        // 3. Fill temp table from static csv file
        await queryRunner.query(`
            COPY geoloc3.${this.TEMP_TABLE} (
               station, uniq_id, voters_count, station_type_id, address, boundaries, place_descr
            ) FROM '${process.cwd()}/migration_data/stations-static-data.csv'
            WITH CSV DELIMITER ',' HEADER NULL 'NULL';
        `);
        // 4. Fill main election_static_data table from to_territory & temp tables
        await queryRunner.query(`
            WITH counties_by_region AS (
                SELECT
                    region,
                    COUNT(county) AS counties
                FROM geoloc3.to_territory
                WHERE type_id = 4
                GROUP BY region
            ), counties_by_districts AS (
                SELECT
                    district,
                    array_length(district_to_counties, 1) AS counties
                FROM geoloc3.to_territory
                WHERE type_id = 3
                GROUP BY district, district_to_counties
            ), districts_by_region AS (
                SELECT
                    region,
                    COUNT(district) AS districts
                FROM geoloc3.to_territory
                WHERE type_id = 3
                GROUP BY region
            ), districts_by_county AS (
                SELECT
                    county,
                    array_length(district_to_counties, 1) AS districts
                FROM geoloc3.to_territory
                WHERE type_id = 4
                GROUP BY county, district_to_counties
            ), stations_by_regions AS (
                SELECT
                    region,
                    COUNT(station) AS stations
                FROM geoloc3.to_territory
                WHERE type_id = 5
                GROUP BY region
            ), stations_by_counties AS (
                SELECT
                    region,
                    county,
                    COUNT(station) AS stations
                FROM geoloc3.to_territory
                WHERE type_id = 5
                GROUP BY region, county
            ), stations_by_districts AS (
                SELECT
                    region,
                    district,
                    COUNT(station) AS stations
                FROM geoloc3.to_territory
                WHERE type_id = 5
                GROUP BY region, district
            )
            INSERT INTO geoloc3.el_static_data (
                to_id, type_id, counties, districts, stations, voters, address, boundaries, place_descr, station_type_id                      
            )
            SELECT
                t.id AS to_id,
                t.type_id,
                (CASE
                    WHEN t.type_id = 1
                        THEN (SELECT SUM(counties) FROM counties_by_region)
                    WHEN t.type_id = 2 
                        THEN (SELECT counties FROM counties_by_region cbr WHERE cbr.region = t.region)
                    WHEN t.type_id = 3
                        THEN (SELECT counties FROM counties_by_districts cbd WHERE cbd.district = t.district)
                    WHEN t.type_id = 4
                        THEN 1
                    ELSE null
                END) AS counties,
            (CASE
                WHEN t.type_id = 1
                    THEN (SELECT SUM(districts) FROM districts_by_region)
                WHEN t.type_id = 2 
                    THEN (SELECT districts FROM districts_by_region dbr WHERE dbr.region = t.region)
                WHEN t.type_id = 3
                    THEN 1
                WHEN t.type_id = 4
                    THEN (SELECT districts FROM districts_by_county dbc WHERE dbc.county = t.county)
                ELSE null
            END) AS districts,
            (CASE
                WHEN t.type_id = 1
                    THEN (SELECT SUM(stations) FROM stations_by_regions)
                WHEN t.type_id = 2 
                     THEN (SELECT stations FROM stations_by_regions sbr WHERE sbr.region = t.region)
                WHEN t.type_id = 3
                    THEN (SELECT stations FROM stations_by_districts sbd WHERE sbd.district = t.district)
                WHEN t.type_id = 4
                    THEN (SELECT stations FROM stations_by_counties sbc WHERE sbc.county = t.county)
                WHEN t.type_id = 5
                    THEN 1
                ELSE null
            END) AS stations,
            ssd.voters_count AS voters,
            ssd.address,
            ssd.boundaries,
            ssd.place_descr,
            ssd.station_type_id
            FROM geoloc3.to_territory t
            LEFT JOIN geoloc3.${this.TEMP_TABLE} ssd
                ON t.uniq_code = ssd.uniq_id
            ORDER BY t.type_id, t.region, t.county, t.district
         `);
        // 5. Delete temp table
        await queryRunner.query(`DROP TABLE IF EXISTS geoloc3.${this.TEMP_TABLE} CASCADE;`);
        // 6. Aggregate voters count by counties, districts, regions and country
        await queryRunner.query(`
            WITH voters_by_regions AS (
                SELECT 
                    t.region,
                    SUM(elsd.voters) AS voters
                FROM geoloc3.el_static_data elsd
                LEFT JOIN geoloc3.to_territory t
                    ON elsd.to_id = t.id
                WHERE elsd.type_id = 5
                GROUP BY t.region
            ), voters_by_districts AS (
                SELECT 
                    t.district,
                    SUM(elsd.voters) AS voters
                FROM geoloc3.el_static_data elsd
                LEFT JOIN geoloc3.to_territory t
                    ON elsd.to_id = t.id
                WHERE elsd.type_id = 5
                GROUP BY t.district
            ), voters_by_counties AS (
                SELECT 
                    t.county,
                    SUM(elsd.voters) AS voters
                FROM geoloc3.el_static_data elsd
                LEFT JOIN geoloc3.to_territory t
                    ON elsd.to_id = t.id
                WHERE elsd.type_id = 5
                GROUP BY t.county
            )
            UPDATE geoloc3.el_static_data el
            SET voters = (
                CASE
                    WHEN el.type_id = 1 
                        THEN (SELECT SUM(voters) FROM voters_by_regions)
                    WHEN el.type_id = 2
                        THEN (SELECT SUM(voters) FROM voters_by_regions v 
                            LEFT JOIN geoloc3.to_territory tr 
                                ON tr.type_id = 2 AND v.region = tr.region 
                            WHERE el.to_id = tr.id)
                    WHEN el.type_id = 3
                        THEN (SELECT SUM(voters) FROM voters_by_districts v 
                            LEFT JOIN geoloc3.to_territory tr 
                                ON tr.type_id = 3 AND v.district = tr.district 
                            WHERE el.to_id = tr.id)
                    WHEN el.type_id = 4
                        THEN (SELECT SUM(voters) FROM voters_by_counties v 
                            LEFT JOIN geoloc3.to_territory tr 
                                ON tr.type_id = 4 AND v.county = tr.county 
                            WHERE el.to_id = tr.id)
                END
            )
            WHERE el.type_id IN (1,2,3,4);
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`TRUNCATE geoloc3.el_station_type_dic CASCADE;`);
        await queryRunner.query(`TRUNCATE geoloc3.el_static_data;`);
    }
}

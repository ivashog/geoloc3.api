import { MigrationInterface, QueryRunner } from 'typeorm';

export class FillPariesAndCandidates1562763125326 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        // Fill el_party
        await queryRunner.query(`
            COPY geoloc3.el_party (
                id, name, color, full_name, list_number
            ) FROM '${process.cwd()}/migration_data/parties-data.csv'
            WITH CSV DELIMITER ',' HEADER NULL 'NULL';
        `);

        // Fill el_candidate
        await queryRunner.query(`
            COPY geoloc3.el_candidate (
                id, full_name, party, county
            ) FROM '${process.cwd()}/migration_data/candidates-data.csv'
            WITH CSV DELIMITER ',' HEADER NULL 'NULL';
        `);

        // Fill el_list_candidates
        await queryRunner.query(`
            COPY geoloc3.el_list_candidates (
                id, full_name, party, list_number
            ) FROM '${process.cwd()}/migration_data/candidates-list-data.csv'
            WITH CSV DELIMITER ',' HEADER NULL 'NULL';
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        // Truncate el_party
        await queryRunner.query(`TRUNCATE geoloc3.el_party CASCADE;`);

        // Truncate el_candidate
        await queryRunner.query(`TRUNCATE geoloc3.el_candidate CASCADE;`);

        // Truncate el_list_candidates
        await queryRunner.query(`TRUNCATE geoloc3.el_list_candidates CASCADE;`);
    }
}

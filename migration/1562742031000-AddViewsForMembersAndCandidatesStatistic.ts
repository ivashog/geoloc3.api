import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddViewsForMembersAndCandidatesStatistic1562742031000 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        // Create view for u_members_statistic
        await queryRunner.query(`
            CREATE OR REPLACE VIEW geoloc3.u_members_statistic AS
             WITH regions AS (
                     SELECT to_territory.region,
                        to_territory.name
                       FROM geoloc3.to_territory
                      WHERE to_territory.type_id = 2
                    ), counties AS (
                     SELECT to_territory.county,
                        to_territory.name,
                        to_territory.region
                       FROM geoloc3.to_territory
                      WHERE to_territory.type_id = 4
                    ), members AS (
                     SELECT u_user.id AS u_id,
                        u_user.name AS user_name,
                        u_role_dic.displayed_name AS user_role,
                        u_role_dic.id AS role_id,
                        to_territory.name AS to_name,
                        to_territory.region,
                        to_territory.county
                       FROM geoloc3.u_user
                         LEFT JOIN geoloc3.u_permission ON u_permission.u_id = u_user.id
                         LEFT JOIN geoloc3.u_role_dic ON u_role_dic.id = u_permission.role_id
                         LEFT JOIN geoloc3.u_permission2to ON u_permission2to.u_p_id = u_permission.id
                         LEFT JOIN geoloc3.to_territory ON to_territory.id = u_permission2to.to_id
                      WHERE u_user.is_deleted <> true AND u_role_dic.group_id = 2
                    )
             SELECT r.region AS id,
                r.name,
                r.region,
                2 AS level,
                count(DISTINCT m.u_id) FILTER (WHERE m.role_id = 3)::integer AS "dvkMembersCount",
                count(DISTINCT m.u_id) FILTER (WHERE m.role_id = 4)::integer AS "spectatorsCount",
                count(DISTINCT m.u_id) FILTER (WHERE m.role_id = 5)::integer AS "spectatorsMediaCount",
                count(DISTINCT m.u_id) FILTER (WHERE m.role_id = 6)::integer AS "representativesGoCount",
                count(DISTINCT m.u_id)::integer AS "totalMembersCount"
               FROM members m
                 FULL JOIN regions r ON m.region::text = r.region::text
              GROUP BY r.name, r.region
            UNION ALL
             SELECT c.county AS id,
                c.name,
                c.region,
                4 AS level,
                count(DISTINCT m.u_id) FILTER (WHERE m.role_id = 3)::integer AS "dvkMembersCount",
                count(DISTINCT m.u_id) FILTER (WHERE m.role_id = 4)::integer AS "spectatorsCount",
                count(DISTINCT m.u_id) FILTER (WHERE m.role_id = 5)::integer AS "spectatorsMediaCount",
                count(DISTINCT m.u_id) FILTER (WHERE m.role_id = 6)::integer AS "representativesGoCount",
                count(DISTINCT m.u_id)::integer AS "totalMembersCount"
               FROM members m
                 FULL JOIN counties c ON m.county::text = c.county::text
              GROUP BY c.region, c.name, c.county;
        `);

        // Create view for el_candidates_statistic
        await queryRunner.query(`
            CREATE OR REPLACE VIEW geoloc3.el_candidates_statistic AS
             WITH counties_by_regions AS (
                     SELECT to_territory.region,
                        count(*) AS counties
                       FROM geoloc3.to_territory
                      WHERE to_territory.type_id = 4
                      GROUP BY to_territory.region
                    ), candidates_by_region AS (
                     SELECT t.region,
                        count(*) AS candidates
                       FROM geoloc3.el_candidate cand
                         JOIN geoloc3.to_territory t ON t.id = cand.county
                      GROUP BY t.region
                    ), candidates_by_county AS (
                     SELECT t.county,
                        count(*) AS candidates
                       FROM geoloc3.el_candidate cand
                         JOIN geoloc3.to_territory t ON t.id = cand.county
                      GROUP BY t.county
                    )
             SELECT terr.region AS id,
                terr.name,
                terr.region,
                2 AS level,
                cbr.counties::integer AS "countiesCount",
                COALESCE(cand.candidates, 0::bigint)::integer AS "candidatesCount"
               FROM geoloc3.to_territory terr
                 JOIN counties_by_regions cbr ON terr.region::text = cbr.region::text AND terr.type_id = 2
                 LEFT JOIN candidates_by_region cand ON terr.region::text = cand.region::text
            UNION ALL
             SELECT terr.county AS id,
                terr.name,
                terr.region,
                4 AS level,
                1 AS "countiesCount",
                COALESCE(cand.candidates, 0::bigint)::integer AS "candidatesCount"
               FROM geoloc3.to_territory terr
                 LEFT JOIN candidates_by_county cand ON terr.county::text = cand.county::text
              WHERE terr.type_id = 4;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        // Drop view u_members_statistic
        await queryRunner.query(`DROP VIEW IF EXISTS geoloc3.u_members_statistic`);

        // Drop view el_candidates_statistic
        await queryRunner.query(`DROP VIEW IF EXISTS geoloc3.el_candidates_statistic`);
    }
}

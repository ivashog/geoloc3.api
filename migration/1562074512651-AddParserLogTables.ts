import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddParserLogTables1562074512651 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        // -- Table: geoloc3.log_parse_files
        await queryRunner.query(`
            CREATE TABLE IF NOT EXISTS geoloc3.log_parse_files
            (
                file_id character varying COLLATE pg_catalog."default",
                ovk_id character varying COLLATE pg_catalog."default",
                region_id character varying COLLATE pg_catalog."default",
                downloaded timestamp without time zone,
                parsed timestamp without time zone,
                meta character varying COLLATE pg_catalog."default",
                parser_name character varying COLLATE pg_catalog."default",
                state_message character varying COLLATE pg_catalog."default"
            )
            WITH (
                OIDS = FALSE
            )
            TABLESPACE pg_default;
            
            ALTER TABLE geoloc3.log_parse_files
                OWNER to postgres;
        `);
        // -- Table: geoloc3.parser_log
        await queryRunner.query(`
            CREATE TABLE IF NOT EXISTS geoloc3.parser_log
            (
                parser_name character varying COLLATE pg_catalog."default",
                started_at timestamp without time zone,
                done_at timestamp without time zone,
                upload_files_first_iter_count integer,
                read_files_count integer,
                upserted_rows integer,
                all_files_count integer,
                upload_files_second_iter_count integer
            )
            WITH (
                OIDS = FALSE
            )
            TABLESPACE pg_default;
            
            ALTER TABLE geoloc3.parser_log
                OWNER to postgres;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`
            DROP TABLE IF EXISTS geoloc3.log_parse_files;
        `);
        await queryRunner.query(`
            DROP TABLE IF EXISTS geoloc3.parser_log;
        `);
    }
}

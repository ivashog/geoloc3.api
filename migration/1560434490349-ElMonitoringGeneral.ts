import { MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex } from 'typeorm';

export class ElMonitoringGeneral1560434490349 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(
            new Table({
                name: 'el_monitoring_general',
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    { name: 'to_id', type: 'integer' },
                    { name: 'ballots_send_ovk_dvk', type: 'integer' },
                    { name: 'st_boxes', type: 'integer' },
                    { name: 'mob_boxes', type: 'integer' },
                    { name: 'electors', type: 'integer' },
                    { name: 'electors_home', type: 'integer' },
                    { name: 'ballots', type: 'integer' },
                    { name: 'opened_at', type: 'time without time zone' },
                    { name: 'closed_at', type: 'time without time zone', isNullable: true },
                    { name: 'meeting_start_at', type: 'time without time zone' },
                    { name: 'is_boxes_sealed', type: 'boolean' },
                    { name: 'is_mob_boxes_returned', type: 'boolean', isNullable: true },
                    { name: 'is_control_list', type: 'boolean' },
                    { name: 'v_created_at', type: 'timestamp without time zone', default: 'now()' },
                    { name: 'v_updated_at', type: 'timestamp without time zone', default: 'now()' },
                    { name: 'v_version', type: 'integer' },
                ],
            }),
            true,
        );

        await queryRunner.createForeignKey(
            'el_monitoring_general',
            new TableForeignKey({
                name: 'FK_EL_MONIT_GEN__TO_ID',
                columnNames: ['to_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'to_territory',
            }),
        );
        await queryRunner.createIndex(
            'el_monitoring_general',
            new TableIndex({
                name: 'UQ_EL_MONIT_GEN__TO_ID',
                isUnique: true,
                columnNames: ['to_id'],
            }),
        );

        await queryRunner.query(`
            COMMENT ON TABLE geoloc3.el_monitoring_general
                IS 'Information about start and finish of election: count electors, ballots, commission, opened/close station time...';

            COMMENT ON COLUMN geoloc3.el_monitoring_general.ballots_send_ovk_dvk
                IS 'Кількість виборчих бюлетнів переданих від ОВК до ДВК';
            
            COMMENT ON COLUMN geoloc3.el_monitoring_general.st_boxes
                IS 'Кількість скриньок стаціонарних';
            
            COMMENT ON COLUMN geoloc3.el_monitoring_general.mob_boxes
                IS 'Кількість скриньок переносних ';
            
            COMMENT ON COLUMN geoloc3.el_monitoring_general.electors
                IS 'Кількість осіб внесених в списки виборців';
            
            COMMENT ON COLUMN geoloc3.el_monitoring_general.electors_home
                IS 'Кількість виборців внесених у списки для голосування на дому';
            
            COMMENT ON COLUMN geoloc3.el_monitoring_general.ballots
                IS 'Кількість бюлетенів';
            
            COMMENT ON COLUMN geoloc3.el_monitoring_general.opened_at
                IS 'Час відкриття дільниці';
            
            COMMENT ON COLUMN geoloc3.el_monitoring_general.closed_at
                IS 'Час закриття дільниці ';
            
            COMMENT ON COLUMN geoloc3.el_monitoring_general.meeting_start_at
                IS 'Час початку засідання ДВК';
            
            COMMENT ON COLUMN geoloc3.el_monitoring_general.is_boxes_sealed
                IS 'Чи опломбовані скриньки ?';
            
            COMMENT ON COLUMN geoloc3.el_monitoring_general.is_mob_boxes_returned
                IS 'Чи повернулися переносні скриньки?';
            
            COMMENT ON COLUMN geoloc3.el_monitoring_general.is_control_list
                IS 'Чи випущені контрольні листки?';
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('el_monitoring_general');
    }
}

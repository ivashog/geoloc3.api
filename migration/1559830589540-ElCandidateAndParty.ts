import { MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex } from 'typeorm';

export class ElCandidateAndParty1559830589540 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(
            new Table({
                name: 'el_candidate',
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    { name: 'full_name', type: 'varchar(200)' },
                    { name: 'party', type: 'integer', isNullable: true },
                    { name: 'county', type: 'integer' },
                ],
            }),
            true,
        );
        await queryRunner.createTable(
            new Table({
                name: 'el_party',
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    { name: 'name', type: 'varchar(200)' },
                    { name: 'color', type: 'varchar(20)' },
                ],
            }),
            true,
        );

        // drop old FK
        const candidateTable = await queryRunner.getTable('el_candidate');
        candidateTable.foreignKeys.forEach(async fk => {
            await queryRunner.dropForeignKey('el_candidate', fk);
        });

        await queryRunner.createForeignKey(
            'el_candidate',
            new TableForeignKey({
                name: 'FK_EL_CANDIDATE__PARTY',
                columnNames: ['party'],
                referencedColumnNames: ['id'],
                referencedTableName: 'el_party',
                onDelete: 'CASCADE',
            }),
        );
        await queryRunner.createForeignKey(
            'el_candidate',
            new TableForeignKey({
                name: 'FK_EL_CANDIDATE__COUNTY',
                columnNames: ['county'],
                referencedColumnNames: ['id'],
                referencedTableName: 'to_territory',
                onDelete: 'CASCADE',
            }),
        );
        await queryRunner.createIndex(
            'el_candidate',
            new TableIndex({
                name: 'IDX_EL_CANDIDATE__PARTY',
                columnNames: ['party'],
            }),
        );
        await queryRunner.createIndex(
            'el_candidate',
            new TableIndex({
                name: 'IDX_EL_CANDIDATE__COUNTY',
                columnNames: ['county'],
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('el_candidate');
        await queryRunner.dropTable('el_party');
    }
}

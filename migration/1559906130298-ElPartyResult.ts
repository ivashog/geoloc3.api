import { MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex } from 'typeorm';

export class ElPartyResult1559906130298 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(
            new Table({
                name: 'el_result_party',
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    { name: 'to_id', type: 'integer' },
                    { name: 'party_id', type: 'integer' },
                    { name: 'votes', type: 'integer', default: 0 },
                    { name: 'v_created_at', type: 'timestamp without time zone', default: 'now()' },
                    { name: 'v_updated_at', type: 'timestamp without time zone', default: 'now()' },
                    { name: 'v_version', type: 'integer' },
                ],
            }),
            true,
        );

        await queryRunner.createForeignKey(
            'el_result_party',
            new TableForeignKey({
                name: 'FK_EL_RES_PARTY__TO_ID',
                columnNames: ['to_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'to_territory',
            }),
        );
        await queryRunner.createForeignKey(
            'el_result_party',
            new TableForeignKey({
                name: 'FK_EL_RES_PARTY__P_ID',
                columnNames: ['party_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'el_party',
            }),
        );
        await queryRunner.createIndex(
            'el_result_party',
            new TableIndex({
                name: 'IDX_EL_RES_PARTY__TO_ID',
                columnNames: ['to_id'],
            }),
        );
        await queryRunner.createIndex(
            'el_result_party',
            new TableIndex({
                name: 'IDX_EL_RES_PARTY__P_ID',
                columnNames: ['party_id'],
            }),
        );
        await queryRunner.createIndex(
            'el_result_party',
            new TableIndex({
                name: 'UQ_EL_RES_PARTY__TO_ID_P_ID',
                isUnique: true,
                columnNames: ['to_id', 'party_id'],
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('el_result_party');
    }
}

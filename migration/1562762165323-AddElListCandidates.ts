import { MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex } from 'typeorm';

export class AddElListCandidates1562762165323 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(
            new Table({
                name: 'el_list_candidates',
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    { name: 'full_name', type: 'varchar(200)' },
                    { name: 'party', type: 'integer' },
                    { name: 'list_number', type: 'integer' },
                ],
            }),
            true,
        );
        await queryRunner.createForeignKey(
            'el_list_candidates',
            new TableForeignKey({
                name: 'FK_EL_CAND_LIST__PARTY',
                columnNames: ['party'],
                referencedColumnNames: ['id'],
                referencedTableName: 'el_party',
                onDelete: 'CASCADE',
            }),
        );

        await queryRunner.createIndex(
            'el_list_candidates',
            new TableIndex({
                name: 'IDX_EL_CAND_LIST__PARTY',
                columnNames: ['party'],
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('el_list_candidates');
    }
}

import {MigrationInterface, QueryRunner} from "typeorm";
import { promises as fsPromises } from "fs";

export class FillTerritoriesData1558613424792 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`
            COPY geoloc3.to_territory (
                name, koatuu, region, district, county, station, district_to_counties, uniq_code, type_id, v_version
            ) FROM '${process.cwd()}/migration_data/territories-data.csv'
            WITH CSV DELIMITER ',' HEADER NULL 'NULL';
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`TRUNCATE geoloc3.to_territory;`);
    }

}

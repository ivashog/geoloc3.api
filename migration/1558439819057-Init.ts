import {MigrationInterface, QueryRunner} from "typeorm";
import { promises as fsPromises } from 'fs';

export class Init1558439819057 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const initDbSql = await fsPromises.readFile(`${process.cwd()}/migration_data/geoloc3--schema-dump.sql`, {
            encoding: 'utf8', flag: 'r'
        });
        await queryRunner.query(initDbSql);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const dropTableQueries = await queryRunner.query(`
            SELECT 
                'DROP TABLE ' || schemaname || '.' || tablename || ' CASCADE;' AS query
            FROM pg_tables
            WHERE schemaname = 'geoloc3' 
            AND tablename <> 'migrations';
        `);

        dropTableQueries.forEach(async dropTable => {
            await queryRunner.query(dropTable.query);
        });
    }

}

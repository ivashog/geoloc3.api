import { MigrationInterface, QueryRunner } from 'typeorm';

export class CandidateRefactoring1559819207656 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('el_candidates');
        if (table) {
            await queryRunner.query(`ALTER TABLE geoloc3.el_candidates RENAME TO el_candidate;`);
        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('el_candidate');
        if (table) {
            await queryRunner.query(`ALTER TABLE geoloc3.el_candidate RENAME TO el_candidates;`);
        }
    }
}
